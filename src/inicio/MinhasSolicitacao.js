import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  TextInput,
  Image,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
  StatusBar,
  Platform,
  RefreshControl,
  ToastAndroid,
  BackHandler
} from "react-native";
import GridView from 'react-native-super-grid';
import { checkInternetConnection } from 'react-native-offline';;
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/EvilIcons';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import Carousel from 'react-native-snap-carousel'
import RNFetchBlob from 'rn-fetch-blob';
import { Searchbar, Provider as PaperProvider } from 'react-native-paper';
import API, { caminhoAPI, versaoAPI } from '../utils/caminhosAPI'

class MinhasSolicitacao extends React.Component {
  state = {
    visible: false,
    refreshing: false,
    refresh: true,
    destino: '',
    qtdAdulto: 0,
    qtdCrianca: 0,
    solicita: []
  }

  componentDidMount() {
    var RNFS = require('react-native-fs');
    var path = RNFS.DocumentDirectoryPath + '/usuario.json';

    RNFS.readFile(path, 'utf8')
      .then(async (contents) => {
        if (!(contents == '[{}]')) {
          this.setState({
            idUsuario: String(JSON.parse(contents)[0].codigo),
            tipo: JSON.parse(contents)[0].tipo,
            token: JSON.parse(contents)[0].token
          })

          const isConnected = await checkInternetConnection();

          if (isConnected) {
            fetch(caminhoAPI() + '/minhasSolicitacoes', {
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Versao': versaoAPI,
                'Auth': 'Bearer ' + this.state.token
                //'Cliente': JSON.parse(contents)[0].codigo,
                //'Tipo': JSON.parse(contents)[0].tipo
              },
            }
            ).then((response) => {
              const statusCode = response.status;
              let responseJson = [];

              if (statusCode == 200) {
                responseJson = response.json();
              }

              return Promise.all([responseJson, statusCode]);
            })
              .then(([responseJson, statusCode]) => {
                if (statusCode == 200) {
                  
                  this.setState({
                    refresh: false,
                    solicita: responseJson
                  })

                } else {
                  alert('Nenhuma solicitação encontrada')
                  this.setState({
                    vazio: true,
                    refresh: false
                  })
                }
              })
              .catch((error) => {
                this.setState({
                  vazio: true,
                  //solicita: [],
                  refresh: false
                })
                alert('ocorreu uma falha')
              });

          } else {
            alert('Sem conexão com a internet!')
            this.setState({
              vazio: true,
              refresh: false
            })
          }
        } else {
          this.setState({ idUsuario: '0' })
        }
      })
      .catch((err) => {
        this.setState({ idUsuario: '0' })
        alert(err.message)
        console.log(err.message, err.code);
      });
  }

  nomeMes = (mes) => {
    return mes == '01' ? 'JANEIRO' :
      mes == '02' ? 'FEVEREIRO' :
        mes == '03' ? 'MARÇO' :
          mes == '04' ? 'ABRIL' :
            mes == '05' ? 'MAIO' :
              mes == '06' ? 'JUNHO' :
                mes == '07' ? 'JULHO' :
                  mes == '08' ? 'AGOSTO' :
                    mes == '09' ? 'SETEMBRO' :
                      mes == '10' ? 'OUTUBRO' :
                        mes == '11' ? 'NOVEMBRO' :
                          mes == '12' ? 'DEZEMBRO' : null
  }

  refresh = async () => {
    const isConnected = await checkInternetConnection();
    if (isConnected) {
      this.setState({
        solicita: []
      })
      fetch(caminhoAPI() + '/minhasSolicitacoes', {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Versao': versaoAPI,
          'Auth': 'Bearer ' + this.state.token
          //'Cliente': JSON.parse(contents)[0].codigo,
          //'Tipo': JSON.parse(contents)[0].tipo
        },
      }
      ).then((response) => {
        const statusCode = response.status;
        let responseJson = [];

        if (statusCode == 200) {
          responseJson = response.json();
        }

        return Promise.all([responseJson, statusCode]);
      })
        .then(([responseJson, statusCode]) => {
          if (statusCode == 200) {

            this.setState({
              refresh: false,
              solicita: responseJson
            })

          } else {
            this.setState({
              vazio: true,
              refresh: false
            })
          }
        })
        .catch((error) => {
          this.setState({
            vazio: true,
            //solicita: [],
            refresh: false
          })
          alert('ocorreu uma falha')
        });

    } else {
      alert('Sem conexão com a internet!')
      this.setState({
        vazio: true,
        refresh: false
      })
    }
  }

  exibirLocais = (cod, ini, fim) => {
    const RNFS = require('react-native-fs');
    const pro = RNFS.DocumentDirectoryPath + '/reservaConfirma.json';

    let dados = [];

    dados.push({
      "codSolicita": cod,
      "inicio": ini,
      "fim": fim
    })

    RNFS.writeFile(pro, JSON.stringify(dados), 'utf8')
      .then((success) => {
        console.log('FILE WRITTEN!');
      })
      .catch((err) => {
        console.log(err.message);
      });

    this.props.navigation.navigate('acomodacoesReserva')
  }

  createSolicitacao() {
    let solicita = []

    for (let index = 0; index < this.state.solicita.length; index++) {
      let sol = this.state.solicita[index]

      solicita.push(
        <View
          style={{
            height: 310,
            marginHorizontal: 29,
            borderWidth: 0.7,
            borderColor: '#ffec00',
            borderRadius: 20,
            marginTop: 20,
            backgroundColor: '#001e46',
            elevation: 4,
          }}>

          <Image
            style={{
              resizeMode: 'cover',
              width: '100%',
              height: '50%',
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20
            }}
            source={require('../img/praia1.jpg')} />

          {sol.StatusDescricao == 'CANCELADO' ?
            <View style={{
              backgroundColor: '#ff5252',
              position: 'absolute',
              height: 30,
              borderBottomLeftRadius: 20,
              borderTopLeftRadius: 20,
              alignSelf: 'flex-end',
              justifyContent: 'center',
              width: '40%',
              marginTop: 20,
            }}>
              <Text style={{ fontSize: 12, color: 'white', textAlign: 'center' }}>CANCELADO</Text>
            </View>
            : null
          }

          <Text style={{
            color: '#ffec00',
            fontSize: 14,
            fontWeight: 'bold',
            marginLeft: 15,
            marginTop: 10
          }}>{String(sol.LocalCidade).toLocaleUpperCase()} - {String(sol.LocalUF).toLocaleUpperCase()}</Text>

          <Text style={{
            color: 'white',
            fontSize: 10,
            marginLeft: 15,
          }}>{String(sol.DataEntrada).substr(-2)} DE {this.nomeMes(String(sol.DataEntrada).substr(5, 2))} - {String(sol.DataSaida).substr(-2)} DE {this.nomeMes(String(sol.DataSaida).substr(5, 2))}</Text>
          <Text style={{
            color: 'white',
            fontSize: 10,
            marginLeft: 15,
            marginTop: 10
          }}>STATUS</Text>

          <View style={{
            borderBottomWidth: 1,
            borderColor: '#ffec00',
            marginTop: 40,
            marginRight: 30,
            marginHorizontal: 35
          }} />

          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: -25 }}>
            <View style={{ width: 80, alignItems: 'center' }}>
              <Text style={{
                color: '#ffec00',
                fontSize: 8,
                marginBottom: 4
              }}>VERIFICANDO</Text>
              <View style={{
                height: 20,
                width: 20,
                borderRadius: 20,
                backgroundColor: '#ffec00'
              }} />
            </View>

            <View style={{ width: 80, alignItems: 'center' }}>
              <Text style={{
                color: '#ffec00',
                fontSize: 8,
                marginBottom: 4
              }}>SELECIONADO</Text>
              <View style={{
                height: 20,
                width: 20,
                borderWidth: 1,
                borderRadius: 20,
                borderColor: sol.StatusDescricao == 'SELECIONADO' ? 'transparent' : '#ffec00',
                backgroundColor: sol.StatusDescricao == 'SELECIONADO' ? '#ffec00' : '#001e46'
              }} />
            </View>

            <View style={{ width: 80, alignItems: 'center' }}>
              <Text style={{
                color: '#ffec00',
                fontSize: 8,
                marginBottom: 4
              }}>CONFIRMADO</Text>
              <View style={{
                height: 20,
                width: 20,
                borderRadius: 20,
                borderWidth: 1,
                borderColor: parseInt(sol.QtdeApartamento) > 0 ? 'transparent' : '#ffec00',
                backgroundColor: parseInt(sol.QtdeApartamento) > 0 ? '#ffec00' : '#001e46'
              }} />
            </View>
          </View>

          {parseInt(sol.QtdeApartamento) > 0 ?
            <TouchableOpacity onPress={() => { this.exibirLocais(sol.Codigo, sol.DataEntrada, sol.DataSaida) }} style={{
              height: 30,
              width: 120,
              paddingHorizontal: 2,
              justifyContent: 'center',
              marginLeft: 15,
              marginTop: 15,
              marginBottom: 25,
              borderRadius: 30,
              borderWidth: 0.8,
              borderColor: '#ffec00',
              backgroundColor: '#ffec00'
            }}>
              <Text style={{
                color: '#001e46',
                fontSize: 12,
                fontWeight: 'bold',
                textAlign: 'center'
              }}>Exibir locais</Text>
            </TouchableOpacity> : null}
        </View>

      )

    }

    return solicita
  }

  render() {

    return (
      <View style={{ backgroundColor: '#001e46', height: '100%' }}>

        <StatusBar
          barStyle="light-content"
          backgroundColor='#001e46'
        />

        <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginLeft: 20,
          marginTop: 40,
          alignItems: 'center'
        }}>
          <TouchableOpacity onPress={() => {
            try {
              this.props.screenProps.myDrawerNavigation.toggleDrawer();
            } catch (e) {
              this.props.navigation.toggleDrawer();
            }
          }}>
            <Iconr name="menu" color='#ffec00' style={{ padding: 5 }} size={40} />
          </TouchableOpacity>

          <View>
            <Text style={{ color: 'white', fontSize: 12, marginRight: 40 }}>MINHAS</Text>
            <Text style={{ color: '#ffec00', fontSize: 18, fontWeight: 'bold', marginRight: 40 }}>SOLICITAÇÕES</Text>
          </View>
        </View>

        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this.refresh()}
            />
          }
          style={{ backgroundColor: '#001e46', height: '100%' }}>
          {
            !this.state.refresh ?
              this.createSolicitacao() :
              <ActivityIndicator size='small' color={'#ffec00'} />
          }
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 29 }}>
            <View />
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('MinhasReservas') }} style={{
              height: 30,
              width: 120,
              paddingHorizontal: 10,
              justifyContent: 'center',
              paddingVertical: 5,
              marginLeft: 29,
              marginTop: 15,
              marginBottom: 25,
              borderRadius: 30,
              borderWidth: 0.8,
              borderColor: '#ffec00',
              backgroundColor: '#001e46'
            }}>
              <Text style={{
                color: '#ffec00',
                fontSize: 12,
                textAlign: 'center'
              }}>Voltar</Text>
            </TouchableOpacity>
          </View>

          <View />
        </ScrollView>
        <View style={{ flexDirection: 'row', height: Platform.OS === 'android' ? 55 : 75, backgroundColor: '#ffec00', alignItems: 'center' }}>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('MenuDestino1') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rdestino.png')}
              style={{
                height: 50,
                width: 50,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('Inicio') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rreservas.png')}
              style={{
                height: 50,
                width: 50,
                marginBottom: 6,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>
          <TouchableOpacity style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rpontos.png')}
              style={{
                height: 50,
                width: 50,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('MinhaConta') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rconta.png')}
              style={{
                height: 50,
                width: 50,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>

        </View>

      </View>
    )
  }
}

export default MinhasSolicitacao;
const styles = StyleSheet.create({
  gridView: {
    height: 300,
  },
  itemContainer: {
    elevation: 4,
    height: 70,
    marginHorizontal: 10,
    marginBottom: 5,
  },
  itemContainerItem: {
    elevation: 4,
    height: 50,
    marginLeft: 45,
    marginBottom: 5,
  },
  itemContainerProfissa: {
    elevation: 4,
    height: 100,
    marginLeft: 45,
    marginRight: -5,
    marginBottom: 5,
  },

});