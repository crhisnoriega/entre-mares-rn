import React, { Component } from "react";
import {
    ScrollView,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ToastAndroid,
    ActivityIndicator,
    Alert,
    Image,
    ImageBackground,
    StatusBar,
    Platform,
    BackHandler,
    Dimensions
} from "react-native";
import { checkInternetConnection } from 'react-native-offline';
import Icon from 'react-native-vector-icons/AntDesign';
import IconO from 'react-native-vector-icons/Octicons';
import IconF from 'react-native-vector-icons/FontAwesome'
import IconM from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import API, { caminhoAPI, versaoAPI } from '../utils/caminhosAPI'

class MinhaCarterinha extends React.Component {
    state = {
        dataSource: [],
        usuario: [],
        idUsuario: '0',
        detalhes: false,
        tem: false,
        loading: false,
        vendedor: false
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('MenuP')
            return true;
        })

        var RNFS = require('react-native-fs');
        var path = RNFS.DocumentDirectoryPath + '/usuario.json';

        RNFS.readFile(path, 'utf8')
            .then(async (contents) => {
                console.warn(contents);
                if (!(contents == '[{}]')) {
                    console.warn(contents);
                    this.setState({
                        dataSource: JSON.parse(contents),
                    });

                    const isConnected = await checkInternetConnection();
                    if (isConnected) {
                        fetch(caminhoAPI() + '/recuperaDadosCliente', {
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                                'Auth': 'Bearer ' + this.state.dataSource[0].token,
                                'Versao': versaoAPI
                            },
                        }
                        ).then((response) => response.json())
                            .then((responseJson) => {
                                this.setState({
                                    tem: true,
                                    usuario: responseJson
                                })
                            })
                            .catch((error) => {
                                this.setState({
                                    vazio: true,
                                    grupos: [],
                                    refresh: false
                                })
                            });

                    } else {
                        alert('Sem conexão com a internet!')
                        this.setState({
                            vazio: true,
                            refresh: false
                        })
                    }
                } else {
                    Alert.alert(
                        'Confirmação',
                        'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                        [
                            { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                            { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                        ]
                    );
                    this.props.navigation.navigate('MenuDestino1')
                }

            })
            .catch((err) => {
                console.log(err.message, err.code);
            });


    }

    render() {
        const width = Dimensions.get('window').width
        const height = (Dimensions.get('window').height - ((Dimensions.get('window').height * (Platform.OS == "android" ? 10 : 13)) / 100))
        return (
            <View style={{ flex: 1, backgroundColor: '#ffec00', justifyContent: "space-between" }}>
                <StatusBar
                    barStyle="dark-content"
                    backgroundColor='#ffec00'
                />
                {!(this.state.tem) ?
                    <ActivityIndicator color={'#001e46'} size="small" style={{ backgroundColor: '#ffec00' }} />
                    :

                    <ImageBackground
                        source={require('../img/carterinha.png')}
                        resizeMode="stretch"
                        style={{
                            marginTop: Platform.OS == "android" ? '35%' : '55%',
                            height: width,
                            width: height,
                            transform: [{ rotate: '90deg' }],
                            alignSelf: 'center',
                        }}>
                        <View
                            style={{
                                marginTop: Platform.OS == "android" ? '43%' :'38%'
                            }}>
                            <Text style={{
                                fontSize: 16,
                                width: 300,
                                fontWeight: "bold",
                                marginLeft: '15%',
                                color: '#ffec00',
                                fontWeight: 'bold'
                            }}>ASSOCIADO: {String(this.state.usuario.Nome).toLocaleUpperCase()}</Text>
                        </View>

                        <View
                            style={{
                                marginTop: '5%',
                            }}>
                            <Text style={{
                                fontSize: 16,
                                fontWeight: "bold",
                                width: 300,
                                marginLeft: '15%',
                                color: '#ffec00',
                                fontWeight: 'bold'
                            }}>CÓDIGO: {this.state.usuario.NumeroTitulo}       DATA: {this.state.usuario.DataCadastro}</Text>
                        </View>
                    </ImageBackground>
                }
                <TouchableOpacity onPress={() => {
                    try {
                        this.props.screenProps.myDrawerNavigation.toggleDrawer();
                    } catch (e) {
                        this.props.navigation.toggleDrawer();
                    }
                }} style={{ marginLeft: 20, position: "absolute", marginTop: Platform.OS == "android" ? 0 : 40 }}>
                    <Iconr name="menu" color='#ffec00' style={{ padding: 5 }} size={40} />
                </TouchableOpacity>


                <View
                    style={{
                        flexDirection: 'row',
                        height: Platform.OS === 'android' ? 55 : 75,
                        backgroundColor: '#ffec00',
                        alignItems: 'center',
                        alignContent: "flex-end"
                    }}>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('MenuDestino1') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rdestino.png')}
                            style={{
                                height: 50,
                                width: 50,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        if (this.state.idUsuario == '0') {
                            Alert.alert(
                                'Confirmação',
                                'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                                [
                                    { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                                    { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                                ]
                            );
                        } else {
                            this.props.navigation.navigate('Inicio')
                        }
                    }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rreservas.png')}
                            style={{
                                height: 50,
                                width: 50,
                                marginBottom: 6,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rpontos.png')}
                            style={{
                                height: 50,
                                width: 50,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        if (this.state.idUsuario == '0') {
                            Alert.alert(
                                'Confirmação',
                                'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                                [
                                    { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                                    { text: 'SIM', onPress: () => { this.props.navigation.navigate('Login') } },
                                ]
                            );
                        } else {
                            this.props.navigation.navigate('MinhaConta')
                        }
                    }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rconta.png')}
                            style={{
                                height: 50,
                                width: 50,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>

                </View>

            </View>
        );
    }
}

export default MinhaCarterinha;
const styles = StyleSheet.create({
    ActivityIndicator_Style: {

        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center'
    },
    desingnIten: {
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 5,
        flexDirection: 'row',
        height: 80,
    },
    TextTitle: {
        color: 'white',
        marginLeft: 0,
    },
    TextTop: {
        fontWeight: 'bold',
        color: '#001e46',
        fontSize: 16,
    },
    TextBaixo: {
        color: '#001e46',
        marginTop: -5,
        fontSize: 11,
    },
    ViewTopText: {
        alignItems: 'center',
    },
    botaoEnd: {
        width: 40,
        height: 40,
        borderWidth: 1.5,
        borderColor: 'white',
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        marginHorizontal: 10,
        elevation: 5,
    },

});