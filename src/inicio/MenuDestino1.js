import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  TextInput,
  Image,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
  StatusBar,
  Linking,
  Platform,
  ToastAndroid,
  BackHandler
} from "react-native";
import GridView from 'react-native-super-grid';
import { checkInternetConnection } from 'react-native-offline';
import imgs from '../utils/icones'
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/EvilIcons';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import Carousel from 'react-native-snap-carousel'
import RNFetchBlob from 'rn-fetch-blob';
import { Searchbar, Provider as PaperProvider } from 'react-native-paper';
import API, { caminhoAPI, versaoAPI } from '../utils/caminhosAPI'

class MenuDestino1 extends React.Component {
  state = {
    visible: false,
    destino: '',
    idUsuario: '0',
    tipo: '',
    qtdAdulto: 0,
    qtdCrianca: 0,
    grupos: []
  }

  async componentDidMount() {

    var RNFS = require('react-native-fs');
    var path = RNFS.DocumentDirectoryPath + '/usuario.json';

    RNFS.readFile(path, 'utf8')
      .then((contents) => {
        if (!(contents == '[{}]')) {
          BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('MenuP')
            return true;
          })

          this.setState({
            idUsuario: String(JSON.parse(contents)[0].codigo),
            tipo: JSON.parse(contents)[0].tipo,
            token: JSON.parse(contents)[0].token
          })
        } else {
          BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('Login')
            return true;
          })
          this.setState({ idUsuario: '0' })
        }
      })
      .catch((err) => {
        BackHandler.addEventListener('hardwareBackPress', () => {
          this.props.navigation.navigate('Login')
          return true;
        })
        alert('Falhou')
        console.log(err.message, err.code);
      });

      const isConnected = await checkInternetConnection();

      if (isConnected) {
        fetch(caminhoAPI() + '/consultarCategoria', {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Versao': versaoAPI
          },
        }
        ).then((response) => response.json())
          .then((responseJson1) => {
            if (responseJson1.length > 0) {

              this.setState({
                refresh: false,
                grupos: responseJson1
              })

            } else {
              this.setState({
                vazio: true,
                refresh: false
              })
            }
          })
          .catch((error) => {
            this.setState({
              vazio: true,
              grupos: [],
              refresh: false
            })
          });

      } else {
        alert('Sem conexão com a internet!')
        this.setState({
          vazio: true,
          refresh: false
        })
      }
  }

  select = (id, descr) => {
    const RNFS = require('react-native-fs');
    const pro = RNFS.DocumentDirectoryPath + '/reservaCria.json';

    let dados = [];

    dados.push({
      tipo: String(descr).replace(' ', ''),
      codigo: id,
      descricao: descr
    })

    RNFS.writeFile(pro, JSON.stringify(dados), 'utf8')
      .then((success) => {
        console.log('FILE WRITTEN!');
      })
      .catch((err) => {
        console.log(err.message);
      });


    this.props.navigation.navigate('MenuDestinoResult')
  }

  rowitem() {
    let rows = []
    let item = []
    let i = 0

    for (let index = 0; index < (this.state.grupos.length); index++) {
      const grupo = this.state.grupos[index];
      i++

      if ((this.state.grupos.length - index) > 2) {
        if (i == 1) {
          item.push(
            <TouchableOpacity
              onPress={() => {
                if (grupo.Descricao == 'Excursões') {
                  Linking.openURL('https://entremares.tur.br/destinos/excursoes/')
                  return
                }

                if (grupo.Descricao == 'Convênios') {
                  Linking.openURL('https://entremares.tur.br/destinos/convenio/')
                  return
                }

                this.select(grupo.Codigo, grupo.Descricao);
              }} style={{
                height: 150,
                width: '50%',
                justifyContent: 'space-between',
                borderBottomWidth: 0.7,
                borderRightWidth: 0.7,
                borderColor: '#ffec00'
              }}>
              <Image
                source={imgs[String(grupo.Descricao).replace(' ', '')]}
                style={{
                  height: 30,
                  width: 30,
                  marginLeft: 20,
                  marginTop: 20,
                  resizeMode: 'stretch',
                }} />

              <View style={{ marginLeft: 20, marginBottom: 10 }}>
                <Text style={{ color: '#ffec00', fontSize: 14, fontWeight: 'bold', marginRight: 40, width: 100 }}>{grupo.Descricao}</Text>
                {/*<Text style={{ color: 'white', fontSize: 10, marginRight: 40 }}>178 DESTINOS</Text>*/}
              </View>
            </TouchableOpacity>
          )
        } else {
          item.push(
            <TouchableOpacity
              onPress={() => {
                if (grupo.Descricao == 'Excursões') {
                  Linking.openURL('https://entremares.tur.br/destinos/excursoes/')
                  return
                }

                if (grupo.Descricao == 'Convênios') {
                  Linking.openURL('https://entremares.tur.br/destinos/convenio/')
                  return
                }

                this.select(grupo.Codigo, grupo.Descricao);
              }} style={{
                height: 150,
                width: '50%',
                borderTopRightRadius: 20,
                justifyContent: 'space-between',
                borderBottomWidth: 0.7,
                borderLeftWidth: 0.7,
                borderColor: '#ffec00'
              }}>
              <Image
                source={imgs[String(grupo.Descricao).replace(' ', '')]}
                style={{
                  height: 30,
                  width: 30,
                  marginLeft: 20,
                  marginTop: 20,
                  resizeMode: 'stretch',
                }} />

              <View style={{ marginLeft: 20, marginBottom: 10 }}>
                <Text style={{ color: '#ffec00', fontSize: 14, fontWeight: 'bold', marginRight: 40, width: 100 }}>{grupo.Descricao}</Text>
                {/*<Text style={{ color: 'white', fontSize: 10, marginRight: 40 }}>178 DESTINOS</Text>*/}
              </View>
            </TouchableOpacity>
          )
        }
      } else {
        if (i == 1) {
          item.push(
            <TouchableOpacity
              onPress={() => {
                if (grupo.Descricao == 'Excursões') {
                  Linking.openURL('https://entremares.tur.br/destinos/excursoes/')
                  return
                }

                if (grupo.Descricao == 'Convênios') {
                  Linking.openURL('https://entremares.tur.br/destinos/convenio/')
                  return
                }

                this.select(grupo.Codigo, grupo.Descricao);
              }} style={{
                height: 150,
                width: '50%',
                justifyContent: 'space-between',
                borderRightWidth: 0.7,
                borderColor: '#ffec00'
              }}>
              <Image
                source={imgs[String(grupo.Descricao).replace(' ', '')]}
                style={{
                  height: 30,
                  width: 30,
                  marginLeft: 20,
                  marginTop: 20,
                  resizeMode: 'stretch',
                }} />

              <View style={{ marginLeft: 20, marginBottom: 10 }}>
                <Text style={{ color: '#ffec00', fontSize: 14, fontWeight: 'bold', marginRight: 40, width: 100 }}>{grupo.Descricao}</Text>
                {/*<Text style={{ color: 'white', fontSize: 10, marginRight: 40 }}>178 DESTINOS</Text>*/}
              </View>
            </TouchableOpacity>
          )
        } else {
          item.push(
            <TouchableOpacity
              onPress={() => {
                if (grupo.Descricao == 'Excursões') {
                  Linking.openURL('https://entremares.tur.br/destinos/excursoes/')
                  return
                }

                if (grupo.Descricao == 'Convênios') {
                  Linking.openURL('https://entremares.tur.br/destinos/convenio/')
                  return
                }

                this.select(grupo.Codigo, grupo.Descricao);
              }} style={{
                height: 150,
                width: '50%',
                justifyContent: 'space-between',
                borderLeftWidth: 0.7,
                borderColor: '#ffec00'
              }}>
              <Image
                source={imgs[String(grupo.Descricao).replace(' ', '')]}
                style={{
                  height: 30,
                  width: 30,
                  marginLeft: 20,
                  marginTop: 20,
                  resizeMode: 'stretch',
                }} />

              <View style={{ marginLeft: 20, marginBottom: 10 }}>
                <Text style={{ color: '#ffec00', fontSize: 14, fontWeight: 'bold', marginRight: 40, width: 100 }}>{grupo.Descricao}</Text>
                {/*<Text style={{ color: 'white', fontSize: 10, marginRight: 40 }}>178 DESTINOS</Text>*/}
              </View>
            </TouchableOpacity>
          )
        }
      }


      if (i == 2) {
        rows.push(
          <View style={{ flexDirection: 'row' }}>
            {item}
          </View>
        )

        item = []
        i = 0
      }
    }

    return rows
  }

  render() {
    return (
      <View style={{ backgroundColor: '#001e46', height: '100%' }}>

        <StatusBar
          barStyle="light-content"
          backgroundColor='#001e46'
        />

        <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginLeft: 20,
          marginTop: 40,
          alignItems: 'center'
        }}>
          <TouchableOpacity onPress={() => {
            try {
              this.props.screenProps.myDrawerNavigation.toggleDrawer();
            } catch (e) {
              this.props.navigation.toggleDrawer();
            }
          }}>
            <Iconr name="menu" color='#ffec00' style={{ padding: 5 }} size={40} />
          </TouchableOpacity>

          <View>
            <Text style={{ color: 'white', fontSize: 12, marginRight: 40 }}>ESCOLHA SEU</Text>
            <Text style={{ color: '#ffec00', fontSize: 18, fontWeight: 'bold', marginRight: 40 }}>DESTINO</Text>
          </View>
        </View>

        <ScrollView style={{ backgroundColor: '#001e46', height: '80%' }}>
          {this.state.grupos.length > 0 ?
            <View style={{
              height: 600,
              marginHorizontal: 40,
              marginVertical: 10,
              borderWidth: 0.7,
              borderColor: '#ffec00',
              borderRadius: 20
            }}>
              {this.rowitem()}
            </View>
            : <ActivityIndicator size={'small'} color={'#ffec00'} style={{ flex: 1 }} />
          }

        </ScrollView>

        <View style={{ flexDirection: 'row', height: Platform.OS === 'android' ? 55 : 75, backgroundColor: '#ffec00', alignItems: 'center' }}>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('MenuDestino1') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rdestino.png')}
              style={{
                height: 50,
                width: 50,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {
            if (this.state.idUsuario == '0') {
              Alert.alert(
                'Confirmação',
                'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                [
                  { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                  { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                ]
              );
            } else {
              this.props.navigation.navigate('Inicio')
            }
          }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rreservas.png')}
              style={{
                height: 50,
                width: 50,
                marginBottom: 6,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>
          <TouchableOpacity style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rpontos.png')}
              style={{
                height: 50,
                width: 50,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {
            if (this.state.idUsuario == '0') {
              Alert.alert(
                'Confirmação',
                'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                [
                  { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                  { text: 'SIM', onPress: () => { this.props.navigation.navigate('Login') } },
                ]
              );
            } else {
              this.props.navigation.navigate('MinhaConta')
            }
          }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rconta.png')}
              style={{
                height: 50,
                width: 50,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>

        </View>

      </View>
    )
  }
}

export default MenuDestino1;


const styles = StyleSheet.create({
  gridView: {
    height: 300,
  },
  itemContainer: {
    elevation: 4,
    height: 70,
    marginHorizontal: 10,
    marginBottom: 5,
  },
  itemContainerItem: {
    elevation: 4,
    height: 50,
    marginLeft: 45,
    marginBottom: 5,
  },
  itemContainerProfissa: {
    elevation: 4,
    height: 100,
    marginLeft: 45,
    marginRight: -5,
    marginBottom: 5,
  },

});