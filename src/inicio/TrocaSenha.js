import React, { Component } from "react";
import {
    View,
    ScrollView,
    Text,
    StyleSheet,
    TouchableOpacity,
    Alert,
    ToastAndroid,
    StatusBar,
    Platform,
    NetInfo,
} from "react-native";
import Icon from 'react-native-vector-icons/AntDesign';
import IconF from 'react-native-vector-icons/Feather';
import { Jiro } from 'react-native-textinput-effects';

class TrocaSenha extends React.Component {
    state = {
        visible: false,
        atual: '',
        nova: '',
        conNova: '',
        idUsuario: '',
        dataSource: [],
    }

    componentDidMount() {
        var RNFS = require('react-native-fs');
        var path = RNFS.DocumentDirectoryPath + '/usuario.json';

        RNFS.readFile(path, 'utf8')
            .then((contents) => {
                console.warn(contents);
                this.setState({
                    dataSource: JSON.parse(contents.replace('null,', '')),
                    idUsuario: JSON.parse(contents)[0].idUsuario,
                });
            })
            .catch((err) => {
                console.log(err.message, err.code);
            });
    }

    trocaSenha = async () => {
        if (!(this.state.atual == '') & !(this.state.nova == '') & !(this.state.conNova == '')) {
            if (this.state.nova == this.state.conNova) {
                const isConnected = await checkInternetConnection();
                if (isConnected) {
                    fetch('http://entremares.com.br/api/senha.php?atual=' +
                        this.state.atual + '&nova=' + this.state.nova + '&idUsuario=' + this.state.idUsuario, {}
                    ).then((response) => response.json())
                        .then((responseJson) => {

                            if (responseJson.SENHA == 'SUCESSO') {
                                ToastAndroid.showWithGravity(
                                    'Senha alterada com sucesso',
                                    ToastAndroid.SHORT,
                                    ToastAndroid.CENTER,
                                );
                                this.props.navigation.navigate('Inicio')
                            } else {
                                alert(responseJson.SENHA);
                            }
                        })
                        .catch((error) => {
                            console.error(error);
                        });

                } else {
                    ToastAndroid.showWithGravity(
                        'Sem conexão com a internet',
                        ToastAndroid.SHORT,
                        ToastAndroid.CENTER,
                    );
                }
            } else {
                ToastAndroid.showWithGravity(
                    '"Nova Senha" e "Confirma Senha" não coecidem',
                    ToastAndroid.SHORT,
                    ToastAndroid.CENTER,
                );
            }
        } else {
            ToastAndroid.showWithGravity(
                'Preencha os campos',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
            );
        }
    }

    handleFirstConnectivityChange = isConnected => {
        NetInfo.isConnected.removeEventListener(
            "connectionChange",
            this.handleFirstConnectivityChange
        );

        if (isConnected === false) {
            if (!this.state.dinheiro) {
                ToastAndroid.showWithGravity(
                    'Sem conexão',
                    ToastAndroid.SHORT,
                    ToastAndroid.CENTER,
                );
            }
        } else {
            fetch('http://entremares.com.br/api/senha.php?atual=' +
                this.state.atual + '&nova=' + this.state.nova + '&idUsuario=' + this.state.idUsuario, {}
            ).then((response) => response.json())
                .then((responseJson) => {

                    if (responseJson.SENHA == 'SUCESSO') {
                        ToastAndroid.showWithGravity(
                            'Senha alterada com sucesso',
                            ToastAndroid.SHORT,
                            ToastAndroid.CENTER,
                        );
                        this.props.navigation.navigate('Inicio')
                    } else {
                        alert(responseJson.SENHA);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }
    };


    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'space-between' }}>
                <StatusBar
                    barStyle="dark-content"
                    backgroundColor='#ffec00'
                />

                <View style={{ backgroundColor: '#282827', height: '100%' }}>
                    <View style={{
                        height: 90,
                        backgroundColor: '#ffec00',
                        flexDirection: 'row',
                        paddingRight: 40

                    }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Email')}
                            style={{
                                flexDirection: 'row',
                                marginHorizontal: 20,
                                marginTop: 15,
                                height: 38,
                                borderRadius: 28,
                                backgroundColor: '#282827'
                            }}>
                            <IconF name="arrow-left" color='#ffec00' style={{ padding: 5 }} size={28} />
                        </TouchableOpacity>


                        <Text style={{ color: '#282827', fontSize: 20, marginTop: 15, marginRight: 50 }}>Troque sua
                    <Text style={{ color: '#282827', fontWeight: 'bold', fontSize: 20 }}> senha, </Text>
                            informando os campos abaixo!</Text>
                    </View>

                    <Jiro
                        style={styles.desingnInput}
                        label={'Senha Atual'}
                        borderColor={'#ffec00'}
                        labelStyle={{ color: '#ffec00' }}
                        value={this.state.atual}
                        onChangeText={atual => this.setState({ atual })}
                        inputStyle={{ color: '#282827' }}
                        secureTextEntry={true}
                    />

                    <Jiro
                        style={styles.desingnInput}
                        label={'Nova Senha'}
                        borderColor={'#fffa65'}
                        labelStyle={{ color: '#fffa65' }}
                        inputStyle={{ color: '#282827' }}
                        value={this.state.nova}
                        onChangeText={nova => this.setState({ nova })}
                        secureTextEntry={true}
                    />

                    <Jiro
                        style={styles.desingnInput}
                        label={'Confirma nova senha'}
                        labelStyle={{ color: '#fffa65' }}
                        borderColor={'#fffa65'}
                        inputStyle={{ color: '#282827' }}
                        value={this.state.conNova}
                        onChangeText={conNova => this.setState({ conNova })}
                        secureTextEntry={true}
                    />

                </View>

                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    position: 'absolute',
                    height: '192%',
                    width: '100%',
                }}>
                    <TouchableOpacity style={styles.rodape2} onPress={() => this.props.navigation.navigate('Inicio')}>
                        <Icon name="close" color='white' size={28} />
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.rodape} onPress={this.trocaSenha}>
                        <Icon name="check" color='white' size={28} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default TrocaSenha;
const styles = StyleSheet.create({
    desingnInput: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 0,
    },
    rodape: {
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 4,
        marginBottom: 20,
        marginRight: 20,
        borderRadius: 60,
        backgroundColor: '#26de81',
        width: 60,
        height: 60,
    },
    rodape2: {
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 4,
        marginBottom: 20,
        marginLeft: 20,
        borderRadius: 60,
        backgroundColor: '#eb4d4b',
        width: 60,
        height: 60,
    },

});