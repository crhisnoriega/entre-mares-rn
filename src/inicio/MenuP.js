import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  TextInput,
  Image,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
  StatusBar,
  Platform,
  NetInfo,
  ToastAndroid,
  BackHandler
} from "react-native";
import GridView from 'react-native-super-grid';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/EvilIcons';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import Carousel from 'react-native-snap-carousel'
import { Searchbar, Provider as PaperProvider } from 'react-native-paper';
import Navigator from '../../src';

class MenuP extends React.Component {
  state = {
    visible: false,
    destino: '',
    qtdAdulto: 0,
    qtdCrianca: 0,
    bloqueio: 0,
    populares: [{ "cidade": "florianópolis - sc", "local": "residencial campeche", "ava": 4.5, "valor": 35.00 },
    { "cidade": "florianópolis - sc", "local": "clube house", "ava": 3.0, "valor": 25.00 },
    { "cidade": "florianópolis - sc", "local": "residencial nautiplus", "ava": 4.8, "valor": 45.00 },
    { "cidade": "florianópolis - sc", "local": "residencial city", "ava": 4.5, "valor": 29.00 }],
    sugestao: [{ "nome": 'praias' }, { "nome": 'hoteis' }, { "nome": 'condominios' }, { "nome": 'residência' }]
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.navigate('Login')
      return true;
    })
    var RNFS = require('react-native-fs');
    var path = RNFS.DocumentDirectoryPath + '/usuario.json';

    RNFS.readFile(path, 'utf8')
      .then((contents) => {
        if (!(contents == '[{}]')) {
          this.setState({
            idUsuario: String(JSON.parse(contents)[0].codigo),
            tipo: JSON.parse(contents)[0].tipo,
            bloqueio: JSON.parse(contents)[0].bloqueio,
            token: JSON.parse(contents)[0].token
          })
        } else {
          Alert.alert(
            'Confirmação',
            'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
            [
              { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
              { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
            ]
          );
          this.props.navigation.navigate('MenuDestino1')
          this.setState({ idUsuario: '0' })
        }
      })
      .catch((err) => {
        this.setState({ idUsuario: '0' })
        alert(err.message)
        console.log(err.message, err.code);
      });
  }

  render() {

    return (
      <View style={{ backgroundColor: '#001e46', height: '100%' }}>

        <StatusBar
          barStyle="light-content"
          backgroundColor='#001e46'
        />

        <TouchableOpacity onPress={() => {
          try {
            this.props.screenProps.myDrawerNavigation.toggleDrawer();
          } catch (e) {
            this.props.navigation.toggleDrawer();
          }
        }} style={{ marginLeft: 20, marginTop: 40 }}>
          <Iconr name="menu" color='#ffec00' style={{ padding: 5 }} size={40} />
        </TouchableOpacity>

        <ScrollView style={{ backgroundColor: '#001e46', height: '100%' }}>
          <View style={{ marginLeft: 40, marginTop: 20 }}>
            <Text style={{ color: '#ffec00', fontSize: 19, marginBottom: -5, marginRight: 40 }}>O CLUBE DE FÉRIAS</Text>
            <Text style={{ color: 'white', fontSize: 32, fontWeight: 'bold', marginRight: 40 }}>MAIS COMPLETO</Text>
            <Text style={{ color: '#ffec00', fontSize: 35, marginRight: 40, marginTop: -10 }}>DO BRASIL</Text>
            <View style={{ width: 20, height: 5, backgroundColor: 'white' }} />
          </View>

          <TouchableOpacity onPress={() => { 
            this.state.bloqueio == 1 ? this.props.navigation.navigate('covid') : this.props.navigation.navigate('Inicio') }} style={{ marginTop: 40, }}>
            <View
              style={{
                borderColor: '#ffec00',
                flexDirection: 'row',
                alignItems: 'center',
                borderWidth: 0.7,
                borderRadius: 20,
                marginHorizontal: 40,
                height: 200
              }}>
              <View style={{ flex: 1, marginRight: -40 }}>
                <View
                  style={{
                    width: '70%',
                    height: '60%',
                    backgroundColor: '#ffec00',

                  }} />

                <Image
                  source={require('../img/reservas1.png')}
                  style={{
                    height: '85%',
                    width: '78%',
                    marginTop: -25,
                    position: 'absolute',
                    resizeMode: 'stretch',
                  }} />
              </View>

              <View>
                <View style={{ width: 20, height: 5, backgroundColor: '#ffec00' }} />
                <Text style={{ color: 'white', fontSize: 14, marginBottom: -5, marginRight: 25 }}>FAÇA SUA</Text>
                <Text style={{ color: '#ffec00', fontSize: 27, fontWeight: 'bold', marginRight: 25 }}>RESERVA</Text>
                <Text style={{ color: 'white', fontSize: 8, marginRight: 25 }}>UNIDADES ENTREMARES</Text>
              </View>
            </View>

            <View style={{
              width: '50%',
              height: 10,
              backgroundColor: '#ffec00',
              marginLeft: 70,
              marginTop: -5
            }} />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => { this.state.bloqueio == 1 ? this.props.navigation.navigate('covid') : this.props.navigation.navigate('MenuDestino1') }} style={{ marginTop: 40, marginBottom: 20 }}>
            <View
              style={{
                borderColor: '#ffec00',
                flexDirection: 'row',
                alignItems: 'center',
                borderWidth: 0.7,
                borderRadius: 20,
                marginHorizontal: 40,
                height: 200
              }}>
              <View style={{ flex: 1, marginRight: -40 }}>
                <View
                  style={{
                    width: '70%',
                    height: '60%',
                    backgroundColor: '#ffec00'
                  }} />

                <Image
                  source={require('../img/destino1.png')}
                  style={{
                    marginTop: -35,
                    height: '85%',
                    width: '80%',
                    position: 'absolute',
                    resizeMode: 'stretch'
                  }} />
              </View>

              <View>
                <View style={{ width: 20, height: 5, backgroundColor: '#ffec00' }} />
                <Text style={{ color: 'white', fontSize: 14, marginBottom: -5, marginRight: 25 }}>ESCOLHA SEU</Text>
                <Text style={{ color: '#ffec00', fontSize: 27, fontWeight: 'bold', marginRight: 25 }}>DESTINO</Text>
                <Text style={{ color: 'white', fontSize: 8, marginRight: 25, width: 100 }}>PESQUISAR SEU DESTINOS</Text>
              </View>
            </View>

            <View style={{
              width: '50%',
              height: 10,
              backgroundColor: '#ffec00',
              marginLeft: 70,
              marginTop: -5
            }} />
          </TouchableOpacity>
        </ScrollView>
      </View>
    )
  }
}

export default MenuP;
const styles = StyleSheet.create({
  gridView: {
    height: 300,
  },
  itemContainer: {
    elevation: 4,
    height: 70,
    marginHorizontal: 10,
    marginBottom: 5,
  },
  itemContainerItem: {
    elevation: 4,
    height: 50,
    marginLeft: 45,
    marginBottom: 5,
  },
  itemContainerProfissa: {
    elevation: 4,
    height: 100,
    marginLeft: 45,
    marginRight: -5,
    marginBottom: 5,
  },

});