import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  TextInput,
  Image,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
  StatusBar,
  Platform,
  NetInfo,
  ToastAndroid,
  BackHandler
} from "react-native";
import GridView from 'react-native-super-grid';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/EvilIcons';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import Carousel from 'react-native-snap-carousel'
import RNFetchBlob from 'rn-fetch-blob';
import { Searchbar, Provider as PaperProvider } from 'react-native-paper';

class MessageAguardar extends React.Component {
  state = {
    visible: false,
    cidade: '',
    diaIni: '',
    diaFim: '',
    adultos: '',
    criancas: ''
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.navigate('MenuP')
      return true;
  })

    var RNFS = require('react-native-fs');
    var path = RNFS.DocumentDirectoryPath + '/reservaselect.json';

    RNFS.readFile(path, 'utf8')
      .then((contents) => {
        if (!(contents == '[{}]')) {
          this.setState({
            "cidade": JSON.parse(contents)[0].cidade,
            "diaIni": JSON.parse(contents)[0].diaIni,
            "diaFim": JSON.parse(contents)[0].diaFim,
            "adultos": JSON.parse(contents)[0].adultos,
            "criancas": JSON.parse(contents)[0].criancas
          })
        } else {
          this.setState({ idUsuario: '0' })
        }
      })
      .catch((err) => {
        this.setState({ idUsuario: '0' })
        console.log(err.message, err.code);
      });
  }

  render() {

    return (
      <View style={{ backgroundColor: '#ffec00', height: '100%' }}>

        <StatusBar
          barStyle="dark-content"
          backgroundColor='#ffec00'
        />

        <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginLeft: 20,
          marginTop: 40,
          alignItems: 'center'
        }}>
          <TouchableOpacity onPress={() => {
            try {
              this.props.screenProps.myDrawerNavigation.toggleDrawer();
            } catch (e) {
              this.props.navigation.toggleDrawer();
            }
          }}>
            <Iconr name="menu" color='#001e46' style={{ padding: 5 }} size={40} />
          </TouchableOpacity>
        </View>

        <ScrollView style={{ backgroundColor: '#ffec00', height: '100%' }}>
          <View
            style={{
              height: 280,
              marginHorizontal: 29,
              borderWidth: 0.7,
              borderColor: '#001e46',
              borderRadius: 20,
              marginTop: 20,
              backgroundColor: '#ffec00',
              elevation: 4,
            }}>
            <View>
              <Text style={{ color: '#001e46', fontSize: 25, fontWeight: 'bold', marginLeft: 15, marginTop: 15 }}>FALTA POUCO :)</Text>
              <Text style={{ color: '#001e46', fontSize: 25, fontWeight: 'bold', marginLeft: 15, marginTop: -5 }}>AGORA É SÓ</Text>
              <Text style={{ color: '#001e46', fontSize: 25, fontWeight: 'bold', marginLeft: 15, marginTop: -5 }}>AGUARDAR</Text>
            </View>

            <View style={{
              borderBottomWidth: 1,
              borderColor: '#001e46',
              marginTop: 10,
              marginRight: 30,
              marginHorizontal: 15
            }} />

            <Text style={{
              color: '#001e46',
              fontSize: 10,
              fontWeight: 'bold',
              marginLeft: 15,
              marginTop: 10
            }}>CASO QUEIRA VERIFICAR SUA SOLICITAÇÃO</Text>

            <View style={{ flexDirection: 'row', marginTop: 6, alignItems: 'flex-end' }}>
              <Text style={{
                color: '#001e46',
                fontSize: 8,
                marginLeft: 15,
              }}>APENAS CLIQUE NESSE BOTÃO NO TOPO </Text>
              <Iconr name="menu" color='#001e46' size={20} />
            </View>
            <Text style={{
              color: '#001e46',
              fontSize: 8,
              marginLeft: 15,
            }}>SELECIONE A OPÇÃO <Text style={{ fontWeight: 'bold' }}>MINHAS RESERVAS</Text></Text>
            <Text style={{
              color: '#001e46',
              fontSize: 8,
              marginLeft: 15,
            }}>LOGO APÓS SELECIONE A OPÇÃO <Text style={{ fontWeight: 'bold' }}>MINHAS SOLICITAÇÕES</Text></Text>

            <View style={{
              borderBottomWidth: 1,
              borderColor: '#001e46',
              marginTop: 10,
              marginRight: 30,
              marginHorizontal: 15
            }} />

            <Text style={{
              color: '#001e46',
              fontSize: 10,
              marginLeft: 15,
              marginTop: 20
            }}>{this.state.diaIni} - {this.state.diaFim}</Text>
            <Text style={{
              color: '#001e46',
              fontSize: 8,
              marginLeft: 15,
            }}>{this.state.adultos} ADULTOS | {this.state.criancas} CRIANÇA ATÉ 4 ANOS</Text>
            <Text style={{
              color: '#001e46',
              fontSize: 10,
              fontWeight: 'bold',
              marginLeft: 15,
            }}>{this.state.cidade}</Text>
          </View>

          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 29 }}>
            <View />
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('MenuP') }} style={{
              height: 30,
              width: 140,
              paddingHorizontal: 10,
              justifyContent: 'center',
              paddingVertical: 5,
              marginLeft: 29,
              marginTop: 15,
              marginBottom: 25,
              borderRadius: 30,
              borderWidth: 0.8,
              borderColor: '#001e46',
              backgroundColor: '#ffec00'
            }}>
              <Text style={{
                color: '#001e46',
                fontSize: 12,
                fontWeight: 'bold',
                textAlign: 'center'
              }}>Continuar navegando</Text>
            </TouchableOpacity>
          </View>

          <View />
        </ScrollView>
        <View style={{ flexDirection: 'row', height: Platform.OS === 'android' ? 55 : 75, backgroundColor: '#ffec00', alignItems: 'center' }}>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('MenuDestino1') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rdestino.png')}
              style={{
                height: 50,
                width: 50,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('Inicio') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rreservas.png')}
              style={{
                height: 50,
                width: 50,
                marginBottom: 6,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>
          <TouchableOpacity style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rpontos.png')}
              style={{
                height: 50,
                width: 50,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('MinhaConta') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rconta.png')}
              style={{
                height: 50,
                width: 50,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>

        </View>

      </View>
    )
  }
}

export default MessageAguardar;
const styles = StyleSheet.create({
  gridView: {
    height: 300,
  },
  itemContainer: {
    elevation: 4,
    height: 70,
    marginHorizontal: 10,
    marginBottom: 5,
  },
  itemContainerItem: {
    elevation: 4,
    height: 50,
    marginLeft: 45,
    marginBottom: 5,
  },
  itemContainerProfissa: {
    elevation: 4,
    height: 100,
    marginLeft: 45,
    marginRight: -5,
    marginBottom: 5,
  },

});