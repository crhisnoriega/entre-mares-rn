import React, { Component } from "react";
import {
    ScrollView,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ToastAndroid,
    ActivityIndicator,
    Alert,
    Image,
    StatusBar,
    Platform,
    BackHandler,
} from "react-native";
import { checkInternetConnection } from 'react-native-offline';
import Icon from 'react-native-vector-icons/AntDesign';
import IconO from 'react-native-vector-icons/Octicons';
import IconF from 'react-native-vector-icons/FontAwesome'
import IconM from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import API, { caminhoAPI, versaoAPI } from '../utils/caminhosAPI'

class MinhaConta extends React.Component {
    state = {
        dataSource: [],
        usuario: [],
        tem: false,
        loading: false,
        vendedor: false
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('MenuP')
            return true;
        })

        var RNFS = require('react-native-fs');
        var path = RNFS.DocumentDirectoryPath + '/usuario.json';

        RNFS.readFile(path, 'utf8')
            .then(async (contents) => {
                console.warn(contents);
                if (!(contents == '[{}]')) {
                    console.warn(contents);
                    this.setState({
                        dataSource: JSON.parse(contents),
                    });

                    const isConnected = await checkInternetConnection();
                    if (isConnected) {
                        fetch(caminhoAPI() + '/recuperaDadosCliente', {
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                                'Auth': 'Bearer ' + this.state.dataSource[0].token,
                                'Versao': versaoAPI
                            },
                        }
                        ).then((response) => response.json())
                            .then((responseJson) => {
                                this.setState({
                                    tem: true,
                                    usuario: responseJson
                                })
                            })
                            .catch((error) => {
                                this.setState({
                                    vazio: true,
                                    grupos: [],
                                    refresh: false
                                })
                            });

                    } else {
                        alert('Sem conexão com a internet!')
                        this.setState({
                            vazio: true,
                            refresh: false
                        })
                    }
                } else {
                    Alert.alert(
                        'Confirmação',
                        'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                        [
                            { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                            { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                        ]
                    );
                    this.props.navigation.navigate('MenuDestino1')
                }

            })
            .catch((err) => {
                console.log(err.message, err.code);
            });


    }

    trocaSenha = async () => {
        const isConnected = await checkInternetConnection();
        if (isConnected) {
            this.props.navigation.navigate('TrocaSenha')
        } else {
            ToastAndroid.showWithGravity(
                'Sem conexão com a internet',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
            );
        }
    }

    handleFirstConnectivityChange = isConnected => {
        NetInfo.isConnected.removeEventListener(
            "connectionChange",
            this.handleFirstConnectivityChange
        );

        if (isConnected === false) {
            if (!this.state.dinheiro) {
                ToastAndroid.showWithGravity(
                    'Sem conexão',
                    ToastAndroid.SHORT,
                    ToastAndroid.CENTER,
                );
            }
        } else {
            this.props.navigation.navigate('TrocaSenha')
        }
    };

    Logout = () => {
        var RNFS = require('react-native-fs');
        var usuario = RNFS.DocumentDirectoryPath + '/usuario.json';
        let dados = [];
        dados.push({})

        RNFS.writeFile(usuario, JSON.stringify(dados), 'utf8')
            .then((success) => {
                console.log('FILE WRITTEN!');
            })
            .catch((err) => {
                console.log(err.message);
            });

        this.props.navigation.navigate('Login');
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#ffec00' }}>
                <StatusBar
                    barStyle="dark-content"
                    backgroundColor='#ffec00'
                />
                <ScrollView style={{ height: '100%' }}>
                    <View style={{ justifyContent: 'space-between', flex: 1, backgroundColor: '#ffec00' }}>
                        {!(this.state.tem) ?

                            <View>
                                <TouchableOpacity onPress={() => {
                                    try {
                                        this.props.screenProps.myDrawerNavigation.toggleDrawer();
                                    } catch (e) {
                                        this.props.navigation.toggleDrawer();
                                    }
                                }} style={{ marginLeft: 20, marginTop: 40 }}>
                                    <Iconr name="menu" color='#001e46' style={{ padding: 5 }} size={40} />
                                </TouchableOpacity>
                                <ActivityIndicator color={'#001e46'} size="small" style={{ backgroundColor: '#ffec00' }} />
                            </View>
                            :
                            <View style={{ marginBottom: 80, backgroundColor: '#ffec00' }}>
                                <TouchableOpacity onPress={() => {
                                    try {
                                        this.props.screenProps.myDrawerNavigation.toggleDrawer();
                                    } catch (e) {
                                        this.props.navigation.toggleDrawer();
                                    }
                                }} style={{ marginLeft: 20, marginTop: 40 }}>
                                    <Iconr name="menu" color='#001e46' style={{ padding: 5 }} size={40} />
                                </TouchableOpacity>
                                <View
                                    style={{
                                        backgroundColor: '#ffec00',
                                        height: 95,
                                        width: '100%',
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        elevation: 3,
                                        alignItems: 'center'
                                    }}>
                                    <View>
                                        <Text style={[styles.TextTop, { marginLeft: 20, }]}>
                                            {String(this.state.usuario.Nome).toUpperCase()}
                                        </Text>
                                        <Text style={[styles.TextBaixo, { marginLeft: 20, marginTop: -1 }]}>
                                            {String(this.state.dataSource[0].tipo).toLocaleUpperCase()}
                                        </Text>
                                    </View>

                                    <TouchableOpacity style={{
                                        marginRight: 20,
                                        marginTop: 10,
                                        height: 60,
                                        width: 60,
                                        borderRadius: 60,
                                        backgroundColor: '#001e46',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                    }}>
                                        <Image
                                            source={require('../img/perfil.jpeg')}
                                            style={{
                                                height: 60,
                                                width: 60,
                                                borderRadius: 60,
                                                resizeMode: 'stretch',
                                            }} />
                                    </TouchableOpacity>

                                </View>

                                <TouchableOpacity onPress={() => { }/*this.props.navigation.navigate('AlteraCadastro')*/} style={[styles.desingnIten, { marginTop: 20, }]}>
                                    <Icon style={{ marginLeft: 30 }} name="idcard" color='#001e46' size={18} />
                                    <View style={{ justifyContent: 'center', width: '60%', }}>
                                        <Text style={[styles.TextTitle, { fontWeight: 'bold' }]}>
                                            {String(this.state.usuario.Nome).toUpperCase()}
                                        </Text>
                                        <Text style={styles.TextTitle}>
                                            {this.state.dataSource[0].email}
                                        </Text>
                                        <Text style={[styles.TextTitle, { fontSize: 10 }]}>
                                            {String(this.state.dataSource[0].tipo).toLocaleUpperCase()}
                                        </Text>
                                    </View>
                                    <Icon style={{ marginRight: 10 }} name="edit" color='#001e46' size={16} />
                                </TouchableOpacity>

                                <View
                                    style={{
                                        borderBottomColor: '#001e46',
                                        borderBottomWidth: 0.5,
                                        marginVertical: 1,
                                        marginLeft: 80,
                                    }}
                                />

                                <TouchableOpacity onPress={() => {/*(this.trocaSenha)*/ }} style={[styles.desingnIten, { marginTop: 0, }]}>
                                    <Icon style={{ marginLeft: 30 }} name="enviroment" color='#001e46' size={18} />
                                    <View style={{ justifyContent: 'center', width: '60%', }}>
                                        <Text style={[styles.TextTitle, { fontWeight: 'bold' }]}>
                                            Endereço
                        </Text>
                                        <Text style={[styles.TextTitle, { fontSize: 10 }]}>
                                            {this.state.usuario.Endereco == '' ? 'Endereço não inforado' : this.state.usuario.Endereco}
                                        </Text>
                                        <Text style={[styles.TextTitle, { fontSize: 10 }]}>
                                            {this.state.usuario.Numero == '' ? 'Numero não inforado' : this.state.usuario.Numero}
                                        </Text>
                                        <Text style={[styles.TextTitle, { fontSize: 10 }]}>
                                            {this.state.usuario.Bairro == '' ? 'Bairro não inforado' : this.state.usuario.Bairro}
                                        </Text>
                                        <Text style={[styles.TextTitle, { fontSize: 10 }]}>
                                            {this.state.usuario.Cidade == '' ? 'Cidade não inforado' : this.state.usuario.Cidade} - {this.state.usuario.UF == '' ? 'UF' : this.state.usuario.UF}
                                        </Text>
                                    </View>
                                    <Icon style={{ marginRight: 10 }} name="edit" color='#001e46' size={16} />
                                </TouchableOpacity>

                                <View
                                    style={{
                                        borderBottomColor: '#001e46',
                                        borderBottomWidth: 0.5,
                                        marginVertical: 1,
                                        marginLeft: 80,
                                    }}
                                />

                                <TouchableOpacity onPress={() => {/*(this.trocaSenha)*/ }} style={[styles.desingnIten, { marginTop: 0, }]}>
                                    <Icon style={{ marginLeft: 30 }} name="key" color='#001e46' size={18} />
                                    <View style={{ justifyContent: 'center', width: '60%', }}>
                                        <Text style={[styles.TextTitle, { fontWeight: 'bold' }]}>
                                            Senha
                        </Text>
                                        <Text style={styles.TextTitle}>
                                            Toque para trocar sua senha
                        </Text>
                                    </View>
                                    <Icon style={{ marginRight: 10 }} name="edit" color='#001e46' size={16} />
                                </TouchableOpacity>

                                <View
                                    style={{
                                        borderBottomColor: '#001e46',
                                        borderBottomWidth: 0.5,
                                        marginVertical: 1,
                                        marginLeft: 80,
                                    }}
                                />

                                <TouchableOpacity onPress={this.Logout} style={[styles.desingnIten, { marginTop: 0, }]}>
                                    <IconM style={{ marginLeft: 30 }} name="exit-run" color='#001e46' size={18} />
                                    <View style={{ justifyContent: 'center', width: '60%', }}>
                                        <Text style={[styles.TextTitle, { fontWeight: 'bold' }]}>
                                            Logout
                        </Text>
                                        <Text style={styles.TextTitle}>
                                            Toque para deslogar da aplicação
                        </Text>
                                    </View>
                                    <Icon style={{ marginRight: 10 }} name="closecircleo" color='#001e46' size={16} />
                                </TouchableOpacity>

                                <View
                                    style={{
                                        borderBottomColor: '#001e46',
                                        borderBottomWidth: 0.5,
                                        marginLeft: 80,
                                    }}
                                />

                            </View>
                        }
                    </View>
                </ScrollView>

                <View style={{ flexDirection: 'row', height: Platform.OS === 'android' ? 55 : 75, backgroundColor: '#ffec00', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('MenuDestino1') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rdestino.png')}
                            style={{
                                height: 50,
                                width: 50,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        if (this.state.idUsuario == '0') {
                            Alert.alert(
                                'Confirmação',
                                'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                                [
                                    { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                                    { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                                ]
                            );
                        } else {
                            this.props.navigation.navigate('Inicio')
                        }
                    }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rreservas.png')}
                            style={{
                                height: 50,
                                width: 50,
                                marginBottom: 6,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rpontos.png')}
                            style={{
                                height: 50,
                                width: 50,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        if (this.state.idUsuario == '0') {
                            Alert.alert(
                                'Confirmação',
                                'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                                [
                                    { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                                    { text: 'SIM', onPress: () => { this.props.navigation.navigate('Login') } },
                                ]
                            );
                        } else {
                            this.props.navigation.navigate('MinhaConta')
                        }
                    }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rconta.png')}
                            style={{
                                height: 50,
                                width: 50,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>

                </View>


            </View>
        );
    }
}

export default MinhaConta;
const styles = StyleSheet.create({
    ActivityIndicator_Style: {

        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center'
    },
    desingnIten: {
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 5,
        flexDirection: 'row',
        height: 80,
    },
    TextTitle: {
        color: '#001e46',
        marginLeft: 0,
    },
    TextTop: {
        fontWeight: 'bold',
        color: '#001e46',
        fontSize: 16,
    },
    TextBaixo: {
        color: '#001e46',
        marginTop: -5,
        fontSize: 11,
    },
    ViewTopText: {
        alignItems: 'center',
    },
    botaoEnd: {
        width: 40,
        height: 40,
        borderWidth: 1.5,
        borderColor: '#001e46',
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#001e46',
        marginHorizontal: 10,
        elevation: 5,
    },

});