import React, { Component } from "react";
import {
    ScrollView,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    StatusBar,
    Modal,
    Image,
    TextInput,
    Platform,
    ToastAndroid,
    NetInfo,
    ActivityIndicator,
    RefreshControl
} from "react-native";
import Iconm from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Entypo';
import GridView from 'react-native-super-grid';
import { Searchbar, Provider as PaperProvider } from 'react-native-paper';

class Avaliacao extends React.Component {
    state = {
        visible: false,
        visibleCancel: false,
        addVisible: false,
        refreshing: false,
        refresh: false,
        vazio: false,
        open: false,
        text: '',
        descri: '',
        firstQuery: '',
        e1: false,
        e2: false,
        e3: false,
        e4: false,
        e5: false,
        eCont: 0,
        idPedido: 0,
        vendedor: true,
        idUsuario: 0,
        cont: 0,
        dataPedido: [],
        itemSelect: []
    }

    componentDidMount() {
        var RNFS = require('react-native-fs');
        var path = RNFS.DocumentDirectoryPath + '/usuario.json';
    
        RNFS.readFile(path, 'utf8')
          .then((contents) => {
            if (!(contents == '[{}]')) {    
              this.setState({ idUsuario: JSON.parse(contents)[0].id})
            } else {
              this.setState({ idUsuario: '0' })
            }
          })
          .catch((err) => {
            this.setState({ idUsuario: '0' })
            console.log(err.message, err.code);
          });

        if (Platform.OS === "android") {
            NetInfo.isConnected.fetch().then(isConnected => {
                if (isConnected) {
                    fetch('http://clickresolve.com.br/trabalho/api/avaliacao/get.php', {}
                    ).then((response) => response.json())
                        .then((responseJson) => {
                            if (!(responseJson.message == 'não foi encontrado avaliações')) {
                                this.setState({
                                    dataPedido: responseJson.records,
                                    refresh: false
                                })
                            } else {
                                this.setState({
                                    vazio: true,
                                    refresh: false
                                })
                            }
                        })
                        .catch((error) => {
                            alert('ocorreu um erro');
                            this.setState({
                                vazio: true,
                                refresh: false
                            })
                        });

                } else {
                    alert('Sem conexão com a internet!')
                    this.setState({
                        vazio: true,
                        refresh: false
                    })
                }
            });
        } else {
            // For iOS devices
            NetInfo.isConnected.addEventListener(
                "connectionChange",
                this.handleFirstConnectivityChange
            );
        }
    }

    avaliar = () => {        
        const dados = []
        dados.push({
            "idUsuario": this.state.idUsuario,
            "idUsuarioAva": this.state.itemSelect.idUsuarioAva,
            "ava": this.state.eCont,
            "descricao": this.state.descri
        })

        if (this.state.descri == '') {
            alert('Faça uma breve descrição de como foi sua experiência')
            return
        }

        if (this.state.descri.length < 10) {
            alert('Descrição muito pequena')
            return
        }

        if (this.state.eCont == 0) {
            alert('Avalie clicando nas estrelas')
            return
        }

        this.setState({
            refresh: true
        })


        if (Platform.OS === "android") {
            NetInfo.isConnected.fetch().then(isConnected => {
                if (isConnected) {
                    fetch('http://clickresolve.com.br/trabalho/api/avaliacao/post.php', {
                        method: 'POST',
                                headers: {
                                    Accept: 'application/json',
                                    'Content-Type': 'application/json',
                                },
                                body: String(JSON.stringify(dados)).substr(1).substr(-(String(JSON.stringify(dados)).length - 1), (String(JSON.stringify(dados)).length - 2))
                    }
                    ).then((response) => response.json())
                        .then((responseJson) => {
                            if (!(responseJson.message == 'JSON incorreto')|!(responseJson.message == 'serviço api inoperante')) {
                                
                                this.atualizar()

                                this.setState({
                                    visible: false,
                                    refresh: false
                                })

                            } else {
                                this.setState({
                                    vazio: true,
                                    refresh: false
                                })
                            }
                        })
                        .catch((error) => {
                            alert('ocorreu um erro');
                            this.setState({
                                vazio: true,
                                refresh: false
                            })
                        });

                } else {
                    alert('Sem conexão com a internet!')
                    this.setState({
                        vazio: true,
                        refresh: false
                    })
                }
            });
        } else {
            // For iOS devices
            NetInfo.isConnected.addEventListener(
                "connectionChange",
                this.handleFirstConnectivityChange
            );
        }
    }

    atualizar = () =>{
        this.setState({
            refreshing: true
        })

        if (Platform.OS === "android") {
            NetInfo.isConnected.fetch().then(isConnected => {
                if (isConnected) {
                    fetch('http://clickresolve.com.br/trabalho/api/avaliacao/get.php', {}
                    ).then((response) => response.json())
                        .then((responseJson) => {
                            if (!(responseJson.message == 'não foi encontrado avaliações')) {
                                this.setState({
                                    dataPedido: responseJson.records,
                                    refreshing: false
                                })
                            } else {
                                this.setState({
                                    vazio: true,
                                    refreshing: false
                                })
                            }
                        })
                        .catch((error) => {
                            alert('ocorreu um erro');
                            this.setState({
                                vazio: true,
                                refreshing: false
                            })
                        });

                } else {
                    alert('Sem conexão com a internet!')
                    this.setState({
                        vazio: true,
                        refreshing: false
                    })
                }
            });
        } else {
            // For iOS devices
            NetInfo.isConnected.addEventListener(
                "connectionChange",
                this.handleFirstConnectivityChange
            );
        }
    }

    render() {
        return (
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.atualizar}
                    />
                }>

                <View style={{ flex: 1, backgroundColor: '#282827' }}>
                    <StatusBar
                        barStyle="light-content"
                        backgroundColor='#282827'
                    />

                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.visible}
                        onRequestClose={() => {
                            this.setState({ visible: false, });
                        }}>

                        <StatusBar
                            barStyle="light-content"
                            backgroundColor='black'
                        />

                        <View style={{
                            flex: 1,
                            backgroundColor: 'rgba(0,0,0,0.8)',
                            flexDirection: 'column',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            <View style={{
                                width: '85%',
                                height: 325,
                                borderRadius: 20,
                                justifyContent: 'space-between',
                                backgroundColor: 'white'
                            }}>
                                <View style={{
                                    backgroundColor: '#ffec00',
                                    borderTopRightRadius: 20,
                                    borderTopLeftRadius: 20
                                }}>
                                    <Text style={[styles.textInfo, { marginTop: 10, alignSelf: 'center', color: '#282827' }]}><Text style={[styles.TextBotao, { color: '#282827' }]}>Avalie</Text> como foi o <Text style={[styles.TextBotao, { color: '#282827' }]}>serviço</Text></Text>
                                    <Text style={[styles.textInfo, { alignSelf: 'center', marginBottom: 10, color: '#282827' }]}>e nos ajude a manter a <Text style={[styles.TextBotao, { color: '#282827' }]}>qualidade</Text></Text>
                                </View>

                                <View>
                                    <Text style={{
                                        color: '#282827',
                                        marginLeft: 20,
                                        fontSize: 10,
                                    }}>Descreva como foi sua experiência</Text>

                                    <TextInput
                                        multiline={true}
                                        value={this.state.descri}
                                        onChangeText={(descri) => { this.setState({ descri }) }}
                                        placeholder={'Descrição'}
                                        underlineColorAndroid='transparent'
                                        style={{
                                            marginHorizontal: 20,
                                            borderRadius: 5,
                                            backgroundColor: 'white',
                                            elevation: 3,

                                        }} />
                                </View>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 65 }}>
                                    <TouchableOpacity onPress={() => {
                                        this.setState({
                                            eCont: 1,
                                            e1: true,
                                            e2: false,
                                            e3: false,
                                            e4: false,
                                            e5: false,
                                        })
                                    }}>
                                        <Icon name={this.state.e1 ? "star" : "staro"} color='#f1c40f' size={32} />
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => {
                                        this.setState({
                                            eCont: 2,
                                            e1: true,
                                            e2: true,
                                            e3: false,
                                            e4: false,
                                            e5: false,
                                        })
                                    }}>
                                        <Icon name={this.state.e2 ? "star" : "staro"} color='#f1c40f' size={32} />
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => {
                                        this.setState({
                                            eCont: 3,
                                            e1: true,
                                            e2: true,
                                            e3: true,
                                            e4: false,
                                            e5: false,
                                        })
                                    }}>
                                        <Icon name={this.state.e3 ? "star" : "staro"} color='#f1c40f' size={32} />
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => {
                                        this.setState({
                                            eCont: 4,
                                            e1: true,
                                            e2: true,
                                            e3: true,
                                            e4: true,
                                            e5: false,
                                        })
                                    }}>
                                        <Icon name={this.state.e4 ? "star" : "staro"} color='#f1c40f' size={32} />
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => {
                                        this.setState({
                                            eCont: 5,
                                            e1: true,
                                            e2: true,
                                            e3: true,
                                            e4: true,
                                            e5: true,
                                        })
                                    }}>
                                        <Icon name={this.state.e5 ? "star" : "staro"} color='#f1c40f' size={32} />
                                    </TouchableOpacity>
                                </View>


                                <TouchableOpacity onPress={() => {
                                    this.avaliar()
                                }} style={{
                                    justifyContent: 'center',
                                    height: 45,
                                    backgroundColor: '#282827',
                                    borderRadius: 5,
                                    marginBottom: 20,
                                    marginHorizontal: 20,
                                    elevation: 3
                                }}>
                                {this.state.refresh ? <ActivityIndicator size='small' color='white' /> :
                                    <Text style={{
                                        fontWeight: 'bold',
                                        fontSize: 12,
                                        textAlign: 'center',
                                        color: 'white',
                                    }}>AVALIAR</Text>
                                    }
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>

                    <View style={{ backgroundColor: '#ffec00' }}>
                        <Text style={[styles.textInfo, { marginTop: 15, alignSelf: 'center', color: '#282827' }]}><Text style={[styles.TextBotao, { color: '#282827' }]}>Confira</Text> as avaliações </Text>
                        <Text style={[styles.textInfo, { alignSelf: 'center', marginBottom: 15, color: '#282827' }]}>de cada <Text style={[styles.TextBotao, { color: '#282827' }]}>profissional</Text></Text>
                    </View>

                    <Searchbar
                        placeholder="Buscar"
                        style={{ marginHorizontal: 10, marginTop: 23 }}
                        onFocus={() => {this.props.navigation.navigate('buscaProfNome')}}
                    />

                    <Text style={{
                        color: 'white',
                        marginLeft: 10,
                        fontSize: 10,
                    }}>Ex: João André.</Text>


                    {this.state.dataPedido.length > 0 ?
                        <GridView
                            itemDimension={251}
                            items={this.state.dataPedido}
                            style={{ marginTop: 20 }}
                            renderItem={item => (
                                <View
                                    style={[styles.itemContainer, {
                                        borderRadius: 10,
                                        justifyContent: 'center',
                                        backgroundColor: 'white',
                                        elevation: 4,
                                    }]}>
                                    <View style={{
                                        borderTopLeftRadius: 10,
                                        borderTopRightRadius: 10,
                                        height: 25,
                                        justifyContent: 'center',
                                        backgroundColor: '#ffec00'
                                    }}>
                                        <Text style={{
                                            textAlign: 'center',
                                            fontSize: 10,
                                            color: '#282827'
                                        }}>{String(item.item.profissional).toUpperCase()} FOI AVALIADO POR</Text>
                                    </View>

                                    <View style={{ flexDirection: 'row', }}>
                                        <Image style={{
                                            height: 80,
                                            width: 70,
                                            backgroundColor: 'white'
                                        }}
                                            source={{ uri: 'http://clickresolve.com.br/trabalho/img/' + '(43)99684-1383' + '/perfil/image.jpg' }}
                                        />

                                        <View style={{ marginLeft: 10, marginTop: 5 }}>
                                            <Text style={{ color: '#282827', fontSize: 10, fontWeight: 'bold' }}>{item.item.contratante}</Text>
                                            <Text style={{ color: '#57606f', fontSize: 10, marginRight: 30 }}>{item.item.descricao}</Text>
                                        </View>
                                    </View>

                                    <View style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                    }}>
                                        {
                                            item.item.ava == 0 ?
                                                <View style={{
                                                    flexDirection: 'row',
                                                    width: 140,
                                                    marginTop: 0,
                                                    justifyContent: 'space-between',
                                                    marginLeft: 5
                                                }}>
                                                    <Text style={{ color: '#f1c40f', fontSize: 8, marginRight: 2 }}>NÃO AVALIADO POR NENHUM USUÁRIO</Text>
                                                    <Icon name="staro" color='#f1c40f' size={10} />
                                                </View> :
                                                item.item.ava == 1 ?
                                                    <View style={{
                                                        flexDirection: 'row',
                                                        width: 70,
                                                        marginTop: 0,
                                                        backgroundColor: '#ff5e57',
                                                        justifyContent: 'space-between',
                                                        paddingHorizontal: 5,
                                                        paddingVertical: 5,
                                                        borderBottomLeftRadius: 10
                                                    }}>
                                                        <Icon name="star" color='#282827' size={10} />
                                                        <Icon name="staro" color='#282827' size={10} />
                                                        <Icon name="staro" color='#282827' size={10} />
                                                        <Icon name="staro" color='#282827' size={10} />
                                                        <Icon name="staro" color='#282827' size={10} />
                                                    </View> :
                                                    item.item.ava == 2 ?
                                                        <View style={{
                                                            flexDirection: 'row',
                                                            width: 70,
                                                            marginTop: 0,
                                                            backgroundColor: '#ff5e57',
                                                            justifyContent: 'space-between',
                                                            paddingHorizontal: 5,
                                                            paddingVertical: 5,
                                                            borderBottomLeftRadius: 10
                                                        }}>
                                                            <Icon name="star" color='#282827' size={10} />
                                                            <Icon name="star" color='#282827' size={10} />
                                                            <Icon name="staro" color='#282827' size={10} />
                                                            <Icon name="staro" color='#282827' size={10} />
                                                            <Icon name="staro" color='#282827' size={10} />
                                                        </View> :
                                                        item.item.ava == 3 ?
                                                            <View style={{
                                                                flexDirection: 'row',
                                                                width: 70,
                                                                marginTop: 0,
                                                                backgroundColor: '#ffaf40',
                                                                justifyContent: 'space-between',
                                                                paddingHorizontal: 5,
                                                                paddingVertical: 5,
                                                                borderBottomLeftRadius: 10
                                                            }}>
                                                                <Icon name="star" color='#282827' size={10} />
                                                                <Icon name="star" color='#282827' size={10} />
                                                                <Icon name="star" color='#282827' size={10} />
                                                                <Icon name="staro" color='#282827' size={10} />
                                                                <Icon name="staro" color='#282827' size={10} />
                                                            </View> :
                                                            item.item.ava == 4 ?
                                                                <View style={{
                                                                    flexDirection: 'row',
                                                                    width: 70,
                                                                    marginTop: 0,
                                                                    backgroundColor: '#7bed9f',
                                                                    justifyContent: 'space-between',
                                                                    paddingHorizontal: 5,
                                                                    paddingVertical: 5,
                                                                    borderBottomLeftRadius: 10
                                                                }}>
                                                                    <Icon name="star" color='#282827' size={10} />
                                                                    <Icon name="star" color='#282827' size={10} />
                                                                    <Icon name="star" color='#282827' size={10} />
                                                                    <Icon name="star" color='#282827' size={10} />
                                                                    <Icon name="staro" color='#282827' size={10} />
                                                                </View> :
                                                                item.item.ava == 5 ?
                                                                    <View style={{
                                                                        flexDirection: 'row',
                                                                        width: 70,
                                                                        marginTop: 0,
                                                                        backgroundColor: '#7bed9f',
                                                                        justifyContent: 'space-between',
                                                                        paddingHorizontal: 5,
                                                                        paddingVertical: 5,
                                                                        borderBottomLeftRadius: 10
                                                                    }}>
                                                                        <Icon name="star" color='#282827' size={10} />
                                                                        <Icon name="star" color='#282827' size={10} />
                                                                        <Icon name="star" color='#282827' size={10} />
                                                                        <Icon name="star" color='#282827' size={10} />
                                                                        <Icon name="star" color='#282827' size={10} />
                                                                    </View> : null

                                        }
                                        <TouchableOpacity onPress={() => { this.setState({ visible: true, itemSelect: item }) }} style={{
                                            height: 25,
                                            borderRadius: 5,
                                            elevation: 3,
                                            backgroundColor: '#282827',
                                            justifyContent: 'center',
                                            marginTop: -10,
                                            marginRight: 5,
                                            padding: 5
                                        }}>
                                            <Text style={{ color: 'white', fontSize: 10, fontWeight: 'bold', alignSelf: 'center' }}>AVALIE ESTE PROFISSIONAL</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )}
                        />
                        :
                        this.state.vazio ?
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Text>NÃO HÁ AVALIAÇÕES DISPONIVÉIS</Text>
                            </View>
                            :
                            <ActivityIndicator color={'#ffec00'} size="large" style={{ flex: 1 }} />
                    }


                </View>
            </ScrollView>
        );
    }
}

export default Avaliacao;
const styles = StyleSheet.create({
    rodape: {
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 4,
        marginBottom: 10,
        marginRight: 20,
        borderRadius: 60,
        backgroundColor: '#40407a',
        width: 60,
        height: 60,
    },
    textInfo: {
        fontSize: 16,
    },
    TextBotao: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    botao: {
        borderWidth: 0.5,
        borderColor: 'silver',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        height: 45,
        borderRadius: 5,
    },
    itemContainer: {
        flex: 1,
    },

});