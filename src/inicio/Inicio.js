import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  TextInput,
  Image,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
  StatusBar,
  Modal,
  Linking,
  Picker,
  Platform,
  ToastAndroid,
  DatePickerIOS,
  DatePickerAndroid,
  BackHandler
} from "react-native";
import GridView from 'react-native-super-grid';
import { checkInternetConnection } from 'react-native-offline';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/EvilIcons';
import IconFF from 'react-native-vector-icons/Feather';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import Carousel from 'react-native-snap-carousel'
import RNFetchBlob from 'rn-fetch-blob';
import { DrawerNavigator, DrawerItems, } from 'react-navigation'
import { Searchbar, Provider as PaperProvider } from 'react-native-paper';
import API, { caminhoAPI, versaoAPI } from '../utils/caminhosAPI'

import MinhaConta from '../inicio/MinhaConta';
import Avaliacao from './Avaliacao';

class Inicio extends Component {
  state = {
    visible: false,
    refreshAdd: false,
    refresh: false,
    visibleServico: false,
    destino: '',
    dataFim: 0,
    dataIni: 0,
    ava: '',
    diaFim: '',
    dia: '',
    mesFim: '',
    mes: '',
    selectDate: new Date(),
    qtdAdulto: 0,
    qtdCrianca: 0,
    idCidade: '',
    nomeCidade: '',
    uf: '',
    idUsuario: '',
    token: '',
    tipo: '',
    tipoDate: '',
    dateIOS: false,
    locais: [],
    sugestao: []
  }

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.navigate('MenuP')
      return true;
    })

    var RNFS = require('react-native-fs');
    var path = RNFS.DocumentDirectoryPath + '/usuario.json';

    RNFS.readFile(path, 'utf8')
      .then((contents) => {
        if (!(contents == '[{}]')) {
          this.setState({
            idUsuario: String(JSON.parse(contents)[0].codigo),
            tipo: JSON.parse(contents)[0].tipo,
            token: JSON.parse(contents)[0].token
          })
        } else {
          Alert.alert(
            'Confirmação',
            'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
            [
              { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
              { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
            ]
          );

          this.props.navigation.navigate('MenuDestino1')
          this.setState({ idUsuario: '0' })
        }
      })
      .catch((err) => {
        this.setState({ idUsuario: '0' })
        alert(err.message)
        console.log(err.message, err.code);
      });

    this.setState({
      refreshCidades: true,
    })

    const isConnected = await checkInternetConnection();

    if (isConnected) {
      fetch(caminhoAPI() + '/consultarLocais', {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Versao': versaoAPI
        },
      }
      ).then((response) => {
        const statusCode = response.status;
        let responseJson1 = [];

        if (statusCode == 200) {
          responseJson1 = response.json();
        }

        return Promise.all([responseJson1, statusCode]);
      })
        .then(([responseJson, statusCode]) => {
          if (responseJson.length > 0) {

            this.setState({
              refreshCidades: false,
              locaisBusca: responseJson,
              locais: responseJson
            })

          } else {
            this.setState({
              vazio: true,
              refreshCidades: false
            })
          }
        })
        .catch((error) => {
          alert(JSON.stringify(error))
          this.setState({
            vazio: true,
            locais: [],
            refreshCidades: false
          })
        });

    } else {
      alert('Sem conexão com a internet!')
      this.setState({
        vazio: true,
        refreshCidades: false
      })
    }


    if (isConnected) {
      fetch(caminhoAPI() + '/consultarCategoria', {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Versao': versaoAPI
        },
      }
      ).then((response) => response.json())
        .then((responseJson1) => {
          if (responseJson1.length > 0) {

            this.setState({
              refresh: false,
              sugestao: responseJson1
            })

          } else {
            this.setState({
              vazio: true,
              sugestao: false
            })
          }
        })
        .catch((error) => {
          this.setState({
            vazio: true,
            grupos: [],
            sugestao: false
          })
        });

    } else {
      alert('Sem conexão com a internet!')
      this.setState({
        vazio: true,
        refresh: false
      })
    }
  }

  select = async () => {
    if (!(this.state.idUsuario == '0')) {
      if (!(this.state.qtdAdulto > 0)) {
        alert('É necessário indicar quantidade de Adultos');
        return
      }

      if (this.state.nomeCidade == '') {
        alert('É necessário indicar o Local');
        return
      }

      if (!(String(this.state.dia).length > 0)) {
        alert('É necessário data de Inicio');
        return
      }

      if (!(String(this.state.diaFim).length > 0)) {
        alert('É necessário data do Fim');
        return
      }

      this.setState({
        refresh: true
      })

      const isConnected = await checkInternetConnection();

      if (isConnected) {
        let dados = [];
        dados.push({
          "Local": this.state.idCidade.replace(/[^0-9]/g, ''),
          "DataInicio": this.state.dataIni,
          "DataFim": this.state.dataFim,
          "Adulto": String(this.state.qtdAdulto),
          "Crianca": String(this.state.qtdCrianca)
        })

        fetch(caminhoAPI() + '/solicitar', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Versao': versaoAPI,
            'Auth': 'Bearer ' + this.state.token
            //'Cliente': this.state.idUsuario,
            //'Tipo': this.state.tipo
          },
          body: String(JSON.stringify(dados)).substr(1).substr(-(String(JSON.stringify(dados)).length - 1), (String(JSON.stringify(dados)).length - 2))
        }
        ).then((response) => {
          const statusCode = response.status;
          return Promise.all([statusCode]);
        })
          .then(([statusCode]) => {
            if (statusCode == 200) {
              const RNFS = require('react-native-fs');
              const pro = RNFS.DocumentDirectoryPath + '/reservaselect.json';

              let dados = [];

              dados.push({
                "cidade": this.state.nomeCidade,
                "diaIni": (this.state.dia + ' DE ' + this.state.mes),
                "diaFim": (this.state.diaFim + ' DE ' + this.state.mesFim),
                "adultos": this.state.qtdAdulto,
                "criancas": this.state.qtdCrianca,
              })

              RNFS.writeFile(pro, JSON.stringify(dados), 'utf8')
                .then((success) => {
                  console.log('FILE WRITTEN!');
                })
                .catch((err) => {
                  console.log(err.message);
                });


              this.props.navigation.navigate('MessageAguardar')

            } else {
              alert('Estabelecimentos não foram encontrados')
              this.setState({
                refresh: false
              })
            }
          })
          .catch((error) => {
            alert('Informações não são válidas');
            this.setState({
              refresh: false
            })
          });

      } else {
        alert('Sem conexão com a internet!')
        this.setState({
          refresh: false
        })
      }
    } else {
      alert('É preciso fazer login para prosseguir ' + this.state.idUsuario)
    }
  }

  selectSug = (id, descr) => {
    if (descr == 'Excursões') {
      Linking.openURL('https://entremares.tur.br/destinos/excursoes/')
      return
    }

    if (descr == 'Convênios') {
      Linking.openURL('https://entremares.tur.br/destinos/convenio/')
      return
    }

    const RNFS = require('react-native-fs');
    const pro = RNFS.DocumentDirectoryPath + '/reservaCria.json';

    let dados = [];

    dados.push({
      tipo: String(descr).replace(' ', ''),
      codigo: id,
      descricao: descr
    })

    RNFS.writeFile(pro, JSON.stringify(dados), 'utf8')
      .then((success) => {
        console.log('FILE WRITTEN!');
      })
      .catch((err) => {
        console.log(err.message);
      });


    this.props.navigation.navigate('MenuDestinoResult')
  }

  searchFilterFunction = async (text) => {
    this.setState({
      refreshCidades: true,
      vazio: false
    })

    const isConnected = await checkInternetConnection();
    if (isConnected) {
      fetch(caminhoAPI() + '/consultarLocaisPorPalavra?Palavra=' + text, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Versao': versaoAPI
        },
      }
      ).then((response) => {
        const statusCode = response.status;
        let responseJson1 = [];

        if (statusCode == 200) {
          responseJson1 = response.json();
        }

        return Promise.all([responseJson1, statusCode]);
      })
        .then(([responseJson, statusCode]) => {
          if (responseJson.length > 0) {

            this.setState({
              locaisBusca: responseJson,
              vazio: false,
              refreshCidades: false,
              locais: responseJson
            })

          } else {
            this.setState({
              vazio: true,
              refreshCidades: false
            })
          }
        })
        .catch((error) => {
          alert(JSON.stringify(error))
          this.setState({
            vazio: true,
            locais: [],
            refreshCidades: false
          })
        });

    } else {
      alert('Sem conexão com a internet!')
      this.setState({
        vazio: true,
        refreshCidades: false
      })
    }
  };

  searchFilterFunction1 = text => {
    const newData = this.state.locaisBusca.filter(item => {
      const itemData = `${item.Cidade.toUpperCase()}`;
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });
    this.setState({ locais: newData });
  };

  buscaDiaIOS(day, month, year, atual) {
    this.setState({ selectDate: atual })
    if (this.state.tipoDate == 'ini') {
      let mes = (parseInt(month) + 1)
      let mesn = ''

      mes = String(mes).length == 1 ? ("0" + mes) : mes
      const hoje = new Date()
      const selectDate = new Date(String(mes) + '/' + String(day) + '/' + String(year))

      mesn = mes

      mes = mes == '01' ? 'JANEIRO' :
        mes == '02' ? 'FEVEREIRO' :
          mes == '03' ? 'MARÇO' :
            mes == '04' ? 'ABRIL' :
              mes == '05' ? 'MAIO' :
                mes == '06' ? 'JUNHO' :
                  mes == '07' ? 'JULHO' :
                    mes == '08' ? 'AGOSTO' :
                      mes == '09' ? 'SETEMBRO' :
                        mes == '10' ? 'OUTUBRO' :
                          mes == '11' ? 'NOVEMBRO' :
                            mes == '12' ? 'DEZEMBRO' : null

      let dia = String(day).length == 1 ? ("0" + day) : day

      if ((selectDate > hoje) || (parseInt(String(String(year) + String(mesn) + String(dia))) == parseInt(String(String(hoje.getFullYear()) + (String(hoje.getMonth() + 1).length == 1 ? ('0' + String(hoje.getMonth() + 1)) : String(hoje.getMonth() + 1)) + (String(hoje.getDate()).length == 1 ? ('0' + String(hoje.getDate())) : String(hoje.getDate())))))) {
        if (this.state.diaFim != '') {
          if (parseInt(String(String(year) + String(mesn) + String(dia))) > this.state.dataFim) {
            this.setState({ dia: '', mes: '', dataIni: '' })
            alert('Data inicial superior a final')
            return
          }
        }
        this.setState({ dia: dia, mes: mes, dataIni: parseInt(String(String(year) + String(mesn) + String(dia))) })
      } else {
        this.setState({ dia: '', mes: '', dataIni: '' })
        alert('Data menor que a data atual')
      }
    } else {
      let mes = (parseInt(month) + 1)
      let mesn = ''
      mes = String(mes).length == 1 ? ("0" + mes) : mes
      const hoje = new Date()
      const selectDate = new Date(String(mes) + '/' + String(day) + '/' + String(year))
      mesn = mes

      mes = mes == '01' ? 'JANEIRO' :
        mes == '02' ? 'FEVEREIRO' :
          mes == '03' ? 'MARÇO' :
            mes == '04' ? 'ABRIL' :
              mes == '05' ? 'MAIO' :
                mes == '06' ? 'JUNHO' :
                  mes == '07' ? 'JULHO' :
                    mes == '08' ? 'AGOSTO' :
                      mes == '09' ? 'SETEMBRO' :
                        mes == '10' ? 'OUTUBRO' :
                          mes == '11' ? 'NOVEMBRO' :
                            mes == '12' ? 'DEZEMBRO' : null

      let dia = String(day).length == 1 ? ("0" + day) : day
      if ((selectDate > hoje) || (parseInt(String(String(year) + String(mesn) + String(dia))) == parseInt(String(String(hoje.getFullYear()) + (String(hoje.getMonth() + 1).length == 1 ? ('0' + String(hoje.getMonth() + 1)) : String(hoje.getMonth() + 1)) + (String(hoje.getDate()).length == 1 ? ('0' + String(hoje.getDate())) : String(hoje.getDate())))))) {
        if (this.state.dia != '') {
          if (parseInt(String(String(year) + String(mesn) + String(dia))) < this.state.dataIni) {
            alert('Data final menor que inicial')
            this.setState({ diaFim: '', mesFim: '', dataFim: '' })
            return
          }
        }
        this.setState({ diaFim: dia, mesFim: mes, dataFim: parseInt(String(String(year) + String(mesn) + String(dia))) })
      } else {
        this.setState({ diaFim: '', mesFim: '', dataFim: '' })
        alert('Data menor que a data atual')
      }
    }
  }

  render() {

    return (
      <View
        style={{ backgroundColor: '#001e46', height: '100%' }}>

        <StatusBar
          barStyle="light-content"
          backgroundColor='#001e46'
        />

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.visibleServico}
          onRequestClose={() => {
            this.setState({
              visibleServico: false,
              idCidade: '',
              nomeCidade: '',
              uf: ''
            });
          }}>
          <StatusBar
            barStyle="light-content"
            backgroundColor='#001e46'
          />
          {this.state.refreshAdd ? <ActivityIndicator size='large' color='#001e46' style={{ flex: 1 }} /> :
            <View style={{ flex: 1, backgroundColor: 'white' }}>
              <StatusBar
                barStyle="light-content"
                backgroundColor='#001e46'
              />

              <View style={{
                height: Platform.OS === 'android' ? 110 : 130,
                backgroundColor: '#001e46',
                flexDirection: 'row',
                paddingRight: 60,
                paddingTop: Platform.OS === 'android' ? null : 35
              }}>
                <TouchableOpacity onPress={() => this.setState({
                  visibleServico: false,
                  idCidade: '',
                  nomeCidade: '',
                  uf: ''
                })}
                  style={{
                    flexDirection: 'row',
                    marginHorizontal: 20,
                    marginTop: 15,
                    height: 38,
                    borderRadius: 28,
                    backgroundColor: '#ffde00'
                  }}>
                  <Icon name="arrowleft" color='#001e46' style={{ padding: 5 }} size={28} />
                </TouchableOpacity>


                <Text style={{ color: 'white', fontSize: 20, marginTop: 15, marginRight: 30 }}>Busque pelo nome, a
                    <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 20 }}> CIDADE </Text>
                  que deseja encontrar</Text>
              </View>

              <Searchbar
                placeholder="Buscar"
                style={{ marginHorizontal: 5, marginVertical: 5 }}
                onChangeText={query => this.searchFilterFunction(query)}
              />

              {!this.state.refreshCidades ?
                <GridView
                  itemDimension={251}
                  items={this.state.locais}
                  style={{ padding: 2, marginTop: 5, }}
                  renderItem={item => (
                    <TouchableOpacity onPress={() => {
                      this.setState({
                        idCidade: item.item.Codigo,
                        nomeCidade: item.item.Cidade,
                        uf: item.item.UF,
                        visibleServico: false
                      })
                    }}
                      style={{
                        marginLeft: 10,
                        paddingVertical: 15,
                      }}>
                      <Text style={{
                        fontSize: 16,
                        color: '#282827'
                      }}>{item.item.Cidade + ' - ' + item.item.UF}</Text>
                    </TouchableOpacity>
                  )}
                />
                :
                this.state.vazio ?
                  <View style={{
                    backgroundColor: 'white',
                    flex: 1,
                    justifyContent: 'center'
                  }}>
                    <Text style={{ color: '#282827', fontWeight: 'bold', textAlign: 'center' }}>Nenhum Serviço Encontrado</Text>
                  </View>
                  :
                  <ActivityIndicator size="large" color="#282827" style={{ flex: 1 }} />
              }
            </View>
          }
        </Modal>

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.dateIOS}
          onRequestClose={() => {
            this.setState({
              dateIOS: false,
              idCidade: '',
              nomeCidade: '',
              uf: ''
            });
          }}>
          <StatusBar
            barStyle="light-content"
            backgroundColor='#001e46'
          />

          <View style={{
            flex: 1,
            backgroundColor: 'rgba(0,0,0,0.8)',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center'
          }}>
            <View style={{
              width: '85%',
              height: 325,
              borderRadius: 20,
              justifyContent: 'space-between',
              backgroundColor: 'white'
            }}>
              <View style={{
                backgroundColor: '#ffde00',
                borderTopRightRadius: 20,
                borderTopLeftRadius: 20
              }}>
                <Text style={[styles.textInfo, { marginVertical: 10, textAlign: 'center', alignSelf: 'center', color: '#282827', width: 200 }]}>
                  Selecione a <Text style={{ fontWeight: 'bold' }}>data</Text> desejada de inicio ou término </Text>
              </View>

              <DatePickerIOS
                date={this.state.selectDate}
                onDateChange={(date) => this.buscaDiaIOS(date.getDate(), date.getMonth(), date.getFullYear(), date)}
                mode="date"
              />

              <TouchableOpacity onPress={() => {
                this.setState({ dateIOS: false })
              }} style={{
                justifyContent: 'center',
                height: 45,
                backgroundColor: '#001e46',
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5,
                elevation: 3
              }}>
                <Text style={{
                  fontWeight: 'bold',
                  fontSize: 12,
                  textAlign: 'center',
                  color: 'white',
                }}>OK</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <TouchableOpacity onPress={() => {
          try {
            this.props.screenProps.myDrawerNavigation.toggleDrawer();
          } catch (e) {
            this.props.navigation.toggleDrawer();
          }
        }} style={{ marginLeft: 20, marginTop: 40 }}>
          <Iconr name="menu" color='#ffec00' style={{ padding: 5 }} size={40} />
        </TouchableOpacity>

        <ScrollView>
          <View style={{
            borderBottomWidth: 0.5,
            borderBottomColor: 'white',
            marginHorizontal: 29,
            marginTop: 20,
          }}>
            {this.state.refreshCidades ? <ActivityIndicator size='small' color={'#ffec00'} /> :
              <TouchableOpacity
                onPress={() => this.setState({ visibleServico: true })}
                style={{
                  paddingVertical: 20
                }}>
                <Text style={{ color: 'white', fontWeight: 'bold' }}>{this.state.nomeCidade == '' ? 'SELECIONE A CIDADE' : String(this.state.nomeCidade + ' - ' + this.state.uf).toLocaleUpperCase()}</Text>
              </TouchableOpacity>
            }
          </View>

          <View style={{
            flexDirection: 'row',
            marginHorizontal: 29,
            paddingVertical: 15,
            alignItems: 'center'
          }}>

            <IconF name="calendar" color='#ffec00' size={25} />

            <View style={{ flexDirection: 'row', marginHorizontal: 5 }}>
              <TouchableOpacity
                onPress={async () => {
                  if (Platform.OS === "android") {
                    try {
                      const { action, year, month, day } = await DatePickerAndroid.open({
                        date: new Date(),
                      });
                      if (action !== DatePickerAndroid.dismissedAction) {
                        let mes = (parseInt(month) + 1)
                        let mesn = ''

                        mes = String(mes).length == 1 ? ("0" + mes) : mes
                        const hoje = new Date()
                        const selectDate = new Date(String(mes) + '/' + String(day) + '/' + String(year))

                        mesn = mes

                        mes = mes == '01' ? 'JANEIRO' :
                          mes == '02' ? 'FEVEREIRO' :
                            mes == '03' ? 'MARÇO' :
                              mes == '04' ? 'ABRIL' :
                                mes == '05' ? 'MAIO' :
                                  mes == '06' ? 'JUNHO' :
                                    mes == '07' ? 'JULHO' :
                                      mes == '08' ? 'AGOSTO' :
                                        mes == '09' ? 'SETEMBRO' :
                                          mes == '10' ? 'OUTUBRO' :
                                            mes == '11' ? 'NOVEMBRO' :
                                              mes == '12' ? 'DEZEMBRO' : null

                        let dia = String(day).length == 1 ? ("0" + day) : day

                        if ((selectDate > hoje) || (parseInt(String(String(year) + String(mesn) + String(dia))) == parseInt(String(String(hoje.getFullYear()) + (String(hoje.getMonth() + 1).length == 1 ? ('0' + String(hoje.getMonth() + 1)) : String(hoje.getMonth() + 1)) + (String(hoje.getDate()).length == 1 ? ('0' + String(hoje.getDate())) : String(hoje.getDate())))))) {
                          if (this.state.diaFim != '') {
                            if (parseInt(String(String(year) + String(mesn) + String(dia))) > this.state.dataFim) {
                              this.setState({ dia: '', mes: '', dataIni: '' })
                              alert('Data inicial superior a final')
                              return
                            }
                          }
                          this.setState({ dia: dia, mes: mes, dataIni: parseInt(String(String(year) + String(mesn) + String(dia))) })
                        } else {
                          this.setState({ dia: '', mes: '', dataIni: '' })
                          alert('Data menor que a data atual')
                        }
                      }
                    } catch ({ code, message }) {
                      console.warn('Cannot open date picker', message);
                    }
                  } else {
                    this.setState({
                      dateIOS: true,
                      tipoDate: "ini"
                    })
                  }
                }}>
                <Text style={{ color: 'white', marginHorizontal: 2, fontSize: 12 }}>
                  {this.state.dia == '' ? 'DIA INICIO' : this.state.dia + ' DE ' + this.state.mes}
                </Text>
              </TouchableOpacity>

              <Text style={{ color: 'white', marginHorizontal: 4 }}>-</Text>

              <TouchableOpacity
                onPress={async () => {
                  if (Platform.OS === "android") {
                    try {
                      const { action, year, month, day } = await DatePickerAndroid.open({
                        date: new Date(),
                      });
                      if (action !== DatePickerAndroid.dismissedAction) {
                        let mes = (parseInt(month) + 1)
                        let mesn = ''
                        mes = String(mes).length == 1 ? ("0" + mes) : mes
                        const hoje = new Date()
                        const selectDate = new Date(String(mes) + '/' + String(day) + '/' + String(year))
                        mesn = mes

                        mes = mes == '01' ? 'JANEIRO' :
                          mes == '02' ? 'FEVEREIRO' :
                            mes == '03' ? 'MARÇO' :
                              mes == '04' ? 'ABRIL' :
                                mes == '05' ? 'MAIO' :
                                  mes == '06' ? 'JUNHO' :
                                    mes == '07' ? 'JULHO' :
                                      mes == '08' ? 'AGOSTO' :
                                        mes == '09' ? 'SETEMBRO' :
                                          mes == '10' ? 'OUTUBRO' :
                                            mes == '11' ? 'NOVEMBRO' :
                                              mes == '12' ? 'DEZEMBRO' : null

                        let dia = String(day).length == 1 ? ("0" + day) : day
                        if ((selectDate > hoje) || (parseInt(String(String(year) + String(mesn) + String(dia))) == parseInt(String(String(hoje.getFullYear()) + (String(hoje.getMonth() + 1).length == 1 ? ('0' + String(hoje.getMonth() + 1)) : String(hoje.getMonth() + 1)) + (String(hoje.getDate()).length == 1 ? ('0' + String(hoje.getDate())) : String(hoje.getDate())))))) {
                          if (this.state.dia != '') {
                            if (parseInt(String(String(year) + String(mesn) + String(dia))) < this.state.dataIni) {
                              alert('Data final menor que inicial')
                              this.setState({ diaFim: '', mesFim: '', dataFim: '' })
                              return
                            }
                          }
                          this.setState({ diaFim: dia, mesFim: mes, dataFim: parseInt(String(String(year) + String(mesn) + String(dia))) })
                        } else {
                          this.setState({ diaFim: '', mesFim: '', dataFim: '' })
                          alert('Data menor que a data atual')
                        }
                      }
                    } catch ({ code, message }) {
                      console.warn('Cannot open date picker', message);
                    }
                  } else {
                    this.setState({
                      dateIOS: true,
                      tipoDate: "fim"
                    })
                  }
                }}>
                <Text style={{ color: 'white', marginHorizontal: 2, fontSize: 12 }}>
                  {this.state.diaFim == '' ? 'DIA FIM' : this.state.diaFim + ' DE ' + this.state.mesFim}
                </Text>
              </TouchableOpacity>
            </View>

            <Icon name="downcircleo" color='#ffec00' size={10} />

          </View>

          <View style={{
            borderBottomWidth: 0.5,
            marginHorizontal: 29,
            borderBottomColor: 'white'
          }} />

          <View style={{
            flexDirection: 'row',
            marginHorizontal: 29,
            paddingVertical: 10,
            alignItems: 'center'
          }}>
            <Text style={{
              color: 'white',
              fontSize: 10,
              marginRight: 10
            }}>ADULTOS</Text>

            <TouchableOpacity
              style={{ padding: 5 }}
              onPress={() => { this.setState({ qtdAdulto: this.state.qtdAdulto == 0 ? 0 : (this.state.qtdAdulto - 1) }) }}>
              <Text style={{
                color: 'white',
                fontSize: 14,
                marginRight: 10
              }}>-</Text>
            </TouchableOpacity>

            <Text style={{
              fontSize: 22,
              color: '#ffec00',
              marginRight: 10
            }}>{this.state.qtdAdulto}</Text>

            <TouchableOpacity
              style={{ padding: 5 }}
              onPress={() => { this.setState({ qtdAdulto: (this.state.qtdAdulto + 1) }) }}>
              <Text style={{
                color: 'white',
                fontSize: 14,
                marginRight: 40
              }}>+</Text>
            </TouchableOpacity>


            <Text style={{
              color: 'white',
              fontSize: 10,
              marginRight: 10
            }}>CRIANÇAS</Text>

            <TouchableOpacity
              style={{ padding: 5 }}
              onPress={() => { this.setState({ qtdCrianca: this.state.qtdCrianca == 0 ? 0 : (this.state.qtdCrianca - 1) }) }}>
              <Text style={{
                color: 'white',
                fontSize: 16,
                marginRight: 10
              }}>-</Text>
            </TouchableOpacity>

            <Text style={{
              fontSize: 22,
              color: '#ffec00',
              marginRight: 10
            }}>{this.state.qtdCrianca}</Text>

            <TouchableOpacity
              style={{ padding: 5 }}
              onPress={() => { this.setState({ qtdCrianca: (this.state.qtdCrianca + 1) }) }}>
              <Text style={{ color: 'white', fontSize: 16 }}>+</Text>
            </TouchableOpacity>
          </View>

          <View style={{
            borderBottomWidth: 0.5,
            marginHorizontal: 29,
            borderBottomColor: 'white'
          }} />

          <TouchableOpacity disabled={this.state.refresh} onPress={() => { this.select() }} style={{
            height: 35,
            width: 120,
            paddingHorizontal: 10,
            paddingVertical: 5,
            marginLeft: 29,
            marginTop: 29,
            borderRadius: 30,
            backgroundColor: '#ffec00'
          }}>
            <Text style={{
              color: '#001e46',
              fontSize: 14,
              textAlign: 'center'
            }}>SOLICITAR</Text>
          </TouchableOpacity>

          <View style={{ width: 140, marginTop: 50 }}>
            <Text style={{
              color: '#ffec00',
              fontSize: 17,
              fontWeight: 'bold',
              marginLeft: 29,
            }}>SUGESTÕES</Text>
          </View>

          {this.state.sugestao.length > 0 ?
            <GridView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              itemDimension={400}
              items={this.state.sugestao}
              style={styles.gridView}
              renderItem={item => (
                <TouchableOpacity
                  onPress={() => { this.selectSug(item.item.Codigo, item.item.Descricao); }}
                  style={{
                    height: 250,
                    width: 270,
                    borderWidth: 0.7,
                    borderColor: '#ffec00',
                    borderRadius: 20,
                    backgroundColor: '#001e46',
                    elevation: 4,
                  }}>
                  <Image
                    style={{
                      resizeMode: 'cover',
                      width: 270,
                      height: 200,
                      marginRight: 1,
                      borderTopLeftRadius: 20,
                      borderTopRightRadius: 20
                    }}
                    source={String(item.item.Descricao).toLowerCase() == 'águas termais' ? require('../img/categoria/aguas-termais.jpg') :
                      String(item.item.Descricao).toLowerCase() == 'convênios' ? require('../img/categoria/convenios.jpg') :
                        String(item.item.Descricao).toLowerCase() == 'excursões' ? require('../img/categoria/excurcoes.jpg') :
                          String(item.item.Descricao).toLowerCase() == 'praias' ? require('../img/categoria/praias.jpg') :
                            String(item.item.Descricao).toLowerCase() == 'turismo cristão' ? require('../img/categoria/turismo-cristao.jpg') :
                              String(item.item.Descricao).toLowerCase() == 'pontos turísticos' ? require('../img/categoria/pontos-turisticos.jpg') :
                                String(item.item.Descricao).toLowerCase() == 'serra' ? require('../img/categoria/serra.jpg') :
                                  String(item.item.Descricao).toLowerCase() == 'pesca' ? require('../img/categoria/pesca.jpg') : null
                    } />

                  <View style={{
                    flex: 1,
                    backgroundColor: '#ffec00',
                    justifyContent: 'center',
                    marginBottom: -0.9,
                    borderBottomRightRadius: 20,
                    borderBottomLeftRadius: 20
                  }}>
                    <Text style={{
                      color: '#001e46',
                      fontSize: 22,
                      fontWeight: 'bold',
                      marginLeft: 20
                    }}>{String(item.item.Descricao).toUpperCase()}</Text>
                  </View>

                </TouchableOpacity>
              )}
            /> :
            <ActivityIndicator color='#ffec00' size='small' />
          }
        </ScrollView>

        <View style={{ flexDirection: 'row', height: Platform.OS === 'android' ? 55 : 75, backgroundColor: '#ffec00', alignItems: 'center' }}>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('MenuDestino1') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rdestino.png')}
              style={{
                height: 50,
                width: 50,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {
            if (this.state.idUsuario == '0') {
              Alert.alert(
                'Confirmação',
                'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                [
                  { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                  { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                ]
              );
            } else {
              this.props.navigation.navigate('Inicio')
            }
          }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rreservas.png')}
              style={{
                height: 50,
                width: 50,
                marginBottom: 6,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>
          <TouchableOpacity style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rpontos.png')}
              style={{
                height: 50,
                width: 50,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {
            if (this.state.idUsuario == '0') {
              Alert.alert(
                'Confirmação',
                'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                [
                  { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                  { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                ]
              );
            } else {
              this.props.navigation.navigate('MinhaConta')
            }
          }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rconta.png')}
              style={{
                height: 50,
                width: 50,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>

        </View>

      </View>
    )
  }
}

export default Inicio;

/*Configuração do Topo | Header é o topo*/
const CustomDrawerContentComponent = (props) => (
  <View>

  </View>
)

/*Configuração do Menu*/
const MyApp = DrawerNavigator({

  Principal: {
    screen: Inicio,
  },
  Contato: {
    screen: Avaliacao,
  },
  Ajuda: {
    screen: MinhaConta,
  },
}, {
  initialRouteName: 'Principal',
  drawerPosition: 'left',
  contentComponent: CustomDrawerContentComponent,
  drawerOpenRoute: 'DrawerOpen',
  drawerCloseRoute: 'DrawerClose',
  drawerToggleRoute: 'DrawerToggle',
}, {
  navigationOptions: ({ navigation }) => ({
    headerStyle: { backgroundColor: '#4C3E53' },
  })


})

const styles = StyleSheet.create({
  gridView: {
    height: 300,
  },
  itemContainer: {
    elevation: 4,
    height: 70,
    marginHorizontal: 10,
    marginBottom: 5,
  },
  itemContainerItem: {
    elevation: 4,
    height: 50,
    marginLeft: 45,
    marginBottom: 5,
  },
  itemContainerProfissa: {
    elevation: 4,
    height: 100,
    marginLeft: 45,
    marginRight: -5,
    marginBottom: 5,
  },

});