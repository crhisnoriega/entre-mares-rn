import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  TextInput,
  Image,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
  StatusBar,
  Platform,
  NetInfo,
  Linking,
  ToastAndroid,
  BackHandler
} from "react-native";

class Politica extends React.Component {

  componentDidMount() {
    this.props.navigation.navigate('MenuP');
    Linking.openURL('http://entremares.tur.br/politica-de-privacidade/')
  }

  render() {

    return (
      <View />
    )
  }
}

export default Politica;
const styles = StyleSheet.create({
  gridView: {
    height: 300,
  },
  itemContainer: {
    elevation: 4,
    height: 70,
    marginHorizontal: 10,
    marginBottom: 5,
  },
  itemContainerItem: {
    elevation: 4,
    height: 50,
    marginLeft: 45,
    marginBottom: 5,
  },
  itemContainerProfissa: {
    elevation: 4,
    height: 100,
    marginLeft: 45,
    marginRight: -5,
    marginBottom: 5,
  },

});