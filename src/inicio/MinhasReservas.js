import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  TextInput,
  Image,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
  StatusBar,
  Platform,
  NetInfo,
  ToastAndroid,
  RefreshControl,
  BackHandler
} from "react-native";
import GridView from 'react-native-super-grid';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/EvilIcons';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import Carousel from 'react-native-snap-carousel'
import RNFetchBlob from 'rn-fetch-blob';
import { Searchbar, Provider as PaperProvider } from 'react-native-paper';

class MinhasReservas extends React.Component {
  state = {
    visible: false,
    destino: '',
    qtdAdulto: 0,
    qtdCrianca: 0,
    populares: [{ "cidade": "florianópolis - sc", "local": "residencial campeche", "ava": 4.5, "valor": 35.00 },
    { "cidade": "florianópolis - sc", "local": "clube house", "ava": 3.0, "valor": 25.00 },
    { "cidade": "florianópolis - sc", "local": "residencial nautiplus", "ava": 4.8, "valor": 45.00 },
    { "cidade": "florianópolis - sc", "local": "residencial city", "ava": 4.5, "valor": 29.00 }],
    sugestao: [{ "nome": 'praias' }, { "nome": 'hoteis' }, { "nome": 'condominios' }, { "nome": 'residência' }]
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.navigate('MenuP')
      return true;
    })

    var RNFS = require('react-native-fs');
    var path = RNFS.DocumentDirectoryPath + '/usuario.json';

    RNFS.readFile(path, 'utf8')
      .then((contents) => {
        if (!(contents == '[{}]')) {
          this.setState({
            idUsuario: String(JSON.parse(contents)[0].codigo),
            tipo: JSON.parse(contents)[0].tipo,
            token: JSON.parse(contents)[0].token
          })
        } else {
          this.setState({ idUsuario: '0' })
        }
      })
      .catch((err) => {
        this.setState({ idUsuario: '0' })
        alert(err.message)
        console.log(err.message, err.code);
      });
  }

  render() {

    return (
      <View style={{ backgroundColor: '#001e46', height: '100%' }}>

        <StatusBar
          barStyle="light-content"
          backgroundColor='#001e46'
        />

        <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginLeft: 20,
          marginTop: 40,
          alignItems: 'center'
        }}>
          <TouchableOpacity onPress={() => {
            try {
              this.props.screenProps.myDrawerNavigation.toggleDrawer();
            } catch (e) {
              this.props.navigation.toggleDrawer();
            }
          }}>
            <Iconr name="menu" color='#ffec00' style={{ padding: 5 }} size={40} />
          </TouchableOpacity>

          <View>
            <Text style={{ color: 'white', fontSize: 12, marginRight: 40 }}>MINHAS</Text>
            <Text style={{ color: '#ffec00', fontSize: 18, fontWeight: 'bold', marginRight: 40 }}>RESERVAS</Text>
          </View>
        </View>

        <ScrollView style={{ backgroundColor: '#001e46', height: '100%' }}>
          <TouchableOpacity onPress={() => {
            if (this.state.idUsuario == '0') {
              Alert.alert(
                'Confirmação',
                'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                [
                  { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                  { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                ]
              );
            } else {
              this.props.navigation.navigate('MinhasSolicitacao')
            }

          }} style={{ marginTop: 40, }}>
            <View
              style={{
                borderColor: '#ffec00',
                flexDirection: 'row',
                alignItems: 'center',
                borderWidth: 0.7,
                borderRadius: 20,
                marginHorizontal: 40,
                height: 120
              }}>
              <View style={{
                width: '30%',
                height: '100%',
                marginRight: 30,
                justifyContent: 'center',
                borderRightWidth: 1,
                borderColor: '#ffec00'
              }}>
                <Iconr name="exclamation" color='#ffec00' style={{ alignSelf: 'center' }} size={60} />
              </View>

              <View>
                <View style={{ width: 20, height: 5, backgroundColor: '#ffec00' }} />
                <Text style={{ color: 'white', fontSize: 14, marginBottom: -5, marginRight: 25 }}>MINHAS</Text>
                <Text style={{ color: '#ffec00', fontSize: 20, fontWeight: 'bold', marginRight: 25 }}>SOLICITAÇÕES</Text>
              </View>
            </View>

            <View style={{
              width: '50%',
              height: 10,
              backgroundColor: '#ffec00',
              marginLeft: 70,
              marginTop: -5
            }} />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => {
            if (this.state.idUsuario == '0') {
              Alert.alert(
                'Confirmação',
                'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                [
                  { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                  { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                ]
              );
            } else {
              this.props.navigation.navigate('MinhasReservasR')
            }
          }} style={{ marginTop: 40, marginBottom: 20 }}>
            <View
              style={{
                borderColor: '#ffec00',
                flexDirection: 'row',
                alignItems: 'center',
                borderWidth: 0.7,
                borderRadius: 20,
                marginHorizontal: 40,
                height: 120
              }}>
              <View style={{
                width: '30%',
                justifyContent: 'center',
                height: '100%',
                marginRight: 30,
                borderRightWidth: 1,
                borderColor: '#ffec00'
              }}>
                <Iconr name="check" color='#ffec00' style={{ alignSelf: 'center' }} size={60} />
              </View>

              <View>
                <View style={{ width: 20, height: 5, backgroundColor: '#ffec00' }} />
                <Text style={{ color: 'white', fontSize: 14, marginBottom: -5, marginRight: 25 }}>MINHAS</Text>
                <Text style={{ color: '#ffec00', fontSize: 20, fontWeight: 'bold', marginRight: 25 }}>RESERVAS</Text>
              </View>
            </View>

            <View style={{
              width: '50%',
              height: 10,
              backgroundColor: '#ffec00',
              marginLeft: 70,
              marginTop: -5
            }} />
          </TouchableOpacity>
        </ScrollView>
        <View style={{ flexDirection: 'row', height: Platform.OS === 'android' ? 55 : 75, backgroundColor: '#ffec00', alignItems: 'center' }}>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('MenuDestino1') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rdestino.png')}
              style={{
                height: 50,
                width: 50,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {
            if (this.state.idUsuario == '0') {
              Alert.alert(
                'Confirmação',
                'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                [
                  { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                  { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                ]
              );
            } else {
              this.props.navigation.navigate('Inicio')
            }
          }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rreservas.png')}
              style={{
                height: 50,
                width: 50,
                marginBottom: 6,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>
          <TouchableOpacity style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rpontos.png')}
              style={{
                height: 50,
                width: 50,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {
            if (this.state.idUsuario == '0') {
              Alert.alert(
                'Confirmação',
                'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                [
                  { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                  { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                ]
              );
            } else {
              this.props.navigation.navigate('MinhaConta')
            }
          }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rconta.png')}
              style={{
                height: 50,
                width: 50,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>

        </View>

      </View>
    )
  }
}

export default MinhasReservas;
const styles = StyleSheet.create({
  gridView: {
    height: 300,
  },
  itemContainer: {
    elevation: 4,
    height: 70,
    marginHorizontal: 10,
    marginBottom: 5,
  },
  itemContainerItem: {
    elevation: 4,
    height: 50,
    marginLeft: 45,
    marginBottom: 5,
  },
  itemContainerProfissa: {
    elevation: 4,
    height: 100,
    marginLeft: 45,
    marginRight: -5,
    marginBottom: 5,
  },

});