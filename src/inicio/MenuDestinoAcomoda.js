import React, { Component } from "react";
import {
    ScrollView,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    StatusBar,
    TextInput,
    Modal,
    Image,
    Alert,
    Linking,
    Platform,
    ToastAndroid,
    ActivityIndicator,
    BackHandler,
    RefreshControl
} from "react-native";
import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/EvilIcons';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import GridView from 'react-native-super-grid';
import { checkInternetConnection } from 'react-native-offline';
import { Searchbar, Provider as PaperProvider } from 'react-native-paper';
import API, { caminhoAPI, versaoAPI } from '../utils/caminhosAPI'

class MenuDestinoAcomoda extends React.Component {
    state = {
        tipo: '',
        grupo: '',
        local: '',
        idUsuario: '0',
        vazio: false,
        visible: false,
        populares: [],
        sugestao: []

    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('MenuDestino1')
            return true;
        })

        var RNFS = require('react-native-fs');
        var path = RNFS.DocumentDirectoryPath + '/reservaCria.json';

        var pathU = RNFS.DocumentDirectoryPath + '/usuario.json';

        RNFS.readFile(pathU, 'utf8')
            .then((contents) => {
                if (!(contents == '[{}]')) {
                    this.setState({
                        idUsuario: String(JSON.parse(contents)[0].codigo),
                        tipo: JSON.parse(contents)[0].tipo,
                        token: JSON.parse(contents)[0].token
                    })
                } else {
                    this.setState({ idUsuario: '0' })
                }
            })
            .catch((err) => {
                this.setState({ idUsuario: '0' })
                alert(err.message)
                console.log(err.message, err.code);
            });

        RNFS.readFile(path, 'utf8')
            .then(async (contents) => {
                console.warn(contents);
                if (!(contents == '[{}]')) {
                    this.setState({
                        tipo: JSON.parse(contents)[0].tipo,
                        idSubGrupo: JSON.parse(contents)[0].id,
                        local: JSON.parse(contents)[0].local,
                    })

                    const isConnected = await checkInternetConnection();

                    if (isConnected) {
                        fetch(caminhoAPI() + '/consultarDestino?Subcategoria=' + JSON.parse(contents)[0].id, {
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                                'Versao': versaoAPI
                            },
                        }
                        ).then((response) => response.json())
                            .then((responseJson) => {
                                if (responseJson.length > 0) {
                                    this.setState({
                                        refresh: false,
                                        populares: responseJson
                                    })

                                } else {
                                    this.setState({
                                        vazio: true,
                                        refresh: false
                                    })
                                }
                            })
                            .catch((error) => {
                                this.setState({
                                    vazio: true,
                                    populares: [],
                                    refresh: false
                                })
                            });

                    } else {
                        alert('Sem conexão com a internet!')
                        this.setState({
                            vazio: true,
                            refresh: false
                        })
                    }

                    if (isConnected) {
                        fetch(caminhoAPI() + '/consultarCategoria', {
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                                'Versao': versaoAPI
                            },
                        }
                        ).then((response) => response.json())
                            .then((responseJson1) => {
                                if (responseJson1.length > 0) {

                                    this.setState({
                                        refresh: false,
                                        sugestao: responseJson1
                                    })

                                } else {
                                    this.setState({
                                        vazio: true,
                                        sugestao: false
                                    })
                                }
                            })
                            .catch((error) => {
                                this.setState({
                                    vazio: true,
                                    grupos: [],
                                    sugestao: false
                                })
                            });

                    } else {
                        alert('Sem conexão com a internet!')
                        this.setState({
                            vazio: true,
                            refresh: false
                        })
                    }
                } else {
                    this.setState({ tipo: '' })
                }
            })
            .catch((err) => {
                this.setState({ tipo: '' })
                console.log(err.message, err.code);
            });
    }

    select = (tipo, local, id) => {
        const RNFS = require('react-native-fs');
        const pro = RNFS.DocumentDirectoryPath + '/reservaCria.json';

        let dados = [];

        dados.push({
            "tipo": this.state.tipo,
            "id": id,
            "cidade": local,
            "local": String(tipo).substr(0, (String(tipo).length - 5))
        })

        RNFS.writeFile(pro, JSON.stringify(dados), 'utf8')
            .then((success) => {
                console.log('FILE WRITTEN!');
            })
            .catch((err) => {
                console.log(err.message);
            });


        this.props.navigation.navigate('acomodacoes')
    }

    selectSug = (id, descr) => {
        const RNFS = require('react-native-fs');
        const pro = RNFS.DocumentDirectoryPath + '/reservaCria.json';

        let dados = [];

        dados.push({
            tipo: String(descr).replace(' ', ''),
            codigo: id,
            descricao: descr
        })

        RNFS.writeFile(pro, JSON.stringify(dados), 'utf8')
            .then((success) => {
                console.log('FILE WRITTEN!');
            })
            .catch((err) => {
                console.log(err.message);
            });


        this.props.navigation.navigate('MenuDestinoResult')
    }

    render() {

        return (
            <View style={{ backgroundColor: '#001e46', height: '100%' }}>
                <StatusBar
                    barStyle="light-content"
                    backgroundColor='#001e46'
                />

                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginLeft: 20,
                    marginTop: 40,
                    alignItems: 'center'
                }}>
                    <TouchableOpacity onPress={() => {
                        try {
                            this.props.screenProps.myDrawerNavigation.toggleDrawer();
                        } catch (e) {
                            this.props.navigation.toggleDrawer();
                        }
                    }}>
                        <Iconr name="menu" color='#ffec00' style={{ padding: 5 }} size={40} />
                    </TouchableOpacity>

                    <View>
                        <Text style={{ color: 'white', fontSize: 12, marginRight: 40 }}>ESCOLHA SEU</Text>
                        <Text style={{ color: '#ffec00', fontSize: 18, fontWeight: 'bold', marginRight: 40 }}>DESTINO</Text>
                    </View>
                </View>

                <ScrollView style={{ backgroundColor: '#001e46', height: '100%' }}>
                    <View style={{ marginHorizontal: 28, marginTop: 20 }}>
                        <Text style={{ color: 'white', fontSize: 12, marginRight: 40 }}>LITORAL</Text>
                        <Text style={{ color: '#ffec00', fontSize: 18, fontWeight: 'bold', marginRight: 40 }}>{String(this.state.local.toUpperCase()).substr(8)}</Text>
                    </View>

                    {this.state.populares.length > 0 ?
                        <GridView
                            itemDimension={212}
                            items={this.state.populares}
                            renderItem={item => (
                                <TouchableOpacity onPress={() => { this.select(item.item.Descricao, this.state.local.toUpperCase(), item.item.Codigo); }}
                                    style={{
                                        height: 80,
                                        marginHorizontal: 30,
                                        marginBottom: 10,
                                        borderWidth: 0.7,
                                        borderColor: '#ffec00',
                                        borderRadius: 20,
                                        flexDirection: 'row',
                                        backgroundColor: '#001e46',
                                        elevation: 4,
                                    }}>
                                    <Image
                                        style={{
                                            resizeMode: 'cover',
                                            width: '40%',
                                            height: '100%',
                                            marginRight: 1,
                                            borderBottomLeftRadius: 20,
                                            borderTopLeftRadius: 20
                                        }}
                                        source={require('../img/categoria/praias.jpg')} />

                                    <View style={{
                                        height: 50,
                                        width: 15,
                                        backgroundColor: '#ffec00',
                                        marginLeft: -8,
                                        marginTop: 15
                                    }} />

                                    <View style={{ justifyContent: 'center', marginTop: -10 }}>
                                        <Text style={{
                                            color: '#ffec00',
                                            fontSize: 12,
                                            fontWeight: 'bold',
                                            width: 100,
                                            marginLeft: 10,
                                            marginTop: 10
                                        }}>{item.item.Descricao.toUpperCase()}</Text>

                                        <View style={{
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            alignItems: 'center',
                                            marginHorizontal: 10
                                        }}>
                                            <Text style={{
                                                color: 'white',
                                                fontSize: 8,
                                            }}>{this.state.local.toUpperCase()}</Text>

                                        </View>
                                    </View>

                                    <Icon name="caretcircleoup" color='#ffec00' style={{
                                        marginTop: 0, transform: [
                                            { rotate: '90deg' }]
                                    }} size={15} />

                                </TouchableOpacity>
                            )}
                        />
                        :
                        <ActivityIndicator size={'small'} color={'#ffec00'} style={{ flex: 1 }} />
                    }
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginHorizontal: 30,
                    }}>
                        <View />

                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('MenuDestino1') }} style={{
                            paddingHorizontal: 30,
                            justifyContent: 'center',
                            paddingVertical: 5,
                            marginRight: 15,
                            borderRadius: 30,
                            borderWidth: 0.8,
                            borderColor: '#ffec00',
                            backgroundColor: '#001e46'
                        }}>
                            <Text style={{
                                color: '#ffec00',
                                fontSize: 12,
                                textAlign: 'center'
                            }}>Voltar</Text>
                        </TouchableOpacity>
                    </View>

                    <Text style={{
                        color: '#ffec00',
                        fontSize: 16,
                        fontWeight: 'bold',
                        marginLeft: 35,
                        marginTop: 20
                    }}>SUGESTÃO</Text>

                    <GridView
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        itemDimension={400}
                        items={this.state.sugestao}
                        style={{ height: 300 }}
                        renderItem={item => (
                            <TouchableOpacity
                                onPress={() => {
                                    if (item.item.Descricao == 'Excursões') {
                                        Linking.openURL('https://entremares.tur.br/destinos/excursoes/')
                                        return
                                    }

                                    if (item.item.Descricao == 'Convênios') {
                                        Linking.openURL('https://entremares.tur.br/destinos/convenio/')
                                        return
                                    }

                                    this.selectSug(item.item.Codigo, item.item.Descricao);
                                }}
                                style={{
                                    height: 250,
                                    width: 270,
                                    borderWidth: 0.7,
                                    borderColor: '#ffec00',
                                    borderRadius: 20,
                                    backgroundColor: '#001e46',
                                    elevation: 4,
                                }}>
                                <Image
                                    style={{
                                        resizeMode: 'cover',
                                        width: 270,
                                        height: 200,
                                        marginRight: 1,
                                        borderTopLeftRadius: 20,
                                        borderTopRightRadius: 20
                                    }}
                                    source={String(item.item.Descricao).toLowerCase() == 'águas termais' ? require('../img/categoria/aguas-termais.jpg') :
                                        String(item.item.Descricao).toLowerCase() == 'convênios' ? require('../img/categoria/convenios.jpg') :
                                            String(item.item.Descricao).toLowerCase() == 'excursões' ? require('../img/categoria/excurcoes.jpg') :
                                                String(item.item.Descricao).toLowerCase() == 'praias' ? require('../img/categoria/praias.jpg') :
                                                    String(item.item.Descricao).toLowerCase() == 'turismo cristão' ? require('../img/categoria/turismo-cristao.jpg') :
                                                        String(item.item.Descricao).toLowerCase() == 'pontos turísticos' ? require('../img/categoria/pontos-turisticos.jpg') :
                                                            String(item.item.Descricao).toLowerCase() == 'serra' ? require('../img/categoria/serra.jpg') :
                                                                String(item.item.Descricao).toLowerCase() == 'pesca' ? require('../img/categoria/pesca.jpg') : null} />

                                <View style={{
                                    flex: 1,
                                    backgroundColor: '#ffec00',
                                    justifyContent: 'center',
                                    marginBottom: -0.9,
                                    borderBottomRightRadius: 20,
                                    borderBottomLeftRadius: 20
                                }}>
                                    <Text style={{
                                        color: '#001e46',
                                        fontSize: 22,
                                        fontWeight: 'bold',
                                        marginLeft: 20
                                    }}>{String(item.item.Descricao).toUpperCase()}</Text>
                                </View>

                            </TouchableOpacity>
                        )}
                    />
                </ScrollView>

                <View style={{ flexDirection: 'row', height: Platform.OS === 'android' ? 55 : 75, backgroundColor: '#ffec00', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('MenuDestino1') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rdestino.png')}
                            style={{
                                height: 50,
                                width: 50,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        if (this.state.idUsuario == '0') {
                            Alert.alert(
                                'Confirmação',
                                'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                                [
                                    { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                                    { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                                ]
                            );
                        } else {
                            this.props.navigation.navigate('Inicio')
                        }
                    }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rreservas.png')}
                            style={{
                                height: 50,
                                width: 50,
                                marginBottom: 6,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rpontos.png')}
                            style={{
                                height: 50,
                                width: 50,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        if (this.state.idUsuario == '0') {
                            Alert.alert(
                                'Confirmação',
                                'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                                [
                                    { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                                    { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                                ]
                            );
                        } else {
                            this.props.navigation.navigate('MinhaConta')
                        }
                    }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rconta.png')}
                            style={{
                                height: 50,
                                width: 50,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>

                </View>

            </View>
        );
    }
}

export default MenuDestinoAcomoda;
const styles = StyleSheet.create({
    itemContainerProfissa: {
        elevation: 4,
        height: 100,
        marginBottom: 5,
    },
});