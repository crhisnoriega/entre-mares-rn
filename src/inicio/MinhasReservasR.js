import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  TextInput,
  Image,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
  StatusBar,
  Platform,
  ToastAndroid,
  RefreshControl,
  BackHandler
} from "react-native";
import GridView from 'react-native-super-grid';
import { checkInternetConnection } from 'react-native-offline';;
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/EvilIcons';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import Carousel from 'react-native-snap-carousel'
import RNFetchBlob from 'rn-fetch-blob';
import { Searchbar, Provider as PaperProvider } from 'react-native-paper';
import API, { caminhoAPI, versaoAPI } from '../utils/caminhosAPI'

class MinhasReservasR extends React.Component {
  state = {
    visible: false,
    destino: '',
    qtdAdulto: 0,
    refreshing: false,
    refresh: true,
    qtdCrianca: 0,
    reservas: []
  }

 componentDidMount() {
    var RNFS = require('react-native-fs');
    var path = RNFS.DocumentDirectoryPath + '/usuario.json';

    RNFS.readFile(path, 'utf8')
      .then(async (contents) => {
        if (!(contents == '[{}]')) {
          this.setState({
            idUsuario: String(JSON.parse(contents)[0].codigo),
            tipo: JSON.parse(contents)[0].tipo,
            token: JSON.parse(contents)[0].token
          })

          const isConnected = await checkInternetConnection();
            if (isConnected) {
              fetch(caminhoAPI() + '/consultarMinhasReservas', {
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                  'Versao': versaoAPI,
                  'Auth': 'Bearer ' + this.state.token
                  //'Cliente': JSON.parse(contents)[0].codigo,
                  //'Tipo': JSON.parse(contents)[0].tipo
                },
              }
              ).then((response) => {
                const statusCode = response.status;
                let responseJson = [];

                if (statusCode == 200) {
                  responseJson = response.json();
                }

                return Promise.all([responseJson, statusCode]);
              })
                .then(([responseJson, statusCode]) => {
                  if (statusCode == 200) {
                    this.setState({
                      refresh: false,
                      reservas: responseJson
                    })

                  } else {
                    alert('Nenhuma reserva encontrada')
                    this.setState({
                      vazio: true,
                      refresh: false
                    })
                  }
                })
                .catch((error) => {
                  this.setState({
                    vazio: true,
                    //reservas: [],
                    refresh: false
                  })
                  alert('ocorreu uma falha')
                });

            } else {
              alert('Sem conexão com a internet!')
              this.setState({
                vazio: true,
                refresh: false
              })
            }
        } else {
          this.setState({ idUsuario: '0' })
        }
      })
      .catch((err) => {
        this.setState({ idUsuario: '0' })
        alert(err.message)
        console.log(err.message, err.code);
      });
  }

  nomeMes = (mes) => {
    return mes == '01' ? 'JANEIRO' :
      mes == '02' ? 'FEVEREIRO' :
        mes == '03' ? 'MARÇO' :
          mes == '04' ? 'ABRIL' :
            mes == '05' ? 'MAIO' :
              mes == '06' ? 'JUNHO' :
                mes == '07' ? 'JULHO' :
                  mes == '08' ? 'AGOSTO' :
                    mes == '09' ? 'SETEMBRO' :
                      mes == '10' ? 'OUTUBRO' :
                        mes == '11' ? 'NOVEMBRO' :
                          mes == '12' ? 'DEZEMBRO' : null
  }

  createReserva() {
    let reserva = []

    for (let index = 0; index < this.state.reservas.length; index++) {
      let sol = this.state.reservas[index]

      reserva.push(
        <View
          style={{
            height: 310,
            marginHorizontal: 29,
            borderWidth: 0.7,
            borderColor: '#ffec00',
            borderRadius: 20,
            marginTop: 20,
            backgroundColor: '#001e46',
            elevation: 4,
          }}>
          <Image
            style={{
              resizeMode: 'cover',
              width: '100%',
              height: '50%',
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20
            }}
            source={require('../img/praia1.jpg')} />

          <Text style={{
            color: '#ffec00',
            fontSize: 14,
            fontWeight: 'bold',
            marginLeft: 15,
            marginTop: 10
          }}>{String(sol.Titulo).toLocaleUpperCase()}</Text>

          {/*String(sol.LocalCidade).toLocaleUpperCase()} - {String(sol.LocalUF).toLocaleUpperCase()*/}

          <Text style={{
            color: 'white',
            fontSize: 10,
            marginLeft: 15,
          }}>{String(sol.DataEntrada).substr(-2)} DE {this.nomeMes(String(sol.DataEntrada).substr(5, 2))} - {String(sol.DataSaida).substr(-2)} DE {this.nomeMes(String(sol.DataSaida).substr(5, 2))}</Text>
          <Text style={{
            color: 'white',
            fontSize: 10,
            marginLeft: 15,
            marginTop: 4
          }}>STATUS</Text>

          <View style={{
            borderBottomWidth: 1,
            borderColor: '#ffec00',
            marginTop: 30,
            marginRight: 30,
            marginHorizontal: 15
          }} />

          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: -25 }}>
            <View style={{ width: 80 }} />
            <View style={{ width: 80, alignItems: 'center' }}>
              <Text style={{
                color: 'white',
                fontSize: 8,
                marginBottom: 4
              }}>VERIFICANDO</Text>
              <View style={{
                height: 20,
                width: 20,
                borderRadius: 20,
                backgroundColor: '#ffec00'
              }} />
            </View>

            <View style={{ width: 80, alignItems: 'center' }}>
              <Text style={{
                color: 'white',
                fontSize: 8,
                marginBottom: 4
              }}>CONFIRMADO</Text>
              <View style={{
                height: 20,
                width: 20,
                borderRadius: 20,
                borderWidth: 1,
                borderColor: sol.Aprovado == '1' ? null : '#ffec00',
                backgroundColor: sol.Aprovado == '1' ? '#ffec00' : '#001e46'
              }} />
            </View>
          </View>

          {sol.Aprovado == '1' ?
            <TouchableOpacity onPress={() => {
              this.voucher(
                String(sol.Titulo).toLocaleUpperCase(),
                (String(sol.DataEntrada).substr(-2) + ' DE ' + this.nomeMes(String(sol.DataEntrada).substr(5, 2))),
                (String(sol.DataSaida).substr(-2) + ' DE ' + this.nomeMes(String(sol.DataSaida).substr(5, 2))),
                sol.Pessoas,
                sol.Criancas,
                sol.ReservaCodigo
              )
            }} style={{
              height: 30,
              width: 120,
              paddingHorizontal: 2,
              justifyContent: 'center',
              marginLeft: 15,
              marginTop: 15,
              marginBottom: 25,
              borderRadius: 30,
              borderWidth: 0.8,
              borderColor: '#ffec00',
              backgroundColor: '#ffec00'
            }}>
              <Text style={{
                color: '#001e46',
                fontSize: 12,
                fontWeight: 'bold',
                textAlign: 'center'
              }}>Exibir Voucher</Text>
            </TouchableOpacity> : null}
        </View>

      )

    }

    return reserva
  }

  voucher = (h, di, df, a, c, idReserva) => {
    const RNFS = require('react-native-fs');
    const pro = RNFS.DocumentDirectoryPath + '/reservaConfirma.json';

    let dados = [];

    dados.push({
      "hotel": String(h).toUpperCase(),
      "diaIni": di,
      "diaFim": df,
      "adultos": a,
      "criancas": c,
      "voucher": idReserva
    })

    RNFS.writeFile(pro, JSON.stringify(dados), 'utf8')
      .then((success) => {
        console.log('FILE WRITTEN!');
      })
      .catch((err) => {
        console.log(err.message);
      });


    this.props.navigation.navigate('Concluido')
  }

  refresh = async () => {
    const isConnected = await checkInternetConnection();
      if (isConnected) {
        this.setState({
          reservas: []
        })
        fetch(caminhoAPI() + '/consultarMinhasReservas', {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Versao': versaoAPI,
            'Auth': 'Bearer ' + this.state.token
            //'Cliente': JSON.parse(contents)[0].codigo,
            //'Tipo': JSON.parse(contents)[0].tipo
          },
        }
        ).then((response) => {
          const statusCode = response.status;
          let responseJson = [];

          if (statusCode == 200) {
            responseJson = response.json();
          }

          return Promise.all([responseJson, statusCode]);
        })
          .then(([responseJson, statusCode]) => {
            if (statusCode == 200) {
              this.setState({
                reservas: responseJson
              })

            } else {
              this.setState({
                vazio: true,
              })
            }
          })
          .catch((error) => {
            this.setState({
              vazio: true,
              //reservas: [],
              refreshing: false
            })
            alert('ocorreu uma falha')
          });

      } else {
        alert('Sem conexão com a internet!')
        this.setState({
          vazio: true,
          refreshing: false
        })
      }
  }

  render() {

    return (
      <View style={{ backgroundColor: '#001e46', height: '100%' }}>

        <StatusBar
          barStyle="light-content"
          backgroundColor='#001e46'
        />

        <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginLeft: 20,
          marginTop: 40,
          alignItems: 'center'
        }}>
          <TouchableOpacity onPress={() => {
            try {
              this.props.screenProps.myDrawerNavigation.toggleDrawer();
            } catch (e) {
              this.props.navigation.toggleDrawer();
            }
          }}>
            <Iconr name="menu" color='#ffec00' style={{ padding: 5 }} size={40} />
          </TouchableOpacity>

          <View>
            <Text style={{ color: 'white', fontSize: 12, marginRight: 40 }}>MINHAS</Text>
            <Text style={{ color: '#ffec00', fontSize: 18, fontWeight: 'bold', marginRight: 40 }}>RESERVAS</Text>
          </View>
        </View>

        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this.refresh()}
            />
          }
          style={{ backgroundColor: '#001e46', height: '100%' }}>

          {
            !this.state.refresh ?
              this.createReserva() :
              <ActivityIndicator size='small' color={'#ffec00'} />
          }

          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 29 }}>
            <View />
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('MinhasReservas') }} style={{
              height: 30,
              width: 120,
              paddingHorizontal: 10,
              justifyContent: 'center',
              paddingVertical: 5,
              marginLeft: 29,
              marginTop: 15,
              marginBottom: 25,
              borderRadius: 30,
              borderWidth: 0.8,
              borderColor: '#ffec00',
              backgroundColor: '#001e46'
            }}>
              <Text style={{
                color: '#ffec00',
                fontSize: 12,
                textAlign: 'center'
              }}>Voltar</Text>
            </TouchableOpacity>
          </View>

          <View />
        </ScrollView>
        <View style={{ flexDirection: 'row', height: Platform.OS === 'android' ? 55 : 75, backgroundColor: '#ffec00', alignItems: 'center' }}>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('MenuDestino1') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rdestino.png')}
              style={{
                height: 50,
                width: 50,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('Inicio') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rreservas.png')}
              style={{
                height: 50,
                width: 50,
                marginBottom: 6,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>
          <TouchableOpacity style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rpontos.png')}
              style={{
                height: 50,
                width: 50,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('MinhaConta') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
            <Image
              source={require('../img/rconta.png')}
              style={{
                height: 50,
                width: 50,
                alignSelf: 'center',
                resizeMode: 'stretch',
              }} />
          </TouchableOpacity>

        </View>

      </View>
    )
  }
}

export default MinhasReservasR;
const styles = StyleSheet.create({
  gridView: {
    height: 300,
  },
  itemContainer: {
    elevation: 4,
    height: 70,
    marginHorizontal: 10,
    marginBottom: 5,
  },
  itemContainerItem: {
    elevation: 4,
    height: 50,
    marginLeft: 45,
    marginBottom: 5,
  },
  itemContainerProfissa: {
    elevation: 4,
    height: 100,
    marginLeft: 45,
    marginRight: -5,
    marginBottom: 5,
  },

});