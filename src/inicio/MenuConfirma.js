import React, { Component } from "react";
import {
    ScrollView,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    StatusBar,
    TextInput,
    Alert,
    Modal,
    Image,
    Platform,
    ToastAndroid,
    ActivityIndicator,
    BackHandler,
    RefreshControl
} from "react-native";
import { checkInternetConnection } from 'react-native-offline';;
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/EvilIcons';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import GridView from 'react-native-super-grid';
import { Searchbar, Provider as PaperProvider } from 'react-native-paper';
import API, { caminhoAPI, versaoAPI } from '../utils/caminhosAPI'

class MenuConfirma extends React.Component {
    state = {
        tipo: '',
        grupo: '',
        dia: '',
        diaFim: '',
        adultos: '0',
        criancas: '0',
        idUsuario: 0,
        codSolicita: '',
        vazio: false,
        visible: false,
        info: []

    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('acomodacoesReserva')
            return true;
        })

        var RNFS = require('react-native-fs');
        var path = RNFS.DocumentDirectoryPath + '/reservaConfirma.json';
        var pathU = RNFS.DocumentDirectoryPath + '/usuario.json';

        RNFS.readFile(pathU, 'utf8')
            .then((contents) => {
                if (!(contents == '[{}]')) {

                    this.setState({
                        idUsuario: String(JSON.parse(contents)[0].codigo),
                        taxa: String(JSON.parse(contents)[0].taxa),
                        tipo: JSON.parse(contents)[0].tipo,
                        token: JSON.parse(contents)[0].token
                    })

                }
            })
            .catch((err) => {
                this.setState({ idUsuario: '0' })
                alert(err.message)
                console.log(err.message, err.code);
            });

        RNFS.readFile(path, 'utf8')
            .then((contents) => {
                console.warn(contents);
                if (!(contents == '[{}]')) {
                    this.setState({
                        info: JSON.parse(contents)[0].info,
                        image: JSON.parse(contents)[0].image,
                        codSolicita: JSON.parse(contents)[0].codSolicita,
                        dia: String(JSON.parse(contents)[0].inicio).substr(-2) + ' DE ' + (String(JSON.parse(contents)[0].inicio).substr(5, 2) == '01' ? 'JANEIRO' :
                            String(JSON.parse(contents)[0].inicio).substr(5, 2) == '02' ? 'FEVEREIRO' :
                                String(JSON.parse(contents)[0].inicio).substr(5, 2) == '03' ? 'MARÇO' :
                                    String(JSON.parse(contents)[0].inicio).substr(5, 2) == '04' ? 'ABRIL' :
                                        String(JSON.parse(contents)[0].inicio).substr(5, 2) == '05' ? 'MAIO' :
                                            String(JSON.parse(contents)[0].inicio).substr(5, 2) == '06' ? 'JUNHO' :
                                                String(JSON.parse(contents)[0].inicio).substr(5, 2) == '07' ? 'JULHO' :
                                                    String(JSON.parse(contents)[0].inicio).substr(5, 2) == '08' ? 'AGOSTO' :
                                                        String(JSON.parse(contents)[0].inicio).substr(5, 2) == '09' ? 'SETEMBRO' :
                                                            String(JSON.parse(contents)[0].inicio).substr(5, 2) == '10' ? 'OUTUBRO' :
                                                                String(JSON.parse(contents)[0].inicio).substr(5, 2) == '11' ? 'NOVEMBRO' :
                                                                    String(JSON.parse(contents)[0].inicio).substr(5, 2) == '12' ? 'DEZEMBRO' : null),
                        diaFim: String(JSON.parse(contents)[0].fim).substr(-2) + ' DE ' + (String(JSON.parse(contents)[0].fim).substr(5, 2) == '01' ? 'JANEIRO' :
                            String(JSON.parse(contents)[0].fim).substr(5, 2) == '02' ? 'FEVEREIRO' :
                                String(JSON.parse(contents)[0].fim).substr(5, 2) == '03' ? 'MARÇO' :
                                    String(JSON.parse(contents)[0].fim).substr(5, 2) == '04' ? 'ABRIL' :
                                        String(JSON.parse(contents)[0].fim).substr(5, 2) == '05' ? 'MAIO' :
                                            String(JSON.parse(contents)[0].fim).substr(5, 2) == '06' ? 'JUNHO' :
                                                String(JSON.parse(contents)[0].fim).substr(5, 2) == '07' ? 'JULHO' :
                                                    String(JSON.parse(contents)[0].fim).substr(5, 2) == '08' ? 'AGOSTO' :
                                                        String(JSON.parse(contents)[0].fim).substr(5, 2) == '09' ? 'SETEMBRO' :
                                                            String(JSON.parse(contents)[0].fim).substr(5, 2) == '10' ? 'OUTUBRO' :
                                                                String(JSON.parse(contents)[0].fim).substr(5, 2) == '11' ? 'NOVEMBRO' :
                                                                    String(JSON.parse(contents)[0].fim).substr(5, 2) == '12' ? 'DEZEMBRO' : null),
                    })
                } else {
                    this.setState({ tipo: '' })
                }
            })
            .catch((err) => {
                this.setState({ tipo: '' })
                console.log(err.message, err.code);
            });
    }

    confirma = () => {
        Alert.alert(
            'Confirmação',
            'Deseja confirma esta Reserva ?',
            [
                { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                {
                    text: 'SIM', onPress: async () => {
                        this.setState({
                            refresh: true
                        })

                        const isConnected = await checkInternetConnection();

                        if (isConnected) {
                            let dados = [];
                            dados.push({
                                "Imovel": this.state.info.Codigo,
                                "Solicitacao": this.state.codSolicita
                            })

                            fetch(caminhoAPI() + '/solicitarReserva', {
                                method: 'POST',
                                headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json',
                                    'Versao': versaoAPI,
                                    'Auth': 'Bearer ' + this.state.token
                                    //'Cliente': this.state.idUsuario,
                                    //'Tipo': this.state.tipo
                                },
                                body: String(JSON.stringify(dados)).substr(1).substr(-(String(JSON.stringify(dados)).length - 1), (String(JSON.stringify(dados)).length - 2))
                            }
                            ).then((response) => {
                                const statusCode = response.status;
                                return Promise.all([statusCode]);
                            })
                                .then(([statusCode]) => {
                                    if (statusCode == 200) {
                                        const RNFS = require('react-native-fs');
                                        const pro = RNFS.DocumentDirectoryPath + '/reservaselect.json';

                                        let dados = [];

                                        dados.push({
                                            "cidade": String(this.state.info.Titulo).toUpperCase(),
                                            "diaIni": (this.state.dia),
                                            "diaFim": (this.state.diaFim),
                                            "adultos": this.state.info.Pessoas,
                                            "criancas": this.state.info.Criancas,
                                        })

                                        RNFS.writeFile(pro, JSON.stringify(dados), 'utf8')
                                            .then((success) => {
                                                console.log('FILE WRITTEN!');
                                            })
                                            .catch((err) => {
                                                console.log(err.message);
                                            });


                                        this.props.navigation.navigate('MessageAguardar')

                                    } else {
                                        alert('Falhou, tente mais tarde')
                                        this.setState({
                                            refresh: false
                                        })
                                    }
                                })
                                .catch((error) => {
                                    alert('Informações não são válidas');
                                    this.setState({
                                        refresh: false
                                    })
                                });

                        } else {
                            alert('Sem conexão com a internet!')
                            this.setState({
                                refresh: false
                            })
                        }
                    }
                },
            ]
        );

    }

    render() {

        return (
            <View style={{ backgroundColor: '#001e46', height: '100%' }}>
                <StatusBar
                    barStyle="light-content"
                    backgroundColor='#001e46'
                />

                <TouchableOpacity onPress={() => {
                    try {
                        this.props.screenProps.myDrawerNavigation.toggleDrawer();
                    } catch (e) {
                        this.props.navigation.toggleDrawer();
                    }
                }} style={{ marginLeft: 20, marginTop: 40 }}>
                    <Iconr name="menu" color='#ffec00' style={{ padding: 5 }} size={40} />
                </TouchableOpacity>

                <ScrollView style={{ backgroundColor: '#001e46', height: '100%' }}>
                    <View
                        style={{
                            height: 600,
                            marginHorizontal: 30,
                            marginBottom: 10,
                            borderWidth: 0.7,
                            borderColor: this.state.info.CategoriaDescricao == 'Ouro' ? '#ffec00' :
                                this.state.info.CategoriaDescricao == 'Prata' ? '#ffec00' :
                                    this.state.info.CategoriaDescricao == 'Bronze' ? '#ffec00' : '#ffec00',
                            borderRadius: 20,
                            backgroundColor: '#001e46',
                            elevation: 4,
                        }}>
                        <Image
                            style={{
                                resizeMode: 'cover',
                                width: '100%',
                                height: '40%',
                                marginRight: 1,
                                borderTopLeftRadius: 20,
                                borderTopRightRadius: 20
                            }}
                            source={String(this.state.image) == 'undefined' ? require('../img/praia1.jpg') : { uri: this.state.image }} />

                        <View style={{
                            height: 110,
                            width: 140,
                            backgroundColor: this.state.info.CategoriaDescricao == 'Ouro' ? '#ffec00' :
                                this.state.info.CategoriaDescricao == 'Prata' ? '#ffec00' :
                                    this.state.info.CategoriaDescricao == 'Bronze' ? '#ffec00' : '#ffec00',
                            marginLeft: 15,
                            marginTop: -96
                        }}>
                            <View style={{
                                backgroundColor: '#001e46',
                                justifyContent: 'center',
                                height: 30
                            }}>
                                <Text style={{
                                    color: '#ffec00',
                                    fontWeight: 'bold',
                                    fontSize: 16,
                                    textAlign: 'center'
                                }}>FALTA POUCO!</Text>
                            </View>

                            <View style={{ justifyContent: 'center', flex: 1 }}>
                                <Text style={{
                                    color: '#001e46',
                                    fontWeight: 'bold',
                                    fontSize: 14,
                                    marginLeft: 20
                                }}>{String(this.state.info.Titulo).toUpperCase()}</Text>
                                <Text style={{
                                    color: '#001e46',
                                    fontSize: 12,
                                    marginLeft: 20
                                }}></Text>
                            </View>
                        </View>

                        <View style={{
                            marginHorizontal: 15,
                            borderBottomWidth: 0.7,
                            paddingTop: 15,
                            paddingBottom: 5,
                            borderColor: 'white'
                        }}>
                            <Text style={{
                                color: this.state.info.CategoriaDescricao == 'Ouro' ? '#ffec00' :
                                    this.state.info.CategoriaDescricao == 'Prata' ? '#ffec00' :
                                        this.state.info.CategoriaDescricao == 'Bronze' ? '#ffec00' : '#ffec00',
                                fontSize: 14,
                                fontWeight: 'bold',
                            }}>ATENÇÃO</Text>

                            <Text style={{
                                marginTop: 10,
                                color: 'white',
                                fontSize: 10,
                            }}>Conﬁrme apenas se realmente deseja concretizar essa reserva. <Text style={{ fontWeight: 'bold' }}>   O CANCELAMENTO PODE GERAR TAXA CONTRATUAIS </Text></Text>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 15 }}>
                                <View />
                                <Text style={{
                                    color: this.state.info.CategoriaDescricao == 'Ouro' ? '#ffec00' :
                                        this.state.info.CategoriaDescricao == 'Prata' ? '#ffec00' :
                                            this.state.info.CategoriaDescricao == 'Bronze' ? '#ffec00' : '#ffec00',
                                    fontSize: 8,
                                    marginTop: 20,
                                    fontWeight: 'bold',
                                }}>VALOR TAXA  <Text style={{ fontSize: 8 }}>R${this.state.taxa}</Text></Text>

                            </View>
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            marginHorizontal: 15,
                            paddingVertical: 15,
                            borderBottomWidth: 0.7,
                            borderColor: 'white',
                            alignItems: 'center'
                        }}>

                            <IconF name="calendar" color='#ffec00' size={25} />

                            <Text style={{ color: 'white', marginHorizontal: 10, fontSize: 10 }}> {this.state.dia} - {this.state.diaFim}</Text>

                            <Icon name="downcircleo" color='#ffec00' size={10} />
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            marginHorizontal: 15,
                            borderBottomWidth: 0.7,
                            borderColor: 'white',
                            paddingVertical: 10,
                            alignItems: 'center'
                        }}>
                            <Text style={{
                                color: 'white',
                                fontSize: 10,
                                marginRight: 10
                            }}>ADULTOS</Text>

                            <Text style={{
                                fontSize: 20,
                                color: '#ffec00',
                                marginRight: 5
                            }}>{this.state.info.Pessoas}</Text>


                            <Text style={{
                                color: 'white',
                                fontSize: 10,
                                marginRight: 10
                            }}>CRIANÇAS ATÉ 4 ANOS</Text>

                            <Text style={{
                                fontSize: 20,
                                color: '#ffec00',
                                marginRight: 10
                            }}>{this.state.info.Criancas}</Text>

                            {/*<TouchableOpacity onPress={() => { this.props.navigation.navigate('Inicio') }} style={{
                                paddingHorizontal: 10,
                                justifyContent: 'center',
                                paddingVertical: 5,
                                paddingHorizontal: 10,
                                marginLeft: 15,
                                borderRadius: 30,
                                borderWidth: 0.8,
                                borderColor: '#ffec00',
                                backgroundColor: '#001e46'
                            }}>
                                <Text style={{
                                    color: '#ffec00',
                                    fontSize: 8,
                                    textAlign: 'center'
                                }}>Alterar</Text>
                            </TouchableOpacity>*/}
                        </View>

                        {/*<View style={{
                            marginHorizontal: 15,
                            borderBottomWidth: 0.7,
                            paddingVertical: 15,
                            borderColor: 'white'
                        }}>
                            <Text style={{
                                color: this.state.info.CategoriaDescricao == 'Ouro' ? '#ffec00' :
                                    this.state.info.CategoriaDescricao == 'Prata' ? '#ffec00' :
                                        this.state.info.CategoriaDescricao == 'Bronze' ? '#ffec00' : '#ffec00',
                                fontSize: 10,
                                fontWeight: 'bold',
                            }}>É CONVIDADO?</Text>

                            <TextInput
                                style={{
                                    height: 30,
                                    borderWidth: 0.7,
                                    borderColor: '#ffec00',
                                    borderRadius: 10,
                                    marginTop: 10,
                                    fontSize: 9,
                                    padding: 5,
                                    color: 'white',
                                    paddingLeft: 15
                                }}
                                placeholder='INSIRA AQUI SEU CÓDIGO PROMOCIONAL'
                                placeholderTextColor='silver'
                                underlineColorAndroid='transparent' />
                        </View>*/}

                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            marginRight: 15,
                            marginTop: 20,
                            marginBottom: 20
                        }}>
                            <View style={{
                                marginHorizontal: 15
                            }}>
                                <Text style={{
                                    color: 'white',
                                    fontSize: 10,
                                }}>{}</Text>
                                <Text style={{
                                    color: 'white',
                                    fontSize: 10,
                                }}>{}</Text>
                                <Text style={{
                                    color: 'white',
                                    fontSize: 8,
                                }}>{}</Text>
                                <Text style={{
                                    color: '#ffec00',
                                    fontWeight: 'bold',
                                    fontSize: 10,
                                    width: 120
                                }}>{}</Text>
                                <Text style={{
                                    color: '#ffec00',
                                    fontSize: 10,
                                }}></Text>

                                <Text style={{
                                    color: this.state.info.CategoriaDescricao == 'Ouro' ? '#ffec00' :
                                        this.state.info.CategoriaDescricao == 'Prata' ? '#ffec00' :
                                            this.state.info.CategoriaDescricao == 'Bronze' ? '#ffec00' : '#ffec00',
                                    fontSize: 10,
                                    fontWeight: 'bold',
                                }}><Text style={{ fontSize: 18, fontWeight: 'bold' }}>R${String(parseFloat(this.state.info.Total).toFixed(2))}</Text></Text>
                                <Text style={{
                                    color: this.state.info.CategoriaDescricao == 'Ouro' ? '#ffec00' :
                                        this.state.info.CategoriaDescricao == 'Prata' ? '#ffec00' :
                                            this.state.info.CategoriaDescricao == 'Bronze' ? '#ffec00' : '#ffec00',
                                    fontSize: 10,
                                    fontWeight: 'bold',
                                }}>VALOR TOTAL</Text>

                            </View>

                            <TouchableOpacity onPress={() => { this.confirma() }} style={{
                                paddingVertical: 15,
                                justifyContent: 'center',
                                borderRadius: 15,
                                backgroundColor: this.state.info.CategoriaDescricao == 'Ouro' ? '#ffec00' :
                                    this.state.info.CategoriaDescricao == 'Prata' ? '#ffec00' :
                                        this.state.info.CategoriaDescricao == 'Bronze' ? '#ffec00' : '#ffec00',
                            }}>
                                {this.state.refresh ? <ActivityIndicator size='small' color='#001e46' /> :
                                    <Text style={{
                                        color: '#001e46',
                                        fontSize: 14,
                                        width: 100,
                                        fontWeight: 'bold',
                                        textAlign: 'center'
                                    }}>CONFIRMAR RESERVA</Text>}
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginHorizontal: 30
                    }}>
                        <View />

                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('MenuP') }} style={{
                            paddingHorizontal: 30,
                            justifyContent: 'center',
                            paddingVertical: 5,
                            marginRight: 15,
                            marginBottom: 20,
                            borderRadius: 30,
                            borderWidth: 0.8,
                            borderColor: '#ffec00',
                            backgroundColor: '#001e46'
                        }}>
                            <Text style={{
                                color: '#ffec00',
                                fontSize: 12,
                                textAlign: 'center'
                            }}>FAZER NOVA BUSCA</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>

                <View style={{ flexDirection: 'row', height: Platform.OS === 'android' ? 55 : 75, backgroundColor: '#ffec00', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('MenuDestino1') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rdestino.png')}
                            style={{
                                height: 50,
                                width: 50,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('Inicio') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rreservas.png')}
                            style={{
                                height: 50,
                                width: 50,
                                marginBottom: 6,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rpontos.png')}
                            style={{
                                height: 50,
                                width: 50,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('MinhaConta') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rconta.png')}
                            style={{
                                height: 50,
                                width: 50,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>

                </View>

            </View>

        );
    }
}

export default MenuConfirma;
const styles = StyleSheet.create({
    itemContainerProfissa: {
        elevation: 4,
        height: 100,
        marginBottom: 5,
    },
});