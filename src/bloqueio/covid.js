import React, { Component } from 'react'
import {
    Text,
    View,
    Image,
    Dimensions,
    StatusBar,
    TouchableOpacity
} from 'react-native'

export default class covid extends Component {
    render() {
        let { height, width } = Dimensions.get("screen")
        return (
            <View style={{
                backgroundColor: "#ffec00",
                flex: 1,
                justifyContent: "space-between"
            }}>
                <StatusBar
                    barStyle="dark-content"
                    backgroundColor='#ffec00'
                />
                <View />

                <Image
                    style={{
                        height: width,
                        width: width
                    }}
                    source={require("../img/covid.jpeg")}
                />

                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.navigate('MenuP')
                    }}
                    style={{
                        marginHorizontal: 20,
                        height: 40,
                        borderRadius: 40,
                        backgroundColor: "#001e46",
                        justifyContent: "center",
                        marginBottom: 40
                    }}>
                    <Text style={{
                        color: "#ffec00",
                        textAlign: "center",
                        fontWeight: "bold"
                    }}>VOLTAR</Text>
                </TouchableOpacity>
            </View>
        )
    }
}
