import React, { Component } from 'react';
import {
  View,
  Button,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  StatusBar,
  Image,
  Alert,
  Linking,
  ImageBackground,
  Platform,
  Modal,
} from 'react-native';
import { checkInternetConnection } from 'react-native-offline';
import Icon from 'react-native-vector-icons/AntDesign';
import { ActivityIndicator } from 'react-native-paper';
import API, { caminhoAPI, versaoAPI } from '../utils/caminhosAPI'
import { atBloqueio } from "../index"

class Login extends Component {
  state = {
    emailS: '',
    email: '',
    login: '',
    senha: '',
    cancelamento: 0,
    idUsuario: '',
    modalVisibleSenha: false,
  }

  static navigationOptions = {
    drawerLockMode: 'locked-closed',
  };

  async componentDidMount() {
    const RNFS = require('react-native-fs');
    const cadastro = RNFS.DocumentDirectoryPath + '/usuario.json';

    let dados = [];
    dados.push({})

    RNFS.writeFile(cadastro, JSON.stringify(dados), 'utf8')
      .then((success) => {
        console.log('FILE WRITTEN!');
      })
      .catch((err) => {
        console.log(err.message);
      });

    const isConnected = await checkInternetConnection();
    if (isConnected) {
      fetch(caminhoAPI() + '/consultarPropriedade', {}
      ).then((response) => {
        const versao = response.headers.get('versao');

        const statusCode = response.status;
        let responseJson = [];

        if (statusCode == 200) {
          responseJson = response.json();
        }

        return Promise.all([responseJson, statusCode, versao])
      })
        .then(([responseJson1, statusCode, versao]) => {
          if (statusCode == 200) {
            this.setState({
              refresh: false,
              cancelamento: responseJson1.valorTaxaCancelamento
            })

            if (!(versaoAPI == versao)) {
              Alert.alert(
                'Esta versão está desatualizada!',
                'Para que o aplicativo funcione corretamente o mesmo precisa ser atualizado. Deseja atualizar agora ?',
                [
                  { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                  { text: 'SIM', onPress: () => Linking.openURL('https://play.google.com/store/apps/details?id=com.entremares') },
                ]
              );
            }

          } else {
            this.setState({
              vazio: true,
              refresh: false
            })
          }
        })
        .catch((error) => {
          this.setState({
            vazio: true,
            grupos: [],
            refresh: false
          })
        });

    } else {
      alert('Sem conexão com a internet!')
      this.setState({
        vazio: true,
        refresh: false
      })
    }
  }

  login = async () => {
    if (this.state.login == '') {
      alert('Email deve ser preenchido')
      return
    }

    if (this.state.senha == '') {
      alert('Senha deve ser preenchido')
      return
    }


    this.setState({
      refresh: true
    })

    const isConnected = await checkInternetConnection();

    if (isConnected) {
      let dados = [];
      dados.push({
        Email: this.state.login,
        Senha: this.state.senha
      })

      fetch(caminhoAPI() + '/login', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Versao': versaoAPI
        },
        body: String(JSON.stringify(dados)).substr(1).substr(-(String(JSON.stringify(dados)).length - 1), (String(JSON.stringify(dados)).length - 2))
      }
      ).then((response) => {
        const token = response.headers.get('token');
        const statusCode = response.status;
        let responseJson = [];

        if (statusCode == 200) {
          responseJson = response.json();
        }

        return Promise.all([responseJson, statusCode, token]);
      })
        .then(([responseJson, statusCode, token]) => {
          if (statusCode == 200) {
            const RNFS = require('react-native-fs');
            const cadastro = RNFS.DocumentDirectoryPath + '/usuario.json';

            let dados = [];
            dados.push({
              "email": responseJson.Email,
              "codigo": responseJson.Codigo,
              "tipo": responseJson.Tipo,
              "nome": responseJson.Nome,
              "bloqueio": responseJson.BloqueioReserva,
              "taxa": this.state.cancelamento,
              "token": token
            })

            if (responseJson.BloqueioReserva == 1) {
              atBloqueio(true)
            }

            this.setState({
              refresh: false
            })

            RNFS.writeFile(cadastro, JSON.stringify(dados), 'utf8')
              .then((success) => {
                console.log('FILE WRITTEN!');
              })
              .catch((err) => {
                console.log(err.message);
              });

            this.props.navigation.navigate('MenuP');

          } else {
            alert('Login ou senha não encontrados')
            this.setState({
              refresh: false
            })
          }
        })
        .catch((error) => {
          alert('falha' + JSON.stringify(error));
          this.setState({
            refresh: false
          })
        });

    } else {
      alert('Sem conexão com a internet!')
      this.setState({
        refresh: false
      })
    }
  }

  /* tela principal */
  render() {
    return (
      <ImageBackground source={require('../img/cover.jpg')} style={{ flex: 1, resizeMode: 'cover', backgroundColor: '#001e46', justifyContent: 'flex-end' }}>
        <StatusBar
          barStyle="light-content"
          translucent={true}
          backgroundColor={'transparent'}
        />

        <Image
          source={require('../img/logo_branca-01.png')}
          style={{
            height: 100,
            width: 200,
            alignSelf: 'center',
            marginBottom: 40,
            resizeMode: 'stretch',
          }} />

        <TextInput
          value={this.state.login}
          onChangeText={login => this.setState({ login })}
          keyboardType='email-address'
          underlineColorAndroid='transparent'
          placeholder='Login'
          placeholderTextColor='white'
          style={{
            backgroundColor: 'transparent',
            marginHorizontal: 40,
            marginBottom: 20,
            fontSize: 18,
            color: 'white',
            fontWeight: 'bold',
            borderBottomWidth: 0.7,
            borderBottomColor: 'white'
          }} />

        <TextInput
          value={this.state.senha}
          onChangeText={senha => this.setState({ senha })}
          underlineColorAndroid='transparent'
          placeholder='Senha'
          secureTextEntry={true}
          placeholderTextColor='white'
          style={{
            backgroundColor: 'transparent',
            marginHorizontal: 40,
            marginBottom: 20,
            fontSize: 18,
            color: 'white',
            fontWeight: 'bold',
            borderBottomWidth: 0.7,
            borderBottomColor: 'white'
          }} />


        <TouchableOpacity disabled={this.state.refresh} onPress={() => this.login()} style={{
          height: 40,
          width: 120,
          paddingHorizontal: 10,
          justifyContent: 'center',
          paddingVertical: 5,
          marginLeft: 40,
          marginBottom: 35,
          borderRadius: 30,
          borderWidth: 0.8,
          borderColor: '#ffec00',
          backgroundColor: 'transparent'
        }}>
          {
            this.state.refresh ?
              <ActivityIndicator size='small' color='#ffec00' />
              :
              <Text style={{
                color: '#ffec00',
                fontSize: 12,
                textAlign: 'center'
              }}>Entrar</Text>
          }
        </TouchableOpacity>

        <TouchableOpacity
          disabled={this.state.refresh}
          onPress={() => this.props.navigation.navigate('MenuDestino1')} style={{
            height: 40,
            paddingHorizontal: 10,
            justifyContent: 'center',
            marginHorizontal: 40,
            marginBottom: 35,
            borderRadius: 30,
            backgroundColor: '#ffec00'
          }}>
          <Text style={{
            color: '#001e46',
            fontSize: 16,
            textAlign: 'center'
          }}>CONTINUAR NAVEGANDO</Text>
        </TouchableOpacity>

        <TouchableOpacity
          disabled={this.state.refresh}
          onPress={() => { Linking.openURL('http://entremares.tur.br/associe-se/') }}>
          <Text style={{
            fontWeight: 'bold',
            textAlign: 'center',
            fontSize: 16,
            marginBottom: 40,
            color: '#ffec00'
          }}>QUERO ME ASSOCIAR</Text>
        </TouchableOpacity>
      </ImageBackground>
    )
  }
}

export default Login;

const styles = StyleSheet.create({

});