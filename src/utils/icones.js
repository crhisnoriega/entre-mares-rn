const images = {
    ÁguasTermais: require('../img/icones_destinos/aguas_termais.png'),
    Convênios: require('../img/icones_destinos/convenios.png'),
    Excursões: require('../img/icones_destinos/excursoes.png'),
    Praias: require('../img/icones_destinos/praias.png'),
    TurismoCristão: require('../img/icones_destinos/turismo_cristao.png'),
    PontosTurísticos: require('../img/icones_destinos/pontos_turisticos.png'),
    Serra: require('../img/icones_destinos/serra.png'),
    Pesca: require('../img/icones_destinos/pesca.png'),
};

export default images;