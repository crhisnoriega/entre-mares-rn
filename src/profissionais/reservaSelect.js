import React, { Component } from "react";
import {
    ScrollView,
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
    StatusBar,
    Modal,
    Platform,
    ToastAndroid,
    NetInfo,
    ActivityIndicator,
    Linking,
    RefreshControl
} from "react-native";
import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/EvilIcons';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';

class reservaSelect extends React.Component {
    state = {
        cidade: '',
        local: '',
        ava: '',
        valor: '',
        adultos: '',
        criancas: ''
    }

    componentDidMount() {
        var RNFS = require('react-native-fs');
        var path = RNFS.DocumentDirectoryPath + '/reservaselect.json';

        RNFS.readFile(path, 'utf8')
            .then((contents) => {
                console.warn(contents);
                if (!(contents == '[{}]')) {
                    this.setState({
                        cidade: JSON.parse(contents)[0].cidade,
                        local: JSON.parse(contents)[0].local,
                        ava: JSON.parse(contents)[0].ava,
                        valor: JSON.parse(contents)[0].valor,
                        adultos: JSON.parse(contents)[0].adultos,
                        criancas: JSON.parse(contents)[0].criancas,
                    })
                } else {
                    this.setState({ tipo: '' })
                }
            })
            .catch((err) => {
                this.setState({ tipo: '' })
                console.log(err.message, err.code);
            });
    }

    render() {

        return (
            <View style={{ backgroundColor: '#001e46', height: '100%' }}>

                <StatusBar
                    barStyle="light-content"
                    backgroundColor='#001e46'
                />

                <TouchableOpacity onPress={() => {
                    try {
                        this.props.screenProps.myDrawerNavigation.toggleDrawer();
                    } catch (e) {
                        this.props.navigation.toggleDrawer();
                    }
                }} style={{ marginLeft: 20, marginTop: 40 }}>
                    <Iconr name="menu" color='#ffec00' style={{ padding: 5 }} size={40} />
                </TouchableOpacity>

                <ScrollView style={{ backgroundColor: '#001e46', height: '100%' }}>
                    <View style={{
                        flexDirection: 'row',
                        borderBottomWidth: 0.5,
                        paddingVertical: 10,
                        borderBottomColor: 'white',
                        marginTop: 20,
                        marginHorizontal: 29,
                    }}>
                        <Text
                            style={{
                                backgroundColor: 'transparent',
                                fontSize: 18,
                                color: 'white',
                                fontWeight: 'bold',
                            }}>{this.state.cidade.toUpperCase()}</Text>

                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('Inicio') }} style={{ marginLeft: 5, marginTop: -10 }}>
                            <Icon name="closecircle" color='#ffec00' style={{ padding: 5 }} size={20} />
                        </TouchableOpacity>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        marginHorizontal: 29,
                        paddingVertical: 15,
                        alignItems: 'center'
                    }}>

                        <IconF name="calendar" color='#ffec00' size={25} />

                        <Text style={{ color: 'white', marginHorizontal: 10 }}> 11 DE MAIO - 15 DE MAIO</Text>

                        <Icon name="downcircleo" color='#ffec00' size={10} />
                    </View>

                    <View style={{
                        borderBottomWidth: 0.5,
                        marginHorizontal: 29,
                        borderBottomColor: 'white'
                    }} />

                    <View style={{
                        flexDirection: 'row',
                        marginHorizontal: 29,
                        paddingVertical: 10,
                        alignItems: 'center'
                    }}>
                        <Text style={{
                            color: 'white',
                            fontSize: 10,
                            marginRight: 10
                        }}>ADULTOS</Text>

                        <Text style={{
                            fontSize: 22,
                            color: '#ffec00',
                            marginRight: 20
                        }}>{this.state.adultos}</Text>


                        <Text style={{
                            color: 'white',
                            fontSize: 10,
                            marginRight: 10
                        }}>CRIANÇAS</Text>

                        <Text style={{
                            fontSize: 22,
                            color: '#ffec00',
                            marginRight: 10
                        }}>{this.state.criancas}</Text>
                    </View>

                    <View style={{
                        borderBottomWidth: 0.5,
                        marginHorizontal: 29,
                        borderBottomColor: 'white'
                    }} />

                    <View>
                        <Text style={{
                            color: '#ffec00',
                            fontSize: 17,
                            fontWeight: 'bold',
                            marginLeft: 29,
                            marginTop: 29
                        }}>ENCONTRAMOS <Text style={{ fontSize: 30, color: 'white' }}>14</Text> ACOMODAÇÕES</Text>
                    </View>


                    <View
                        style={{
                            height: 250,
                            marginHorizontal: 29,
                            borderWidth: 0.7,
                            borderColor: '#ffec00',
                            borderRadius: 20,
                            marginTop: 20,
                            backgroundColor: '#001e46',
                            elevation: 4,
                        }}>
                        <Image
                            style={{
                                resizeMode: 'cover',
                                width: '100%',
                                height: '65%',
                                borderTopLeftRadius: 20,
                                borderTopRightRadius: 20
                            }}
                            source={require('../img/praia1.jpg')} />

                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            position: 'absolute',
                            width: '100%'
                        }}>
                            <View />
                            <View style={{
                                height: '220%',
                                width: 35,
                                backgroundColor: '#ffec00',
                                marginTop: 36,
                                justifyContent: 'center'
                            }}>
                                <Text style={{
                                    color: '#001e46',
                                    fontSize: 12,
                                    fontWeight: 'bold',
                                    transform: [
                                        { rotate: '270deg' },
                                        { translateX: -((40 / 2) - (60 / 2)) },
                                        { translateY: ((2 / 2) - (85 / 2)) }
                                    ],
                                    width: 120
                                }}>MAIS RESERVADA</Text>
                            </View>
                        </View>

                        <View style={{
                            height: 15,
                            width: 130,
                            backgroundColor: '#ffec00',
                            marginLeft: 15,
                            marginTop: -8
                        }} />

                        <Text style={{
                            color: '#ffec00',
                            fontSize: 14,
                            fontWeight: 'bold',
                            marginLeft: 15,
                            marginTop: 10
                        }}>{this.state.cidade.toUpperCase()}</Text>

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{
                                color: 'white',
                                fontSize: 10,
                                marginLeft: 15,
                                marginTop: 2
                            }}>{this.state.local.toUpperCase()}</Text>
                            <Text style={{
                                color: '#ffec00',
                                fontSize: 14,
                                fontWeight: 'bold',
                                marginLeft: 5,
                            }}>{String(this.state.ava + '/').toUpperCase()}<Text style={{ fontSize: 10, color: '#ffec00', fontWeight: '100' }}>5.0</Text></Text>
                        </View>

                        <View style={{ flexDirection: 'row', height: 50 }}>
                            <Text style={{
                                color: 'white',
                                fontSize: 10,
                                marginLeft: 15,
                                marginTop: 2
                            }}>R$</Text>
                            <Text style={{
                                color: '#ffec00',
                                fontSize: 25,
                                fontWeight: 'bold',
                                marginLeft: 2,
                                marginTop: -4
                            }}>{String(parseFloat(this.state.valor).toFixed(0))}</Text>
                            <View>
                                <Text style={{
                                    color: '#ffec00',
                                    fontSize: 12,
                                    fontWeight: 'bold',
                                    marginLeft: 2,
                                    marginTop: -2
                                }}>,{String(parseFloat(this.state.valor).toFixed(2)).substr(-2, 2)}</Text>
                                <Text style={{
                                    color: 'white',
                                    fontSize: 8,
                                    marginLeft: 5
                                }}>NOITE</Text>
                            </View>
                        </View>
                    </View>

                    <TouchableOpacity style={{
                        height: 30,
                        width: 120,
                        paddingHorizontal: 10,
                        justifyContent: 'center',
                        paddingVertical: 5,
                        marginLeft: 29,
                        marginTop: 15,
                        marginBottom: 25,
                        borderRadius: 30,
                        borderWidth: 0.8,
                        borderColor: '#ffec00',
                        backgroundColor: '#001e46'
                    }}>
                        <Text style={{
                            color: '#ffec00',
                            fontSize: 12,
                            textAlign: 'center'
                        }}>Nova Busca</Text>
                    </TouchableOpacity>

                    <View />
                </ScrollView>

            </View>
        );
    }
}


export default reservaSelect;

const styles = StyleSheet.create({
    itemContainerProfissa: {
        elevation: 4,
        height: 100,
        marginBottom: 5,
    },
    itemContainerProfissaDetalhe: {
        elevation: 4,
        height: 450,
        marginBottom: 5,
    },
});