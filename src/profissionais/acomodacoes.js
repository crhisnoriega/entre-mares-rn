import React, { Component } from "react";
import {
    ScrollView,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    StatusBar,
    TextInput,
    Modal,
    Alert,
    Image,
    Platform,
    ToastAndroid,
    ActivityIndicator,
    BackHandler,
    RefreshControl
} from "react-native";
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { checkInternetConnection } from 'react-native-offline';;
import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Ionicons';
import IconF from 'react-native-vector-icons/FontAwesome';
import IconFF from 'react-native-vector-icons/FontAwesome5';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import GridView from 'react-native-super-grid';
import { Searchbar, Provider as PaperProvider } from 'react-native-paper';
import API, { caminhoAPI, versaoAPI } from '../utils/caminhosAPI'

class acomodacoes extends React.Component {
    state = {
        tipo: '',
        grupo: '',
        local: '',
        idUsuario: 0,
        refreshing: false,
        vazio: false,
        visible: false,
        populares: [],
    }

     componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('MenuDestino1')
            return true;
        })

        var RNFS = require('react-native-fs');
        var path = RNFS.DocumentDirectoryPath + '/reservaCria.json';
        var pathU = RNFS.DocumentDirectoryPath + '/usuario.json';

        RNFS.readFile(pathU, 'utf8')
            .then((contents) => {
                if (!(contents == '[{}]')) {
                    this.setState({
                        idUsuario: String(JSON.parse(contents)[0].codigo),
                        tipoU: JSON.parse(contents)[0].tipo,
                        //token: JSON.parse(contents)[0].token
                    })
                } else {
                    this.setState({ idUsuario: '0' })
                }
            })
            .catch((err) => {
                this.setState({ idUsuario: '0' })
                alert(err.message)
                console.log(err.message, err.code);
            });

        RNFS.readFile(path, 'utf8')
            .then(async (contents) => {
                console.warn(contents);
                if (!(contents == '[{}]')) {
                    this.setState({
                        tipo: JSON.parse(contents)[0].tipo,
                        cidade: JSON.parse(contents)[0].cidade,
                        idCidade: parseInt(JSON.parse(contents)[0].id),
                        local: JSON.parse(contents)[0].local ? JSON.parse(contents)[0].local : '',
                    })

                    const isConnected = await checkInternetConnection();
                    if (isConnected) {
                        this.setState({
                            refresh: true,
                        })

                        fetch(caminhoAPI() + '/consultarApartamentosDestino?Destino=' + String(JSON.parse(contents)[0].local).toLowerCase(), {
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                                'Versao': versaoAPI
                            },
                        }
                        ).then((response) => {
                            const statusCode = response.status;
                            let responseJson1 = [];

                            if (statusCode == 200) {
                                responseJson1 = response.json();
                            }

                            return Promise.all([responseJson1, statusCode]);
                        })
                            .then(([responseJson, statusCode]) => {
                                if (responseJson.length > 0) {
                                    console.log('')
                                    this.setState({
                                        refresh: false,
                                        populares: responseJson,
                                    })

                                    const RNFS = require('react-native-fs');
                                    const pro = RNFS.DocumentDirectoryPath + '/direto.json';

                                    let dados = [];

                                    dados.push({
                                        "direto": false
                                    })

                                    RNFS.writeFile(pro, JSON.stringify(dados), 'utf8')
                                        .then((success) => {
                                            console.log('FILE WRITTEN!');
                                        })
                                        .catch((err) => {
                                            console.log(err.message);
                                        });

                                } else {
                                    this.setState({
                                        vazio: true,
                                        refresh: false
                                    })

                                    const RNFS = require('react-native-fs');
                                    const pro = RNFS.DocumentDirectoryPath + '/direto.json';

                                    let dados = [];

                                    dados.push({
                                        "direto": true
                                    })

                                    RNFS.writeFile(pro, JSON.stringify(dados), 'utf8')
                                        .then((success) => {
                                            console.log('FILE WRITTEN!');
                                        })
                                        .catch((err) => {
                                            console.log(err.message);
                                        });

                                    if (this.state.idUsuario == '0') {
                                        alert('Nenhuma acomodação foi encontrada nesse destino')
                                        this.props.navigation.navigate('MenuDestino1')
                                    } else {
                                        this.props.navigation.navigate('reservaSelect1')
                                    }

                                }
                            })
                            .catch((error) => {
                                alert(JSON.stringify(error))
                                this.setState({
                                    vazio: true,
                                    populares: [],
                                    refresh: false
                                })
                            });

                    } else {
                        alert('Sem conexão com a internet!')
                        this.setState({
                            vazio: true,
                            refresh: false
                        })
                    }
                } else {
                    this.setState({ tipo: '' })
                }
            })
            .catch((err) => {
                this.setState({ tipo: '' })
                console.log(err.message, err.code);
            });
    }

    select = (ap, image) => {
        const RNFS = require('react-native-fs');
        const pro = RNFS.DocumentDirectoryPath + '/reservaCria.json';

        let dados = [];

        dados.push({
            "tipo": this.state.tipo,
            "id": this.state.id,
            "cidade": this.state.local,
            "image": image,
            "local": ap
        })

        RNFS.writeFile(pro, JSON.stringify(dados), 'utf8')
            .then((success) => {
                console.log('FILE WRITTEN!');
            })
            .catch((err) => {
                console.log(err.message);
            });

        this.props.navigation.navigate('reservaSelect1')
    }

    refresh = async () => {
        const isConnected = await checkInternetConnection();
        if (isConnected) {
            this.setState({
                refresh: true,
            })

            fetch(caminhoAPI() + '/consultarApartamentosDestino?Destino=' + String(this.state.local).toLowerCase(), {
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Versao': versaoAPI
                },
            }
            ).then((response) => {
                const statusCode = response.status;
                let responseJson1 = [];

                if (statusCode == 200) {
                    responseJson1 = response.json();
                }

                return Promise.all([responseJson1, statusCode]);
            })
                .then(([responseJson, statusCode]) => {
                    if (responseJson.length > 0) {
                        console.log('')
                        this.setState({
                            refresh: false,
                            populares: responseJson
                        })

                        const RNFS = require('react-native-fs');
                        const pro = RNFS.DocumentDirectoryPath + '/direto.json';

                        let dados = [];

                        dados.push({
                            "direto": false
                        })

                        RNFS.writeFile(pro, JSON.stringify(dados), 'utf8')
                            .then((success) => {
                                console.log('FILE WRITTEN!');
                            })
                            .catch((err) => {
                                console.log(err.message);
                            });

                    } else {
                        this.setState({
                            vazio: true,
                            refresh: false
                        })

                        const RNFS = require('react-native-fs');
                        const pro = RNFS.DocumentDirectoryPath + '/direto.json';

                        let dados = [];

                        dados.push({
                            "direto": true
                        })

                        RNFS.writeFile(pro, JSON.stringify(dados), 'utf8')
                            .then((success) => {
                                console.log('FILE WRITTEN!');
                            })
                            .catch((err) => {
                                console.log(err.message);
                            });

                        this.props.navigation.navigate('reservaSelect1')
                    }
                })
                .catch((error) => {
                    alert(JSON.stringify(error))
                    this.setState({
                        vazio: true,
                        populares: [],
                        refresh: false
                    })
                });

        } else {
            alert('Sem conexão com a internet!')
            this.setState({
                vazio: true,
                refresh: false
            })
        }
    }

    render() {

        return (
            <View style={{ backgroundColor: '#001e46', height: '100%' }}>
                <StatusBar
                    barStyle="light-content"
                    backgroundColor='#001e46'
                />

                <TouchableOpacity onPress={() => {
                    try {
                        this.props.screenProps.myDrawerNavigation.toggleDrawer();
                    } catch (e) {
                        this.props.navigation.toggleDrawer();
                    }
                }} style={{ marginLeft: 20, marginTop: 40 }}>
                    <Iconr name="menu" color='#ffec00' style={{ padding: 5 }} size={40} />
                </TouchableOpacity>

                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this.refresh}
                        />
                    }
                    style={{ backgroundColor: '#001e46', height: '100%' }}>
                    {this.state.refresh ?
                        <ActivityIndicator size='small' color={'#ffec00'} />
                        :
                        <GridView
                            itemDimension={212}
                            items={this.state.populares}
                            style={styles.gridView}
                            renderItem={item => (
                                <View
                                    style={{
                                        marginHorizontal: 30,
                                        marginBottom: 10,
                                        borderWidth: 0.7,
                                        borderColor: item.item.CategoriaDescricao == 'Ouro' ? '#ffec00' :
                                            item.item.CategoriaDescricao == 'Prata' ? '#dfe6e9' :
                                                item.item.CategoriaDescricao == 'Bronze' ? '#ffbe76' : 'white',
                                        borderRadius: 20,
                                        backgroundColor: '#001e46',
                                        elevation: 4,
                                    }}>
                                    <TouchableOpacity onPress={() => {

                                    }}>
                                        <Image
                                            style={{
                                                resizeMode: 'cover',
                                                width: '100%',
                                                height: 200,
                                                marginRight: 1,
                                                borderTopLeftRadius: 20,
                                                borderTopRightRadius: 20
                                            }}
                                            source={{ uri: 'https://sistema.entremares.tur.br/arquivos/' + (this.state[item.item.Titulo] ? String(item.item.Imagem).split('|')[this.state[item.item.Titulo]] : String(item.item.Imagem).split('|')[0]) }} />
                                    </TouchableOpacity>

                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            alignSelf: 'center',
                                            width: '90%',
                                            position: 'absolute',
                                            marginTop: 100
                                        }}>
                                        <TouchableOpacity
                                            onPress={() => { this.setState({ [item.item.Titulo]: (!this.state[item.item.Titulo] ? 0 : this.state[item.item.Titulo] ? 0 : (this.state[item.item.Titulo] - 1)) }) }}
                                            style={{
                                                width: 30,
                                                height: 30,
                                                borderRadius: 30,
                                                justifyContent: 'center',
                                                alignSelf: 'center',
                                                backgroundColor: 'silver'
                                            }}>
                                            <Icon name="left" color='#001e46' style={{ padding: 5 }} size={18} />
                                        </TouchableOpacity>

                                        <TouchableOpacity
                                            onPress={() => { this.setState({ [item.item.Titulo]: !this.state[item.item.Titulo] ? 1 : this.state[item.item.Titulo] == String(item.item.Imagem).split('|').length ? String(item.item.Imagem).split('|').length : (this.state[item.item.Titulo] + 1) }) }}
                                            style={{
                                                width: 30,
                                                height: 30,
                                                borderRadius: 30,
                                                justifyContent: 'center',
                                                alignSelf: 'center',
                                                backgroundColor: 'silver'
                                            }}>
                                            <Icon name="right" color='#001e46' style={{ padding: 5 }} size={18} />
                                        </TouchableOpacity>
                                    </View>

                                    <View style={{
                                        height: 15,
                                        width: 130,
                                        backgroundColor: item.item.CategoriaDescricao == 'Ouro' ? '#ffec00' :
                                            item.item.CategoriaDescricao == 'Prata' ? '#dfe6e9' :
                                                item.item.CategoriaDescricao == 'Bronze' ? '#ffbe76' : 'white',
                                        marginLeft: 15,
                                        marginTop: -8
                                    }} />

                                    <View style={{
                                        flexDirection: 'row',
                                        alignItems: 'flex-end',
                                        justifyContent: 'space-between',
                                        marginHorizontal: 15,
                                        borderBottomWidth: 0.7,
                                        paddingVertical: 15,
                                        borderColor: 'white'
                                    }}>
                                        <Text style={{
                                            color: item.item.CategoriaDescricao == 'Ouro' ? '#ffec00' :
                                                item.item.CategoriaDescricao == 'Prata' ? '#dfe6e9' :
                                                    item.item.CategoriaDescricao == 'Bronze' ? '#ffbe76' : 'white',
                                            fontSize: 14,
                                            width: 130,
                                            fontWeight: 'bold',
                                        }}>{String(item.item.Cidade).toUpperCase()} - {String(item.item.UF).toUpperCase()}</Text>

                                        <View>
                                            <Text style={{ color: 'white', fontSize: 10, marginRight: 10 }}>Acomodações</Text>
                                            <Text style={{
                                                color: item.item.CategoriaDescricao == 'Ouro' ? '#ffec00' :
                                                    item.item.CategoriaDescricao == 'Prata' ? '#dfe6e9' :
                                                        item.item.CategoriaDescricao == 'Bronze' ? '#ffbe76' : 'white',
                                                fontSize: 14,
                                                fontWeight: 'bold',
                                                marginRight: 10,
                                                marginTop: -5,
                                                alignSelf: 'flex-end'
                                            }}>{String(item.item.CategoriaDescricao).toUpperCase()}</Text>
                                        </View>
                                    </View>

                                    <View style={{
                                        marginHorizontal: 15,
                                        borderBottomWidth: 0.7,
                                        paddingVertical: 15,
                                        borderColor: 'white'
                                    }}>
                                        <Text style={{
                                            color: item.item.CategoriaDescricao == 'Ouro' ? '#ffec00' :
                                                item.item.CategoriaDescricao == 'Prata' ? '#dfe6e9' :
                                                    item.item.CategoriaDescricao == 'Bronze' ? '#ffbe76' : 'white',
                                            fontSize: 14,
                                            fontWeight: 'bold',
                                        }}>{String(item.item.Titulo).toUpperCase()}</Text>
                                        <Text style={{
                                            color: 'white',
                                            fontSize: 10,
                                        }}>{item.item.Descricao}</Text>
                                    </View>

                                    <View style={{
                                        marginHorizontal: 15,
                                        borderBottomWidth: 0.7,
                                        paddingVertical: 15,
                                        borderColor: 'white'
                                    }}>
                                        <Text style={{
                                            color: item.item.CategoriaDescricao == 'Ouro' ? '#ffec00' :
                                                item.item.CategoriaDescricao == 'Prata' ? '#dfe6e9' :
                                                    item.item.CategoriaDescricao == 'Bronze' ? '#ffbe76' : 'white',
                                            fontSize: 14,
                                            fontWeight: 'bold',
                                        }}>LOCALIZAÇÃO</Text>
                                        {/* <Text style={{
                                            color: 'white',
                                            fontSize: 10,
                                        }}>{String(item.item.Endereco + ', ' + item.item.Numero + ', ' + item.item.Bairro).toUpperCase()} | {String(item.item.Cidade).toUpperCase()} - {String(item.item.UF).toUpperCase()}</Text>
                                    */}
                                    </View>

                                    <View style={{ flexDirection: 'row', marginHorizontal: 15, marginTop: 5, marginBottom: 20 }}>
                                        <View style={{
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            borderRadius: 5,
                                            padding: 2,
                                            backgroundColor: item.item.CategoriaDescricao == 'Ouro' ? '#ffec00' :
                                                item.item.CategoriaDescricao == 'Prata' ? '#dfe6e9' :
                                                    item.item.CategoriaDescricao == 'Bronze' ? '#ffbe76' : 'white',
                                        }}>
                                            <View>
                                                <IconF name="shower" color='#001e46' style={{ padding: 5 }} size={20} />
                                                <Text style={{ color: '#001e46', fontSize: 8 }}>BANHEIRO</Text>
                                            </View>
                                            <Text style={{ color: '#001e46', fontSize: 12, marginLeft: -4 }}>{item.item.Banheiros}</Text>
                                        </View>

                                        <View style={{
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            borderRadius: 5,
                                            marginLeft: 5,
                                            padding: 2,
                                            backgroundColor: item.item.CategoriaDescricao == 'Ouro' ? '#ffec00' :
                                                item.item.CategoriaDescricao == 'Prata' ? '#dfe6e9' :
                                                    item.item.CategoriaDescricao == 'Bronze' ? '#ffbe76' : 'white',
                                        }}>
                                            <View>
                                                <IconE name="ios-bed" color='#001e46' style={{ padding: 5 }} size={20} />
                                                <Text style={{ color: '#001e46', fontSize: 8, width: 40 }}>CASAL</Text>
                                            </View>
                                            <Text style={{ color: '#001e46', fontSize: 12, marginLeft: -4 }}>{item.item.CamasCasal}</Text>
                                        </View>

                                        <View style={{
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            borderRadius: 5,
                                            marginLeft: 5,
                                            padding: 2,
                                            backgroundColor: item.item.CategoriaDescricao == 'Ouro' ? '#ffec00' :
                                                item.item.CategoriaDescricao == 'Prata' ? '#dfe6e9' :
                                                    item.item.CategoriaDescricao == 'Bronze' ? '#ffbe76' : 'white',
                                        }}>
                                            <View>
                                                <IconE name="md-bed" color='#001e46' style={{ padding: 5 }} size={20} />
                                                <Text style={{ color: '#001e46', fontSize: 8, width: 40 }}>SOLTEIRO</Text>
                                            </View>
                                            <Text style={{ color: '#001e46', fontSize: 12, marginLeft: -4 }}>{item.item.CamasSolteiro}</Text>
                                        </View>

                                        <View style={{
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            borderRadius: 5,
                                            marginLeft: 5,
                                            padding: 2,
                                            backgroundColor: item.item.CategoriaDescricao == 'Ouro' ? '#ffec00' :
                                                item.item.CategoriaDescricao == 'Prata' ? '#dfe6e9' :
                                                    item.item.CategoriaDescricao == 'Bronze' ? '#ffbe76' : 'white',
                                        }}>
                                            <View>
                                                <IconF name="car" color='#001e46' style={{ padding: 5 }} size={20} />
                                                <Text style={{ color: '#001e46', fontSize: 8, width: 40 }}>GARAGEM</Text>
                                            </View>
                                            <Text style={{ color: '#001e46', fontSize: 12, marginLeft: -4 }}>{item.item.Garagem}</Text>
                                        </View>
                                    </View>

                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            marginRight: 15
                                        }}>
                                        <View />

                                        {(this.state.idUsuario !== '') ? null :
                                            <TouchableOpacity onPress={() => { this.select(item.item.Titulo, ('https://sistema.entremares.tur.br/arquivos/' + (String(item.item.Imagem).split('|')[0]))) }} style={{
                                                paddingHorizontal: 10,
                                                paddingVertical: 5,
                                                marginLeft: 29,
                                                marginVertical: 20,
                                                borderRadius: 30,
                                                backgroundColor: item.item.CategoriaDescricao == 'Ouro' ? '#ffec00' :
                                                    item.item.CategoriaDescricao == 'Prata' ? '#dfe6e9' :
                                                        item.item.CategoriaDescricao == 'Bronze' ? '#ffbe76' : 'white',
                                            }}>
                                                <Text style={{
                                                    color: '#001e46',
                                                    fontSize: 10,
                                                    textAlign: 'center'
                                                }}>Reservar</Text>
                                            </TouchableOpacity>}
                                    </View>

                                </View>
                            )}
                        />
                    }

                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginHorizontal: 30
                    }}>
                        <View />

                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('MenuDestino1') }} style={{
                            paddingHorizontal: 30,
                            justifyContent: 'center',
                            paddingVertical: 5,
                            marginRight: 15,
                            marginVertical: 20,
                            borderRadius: 30,
                            borderWidth: 0.8,
                            borderColor: '#ffec00',
                            backgroundColor: '#001e46'
                        }}>
                            <Text style={{
                                color: '#ffec00',
                                fontSize: 12,
                                textAlign: 'center'
                            }}>Voltar</Text>
                        </TouchableOpacity>
                    </View>

                    {/*<Text style={{
                        color: '#ffec00',
                        fontSize: 16,
                        fontWeight: 'bold',
                        textAlign: 'center',
                        marginTop: 20
                    }}>ACOMODAÇÕES SIMILARES</Text>*/}

                    {/*<GridView
                        itemDimension={212}
                        items={this.state.populares}
                        style={styles.gridView}
                        renderItem={item => (
                            <View
                                style={{
                                    height: 80,
                                    marginHorizontal: 30,
                                    marginBottom: 10,
                                    borderWidth: 0.7,
                                    borderColor: '#ffec00',
                                    borderRadius: 20,
                                    flexDirection: 'row',
                                    backgroundColor: '#001e46',
                                    elevation: 4,
                                }}>
                                <Image
                                    style={{
                                        resizeMode: 'cover',
                                        width: '40%',
                                        height: '100%',
                                        marginRight: 1,
                                        borderBottomLeftRadius: 20,
                                        borderTopLeftRadius: 20
                                    }}
                                    source={require('../img/praia1.jpg')} />

                                <View style={{
                                    height: 50,
                                    width: 15,
                                    backgroundColor: '#ffec00',
                                    marginLeft: -8,
                                    marginTop: 15
                                }} />

                                <View style={{ justifyContent: 'center' }}>
                                    <Text style={{
                                        color: '#ffec00',
                                        fontSize: 8,
                                        fontWeight: 'bold',
                                        marginLeft: 10,
                                        marginTop: 10
                                    }}>{item.item.Cidade.toUpperCase()}</Text>

                                    <View style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                        marginHorizontal: 10
                                    }}>
                                        <Text style={{
                                            color: 'white',
                                            fontSize: 8,
                                        }}>{item.item.Cidade.toUpperCase()}</Text>

                                        <Text style={{
                                            color: '#ffec00',
                                            fontSize: 12,
                                            marginLeft: 5,
                                            fontWeight: 'bold',
                                        }}>{String('5.0 /').toUpperCase()}<Text style={{ fontSize: 10, color: '#ffec00', fontWeight: '100' }}>5.0</Text></Text>
                                    </View>
                                </View>

                            </View>
                        )}
                    />*/}
                </ScrollView>

                <View style={{ flexDirection: 'row', height: Platform.OS === 'android' ? 55 : 75, backgroundColor: '#ffec00', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('MenuDestino1') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rdestino.png')}
                            style={{
                                height: 50,
                                width: 50,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        if (this.state.idUsuario == '0') {
                            Alert.alert(
                                'Confirmação',
                                'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                                [
                                    { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                                    { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                                ]
                            );
                        } else {
                            this.props.navigation.navigate('Inicio')
                        }
                    }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rreservas.png')}
                            style={{
                                height: 50,
                                width: 50,
                                marginBottom: 6,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rpontos.png')}
                            style={{
                                height: 50,
                                width: 50,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        if (this.state.idUsuario == '0') {
                            Alert.alert(
                                'Confirmação',
                                'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                                [
                                    { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                                    { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                                ]
                            );
                        } else {
                            this.props.navigation.navigate('MinhaConta')
                        }
                    }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rconta.png')}
                            style={{
                                height: 50,
                                width: 50,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>

                </View>

            </View>
        );
    }
}

export default acomodacoes;
const styles = StyleSheet.create({
    itemContainerProfissa: {
        elevation: 4,
        height: 100,
        marginBottom: 5,
    },
});