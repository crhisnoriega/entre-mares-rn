import React, { Component } from "react";
import {
    ScrollView,
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
    StatusBar,
    Modal,
    Platform,
    Alert,
    ToastAndroid,
    ActivityIndicator,
    DatePickerAndroid,
    Linking,
    BackHandler,
    RefreshControl
} from "react-native";
import { checkInternetConnection } from 'react-native-offline';
import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/EvilIcons';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import API, { caminhoAPI, versaoAPI } from '../utils/caminhosAPI'

class reservaSelect1 extends React.Component {
    state = {
        cidade: '',
        Y: ((2 / 2) - (85 / 2)),
        X: (((40 / 2) - (60 / 2)) * -1),
        local: '',
        tipo: '',
        ava: '',
        dataIni: '',
        dataFim: '',
        diaFim: '',
        dia: '',
        mesFim: '',
        mes: '',
        valor: '',
        adultos: '',
        idCidade: 0,
        qtdAdulto: 0,
        qtdCrianca: 0,
        criancas: '',
        token: '',
        image: '',
        idUsuario: '',
        tipoU: ''
    }

     componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('MenuDestino1')
            return true;
        })

        const RNFS = require('react-native-fs');
        const path = RNFS.DocumentDirectoryPath + '/reservaCria.json';
        const pathU = RNFS.DocumentDirectoryPath + '/usuario.json';

        RNFS.readFile(pathU, 'utf8')
            .then((contents) => {
                if (!(contents == '[{}]')) {
                    this.setState({
                        idUsuario: String(JSON.parse(contents)[0].codigo),
                        tipoU: JSON.parse(contents)[0].tipo,
                        token: JSON.parse(contents)[0].token
                    })
                } else {
                    this.setState({ idUsuario: '0' })
                }
            })
            .catch((err) => {
                this.setState({ idUsuario: '0' })
                alert(err.message)
                console.log(err.message, err.code);
            });

        RNFS.readFile(path, 'utf8')
            .then((contents) => {
                console.warn(contents);
                if (!(contents == '[{}]')) {
                    this.setState({
                        tipo: JSON.parse(contents)[0].tipo,
                        cidade: JSON.parse(contents)[0].cidade,
                        image: JSON.parse(contents)[0].image,
                        local: JSON.parse(contents)[0].local ? JSON.parse(contents)[0].local : '',
                    })

                    const RNFS1 = require('react-native-fs');
                    const pro = RNFS.DocumentDirectoryPath + '/direto.json';
                    let local = String(JSON.parse(contents)[0].local).toLowerCase()
                    let cidade = String(JSON.parse(contents)[0].cidade).toLowerCase()

                     RNFS1.readFile(pro, 'utf8')
                        .then(async (contents) => {
                            if (!(contents == '[{}]')) {
                                const caminhoID = (Boolean(JSON.parse(contents)[0].direto) ? local : cidade)

                                const isConnected = await checkInternetConnection();

                                if (isConnected) {
                                    fetch(caminhoAPI() + '/consultarLocaisPelaCidade?Cidade=' + caminhoID, {
                                        headers: {
                                            Accept: 'application/json',
                                            'Content-Type': 'application/json',
                                            'Versao': versaoAPI
                                        },
                                    }
                                    ).then((response) => {
                                        const statusCode = response.status;
                                        let responseJson1 = [];

                                        if (statusCode == 200) {
                                            responseJson1 = response.json();
                                        }

                                        return Promise.all([responseJson1, statusCode]);
                                    })
                                        .then(([responseJson, statusCode]) => {
                                            if (statusCode == 200) {

                                                this.setState({
                                                    refresh: false,
                                                    idCidade: parseInt(responseJson[0].Codigo)
                                                })

                                            } else {
                                                this.setState({
                                                    vazio: true,
                                                    refresh: false
                                                })
                                            }
                                        })
                                        .catch((error) => {
                                            alert(JSON.stringify(error))
                                            this.setState({
                                                vazio: true,
                                                refresh: false
                                            })
                                        });

                                } else {
                                    alert('Sem conexão com a internet!')
                                    this.setState({
                                        vazio: true,
                                        refresh: false
                                    })
                                }
                            } else {
                                alert('falhou')
                            }
                        })
                        .catch((err) => {
                            alert(err.message)
                            console.log(err.message, err.code);
                        });

                } else {
                    this.setState({ tipo: '' })
                }
            })
            .catch((err) => {
                this.setState({ tipo: '' })
                console.log(err.message, err.code);
            });
    }

    select = async () => {
        if (!(this.state.idUsuario == '0')) {
            if (!(this.state.qtdAdulto > 0)) {
                alert('É necessário indicar quantidade de Adultos');
                return
            }

            if (!(String(this.state.dia).length > 0)) {
                alert('É necessário data de Inicio');
                return
            }

            if (!(String(this.state.diaFim).length > 0)) {
                alert('É necessário data do Fim');
                return
            }

            this.setState({
                refresh: true
            })
            const isConnected = await checkInternetConnection();
            if (isConnected) {
                let dados = [];
                dados.push({
                    "Local": this.state.idCidade,
                    "DataInicio": this.state.dataIni,
                    "DataFim": this.state.dataFim,
                    "Adulto": String(this.state.qtdAdulto),
                    "Crianca": String(this.state.qtdCrianca)
                })

                fetch(caminhoAPI() + '/solicitar', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Versao': versaoAPI,
                        'Auth': 'Bearer ' + this.state.token
                        //'Cliente': this.state.idUsuario,
                        //'Tipo': this.state.tipoU
                    },
                    body: String(JSON.stringify(dados)).substr(1).substr(-(String(JSON.stringify(dados)).length - 1), (String(JSON.stringify(dados)).length - 2))
                }
                ).then((response) => {
                    const statusCode = response.status;
                    return Promise.all([statusCode]);
                })
                    .then(([statusCode]) => {
                        if (statusCode == 200) {
                            this.setState({
                                refresh: false
                            })
                            const RNFS = require('react-native-fs');
                            const pro = RNFS.DocumentDirectoryPath + '/reservaselect.json';

                            let dados = [];

                            dados.push({
                                "cidade": String(this.state.local).toUpperCase(),
                                "diaIni": (this.state.dia + ' DE ' + this.state.mes),
                                "diaFim": (this.state.diaFim + ' DE ' + this.state.mesFim),
                                "adultos": this.state.qtdAdulto,
                                "criancas": this.state.qtdCrianca,
                            })

                            RNFS.writeFile(pro, JSON.stringify(dados), 'utf8')
                                .then((success) => {
                                    console.log('FILE WRITTEN!');
                                })
                                .catch((err) => {
                                    console.log(err.message);
                                });


                            this.props.navigation.navigate('MessageAguardar')

                        } else {
                            alert('Estabelecimentos não foram encontrados')
                            this.setState({
                                refresh: false
                            })
                        }
                    })
                    .catch((error) => {
                        alert('Informações não são válidas');
                        this.setState({
                            refresh: false
                        })
                    });

            } else {
                alert('Sem conexão com a internet!')
                this.setState({
                    refresh: false
                })
            }
        } else {
            alert('É preciso fazer login para prosseguir ')
        }
    }

    render() {

        return (
            <View style={{ backgroundColor: '#001e46', height: '100%' }}>

                <StatusBar
                    barStyle="light-content"
                    backgroundColor='#001e46'
                />

                <TouchableOpacity onPress={() => {
                    try {
                        this.props.screenProps.myDrawerNavigation.toggleDrawer();
                    } catch (e) {
                        this.props.navigation.toggleDrawer();
                    }
                }} style={{ marginLeft: 20, marginTop: 40 }}>
                    <Iconr name="menu" color='#ffec00' style={{ padding: 5 }} size={40} />
                </TouchableOpacity>

                <ScrollView style={{ backgroundColor: '#001e46', height: '100%' }}>
                    <View style={{
                        flexDirection: 'row',
                        borderBottomWidth: 0.5,
                        paddingVertical: 10,
                        borderBottomColor: 'white',
                        marginTop: 20,
                        marginHorizontal: 29,
                    }}>
                        <Text
                            style={{
                                backgroundColor: 'transparent',
                                fontSize: 18,
                                color: 'white',
                                fontWeight: 'bold',
                            }}>SELECIONE UMA DATA</Text>
                    </View>

                    <View
                        style={{
                            flexDirection: 'row',
                            marginHorizontal: 29,
                            paddingVertical: 15,
                            alignItems: 'center'
                        }}>

                        <IconF name="calendar" color='#ffec00' size={25} />

                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity
                                onPress={async () => {
                                    try {
                                        const { action, year, month, day } = await DatePickerAndroid.open({
                                            date: new Date(),
                                        });
                                        if (action !== DatePickerAndroid.dismissedAction) {
                                            let mes = (parseInt(month) + 1)
                                            let mesn = ''
                                            mes = String(mes).length == 1 ? ("0" + mes) : mes
                                            const hoje = new Date()
                                            const selectDate = new Date(String(mes) + '/' + String(day) + '/' + String(year))
                                            mesn = mes

                                            mes = mes == '01' ? 'JANEIRO' :
                                                mes == '02' ? 'FEVEREIRO' :
                                                    mes == '03' ? 'MARÇO' :
                                                        mes == '04' ? 'ABRIL' :
                                                            mes == '05' ? 'MAIO' :
                                                                mes == '06' ? 'JUNHO' :
                                                                    mes == '07' ? 'JULHO' :
                                                                        mes == '08' ? 'AGOSTO' :
                                                                            mes == '09' ? 'SETEMBRO' :
                                                                                mes == '10' ? 'OUTUBRO' :
                                                                                    mes == '11' ? 'NOVEMBRO' :
                                                                                        mes == '12' ? 'DEZEMBRO' : null

                                            let dia = String(day).length == 1 ? ("0" + day) : day
                                            if ((selectDate > hoje) || (parseInt(String(String(year) + String(mesn) + String(dia))) == parseInt(String(String(hoje.getFullYear()) + (String(hoje.getMonth() + 1).length == 1 ? ('0' + String(hoje.getMonth() + 1)) : String(hoje.getMonth() + 1)) + (String(hoje.getDate()).length == 1 ? ('0' + String(hoje.getDate())) : String(hoje.getDate())))))) {
                                                if (this.state.diaFim != '') {
                                                    if (parseInt(String(String(year) + String(mesn) + String(dia))) > this.state.dataFim) {
                                                        alert('Data inicial superior a final')
                                                        this.setState({ dia: '', mes: '', dataIni: '' })
                                                        return
                                                    }
                                                }
                                                this.setState({ dia: dia, mes: mes, dataIni: parseInt(String(String(year) + String(mesn) + String(dia))) })
                                            } else {
                                                this.setState({ dia: '', mes: '', dataIni: '' })
                                                alert('Data menor que a data atual')
                                            }
                                        }
                                    } catch ({ code, message }) {
                                        console.warn('Cannot open date picker', message);
                                    }
                                }}>
                                <Text style={{ color: 'white', marginHorizontal: 10, fontSize: 12 }}>
                                    {this.state.dia == '' ? 'INICIO' : this.state.dia + ' DE ' + this.state.mes}
                                </Text>
                            </TouchableOpacity>

                            <Text style={{ color: 'white', marginHorizontal: 10 }}>   </Text>

                            <TouchableOpacity
                                onPress={async () => {
                                    try {
                                        const { action, year, month, day } = await DatePickerAndroid.open({
                                            date: new Date(),
                                        });
                                        if (action !== DatePickerAndroid.dismissedAction) {
                                            let mes = (parseInt(month) + 1)
                                            let mesn = ''
                                            mes = String(mes).length == 1 ? ("0" + mes) : mes
                                            const hoje = new Date()
                                            const selectDate = new Date(String(mes) + '/' + String(day) + '/' + String(year))
                                            mesn = mes

                                            mes = mes == '01' ? 'JANEIRO' :
                                                mes == '02' ? 'FEVEREIRO' :
                                                    mes == '03' ? 'MARÇO' :
                                                        mes == '04' ? 'ABRIL' :
                                                            mes == '05' ? 'MAIO' :
                                                                mes == '06' ? 'JUNHO' :
                                                                    mes == '07' ? 'JULHO' :
                                                                        mes == '08' ? 'AGOSTO' :
                                                                            mes == '09' ? 'SETEMBRO' :
                                                                                mes == '10' ? 'OUTUBRO' :
                                                                                    mes == '11' ? 'NOVEMBRO' :
                                                                                        mes == '12' ? 'DEZEMBRO' : null

                                            let dia = String(day).length == 1 ? ("0" + day) : day
                                            if ((selectDate > hoje) || (parseInt(String(String(year) + String(mesn) + String(dia))) == parseInt(String(String(hoje.getFullYear()) + (String(hoje.getMonth() + 1).length == 1 ? ('0' + String(hoje.getMonth() + 1)) : String(hoje.getMonth() + 1)) + (String(hoje.getDate()).length == 1 ? ('0' + String(hoje.getDate())) : String(hoje.getDate())))))) {
                                                if (this.state.dia != '') {
                                                    if (parseInt(String(String(year) + String(mesn) + String(dia))) < this.state.dataIni) {
                                                        alert('Data final menor que inicial')
                                                        this.setState({ diaFim: '', mesFim: '', dataFim: '' })
                                                        return
                                                    }
                                                }
                                                this.setState({ diaFim: dia, mesFim: mes, dataFim: parseInt(String(String(year) + String(mesn) + String(dia))) })
                                            } else {
                                                this.setState({ diaFim: '', mesFim: '', dataFim: '' })
                                                alert('Data menor que a data atual')
                                            }
                                        }
                                    } catch ({ code, message }) {
                                        console.warn('Cannot open date picker', message);
                                    }
                                }}>
                                <Text style={{ color: 'white', marginHorizontal: 10, fontSize: 12 }}>
                                    {this.state.diaFim == '' ? 'FIM' : this.state.diaFim + ' DE ' + this.state.mesFim}
                                </Text>
                            </TouchableOpacity>
                        </View>

                        <Icon name="downcircleo" color='#ffec00' size={10} />

                    </View>

                    <View style={{
                        borderBottomWidth: 0.5,
                        marginHorizontal: 29,
                        borderBottomColor: 'white'
                    }} />

                    <View
                        style={{
                            flexDirection: 'row',
                            marginHorizontal: 29,
                            paddingVertical: 10,
                            alignItems: 'center'
                        }}>
                        <Text style={{
                            color: 'white',
                            fontSize: 8,
                            marginRight: 10
                        }}>ADULTOS</Text>


                        <TouchableOpacity
                            style={{ padding: 5 }}
                            onPress={() => { this.setState({ qtdAdulto: this.state.qtdAdulto == 0 ? 0 : (this.state.qtdAdulto - 1) }) }}>
                            <Text style={{
                                color: 'white',
                                fontSize: 14,
                                marginRight: 10
                            }}>{'-'}</Text>
                        </TouchableOpacity>

                        <Text style={{
                            fontSize: 20,
                            color: '#ffec00',
                            marginRight: 10
                        }}>{this.state.qtdAdulto}</Text>

                        <TouchableOpacity
                            style={{ padding: 5 }}
                            onPress={() => { this.setState({ qtdAdulto: (this.state.qtdAdulto + 1) }) }}>
                            <Text style={{
                                color: 'white',
                                fontSize: 14,
                                marginRight: 40
                            }}>+</Text>
                        </TouchableOpacity>


                        <Text style={{
                            color: 'white',
                            fontSize: 8,
                            marginRight: 10
                        }}>CRIANÇAS ATÉ 4 ANOS</Text>


                        <TouchableOpacity
                            style={{ padding: 5 }}
                            onPress={() => { this.setState({ qtdCrianca: this.state.qtdCrianca == 0 ? 0 : (this.state.qtdCrianca - 1) }) }}>
                            <Text style={{
                                color: 'white',
                                fontSize: 16,
                                marginRight: 10
                            }}>{'-'}</Text>
                        </TouchableOpacity>

                        <Text style={{
                            fontSize: 20,
                            color: '#ffec00',
                            marginRight: 10
                        }}>{this.state.qtdCrianca}</Text>

                        <TouchableOpacity
                            style={{ padding: 5 }}
                            onPress={() => { this.setState({ qtdCrianca: (this.state.qtdCrianca + 1) }) }}>
                            <Text style={{ color: 'white', fontSize: 16 }}>+</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{
                        borderBottomWidth: 0.5,
                        marginHorizontal: 29,
                        borderBottomColor: 'white'
                    }} />

                    <View
                        style={{
                            height: 250,
                            marginHorizontal: 29,
                            borderWidth: 0.7,
                            borderColor: '#ffec00',
                            borderRadius: 20,
                            marginTop: 20,
                            backgroundColor: '#001e46',
                            elevation: 4,
                        }}>
                        <Image
                            style={{
                                resizeMode: 'cover',
                                width: '100%',
                                height: '65%',
                                borderTopLeftRadius: 20,
                                borderTopRightRadius: 20
                            }}
                            source={String(this.state.image) == 'undefined' ? require('../img/praia1.jpg') : { uri: this.state.image }} />

                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                position: 'absolute',
                                width: '100%'
                            }}>
                            <View />
                            <View style={{
                                height: '220%',
                                width: 35,
                                backgroundColor: '#ffec00',
                                marginTop: 36,
                                justifyContent: 'center'
                            }}>

                                <Text style={{
                                    color: '#001e46',
                                    fontSize: 12,
                                    fontWeight: 'bold',
                                    transform: [
                                        { rotate: '270deg' },
                                        { translateX: this.state.X },
                                        { translateY: this.state.Y }
                                    ],
                                    width: 120
                                }}></Text>
                            </View>
                        </View>

                        <View style={{
                            height: 15,
                            width: 130,
                            backgroundColor: '#ffec00',
                            marginLeft: 15,
                            marginTop: -8
                        }} />

                        <Text style={{
                            color: '#ffec00',
                            fontSize: 14,
                            fontWeight: 'bold',
                            marginLeft: 15,
                            marginTop: 10
                        }}>{this.state.local.toUpperCase()}</Text>

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{
                                color: 'white',
                                fontSize: 10,
                                marginLeft: 15,
                                marginTop: 2
                            }}>{this.state.cidade.toUpperCase()}</Text>
                        </View>

                        {/*<View style={{ flexDirection: 'row', height: 50 }}>
                            <Text style={{
                                color: 'white',
                                fontSize: 10,
                                marginLeft: 15,
                                marginTop: 2
                            }}>R$</Text>
                            <Text style={{
                                color: '#ffec00',
                                fontSize: 25,
                                fontWeight: 'bold',
                                marginLeft: 2,
                                marginTop: -4
                            }}>35</Text>
                            <View>
                                <Text style={{
                                    color: '#ffec00',
                                    fontSize: 12,
                                    fontWeight: 'bold',
                                    marginLeft: 2,
                                    marginTop: -2
                                }}>,00</Text>
                                <Text style={{
                                    color: 'white',
                                    fontSize: 8,
                                    marginLeft: 5
                                }}>NOITE</Text>
                            </View>
                        </View>
                    */}
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View />
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity onPress={() => { this.props.navigation.navigate('MenuDestino1') }} style={{
                                height: 30,
                                paddingHorizontal: 30,
                                justifyContent: 'center',
                                paddingVertical: 5,
                                marginLeft: 29,
                                marginTop: 15,
                                marginBottom: 25,
                                borderRadius: 30,
                                borderWidth: 0.8,
                                borderColor: '#ffec00',
                                backgroundColor: '#001e46'
                            }}>
                                <Text style={{
                                    color: '#ffec00',
                                    fontSize: 10,
                                    textAlign: 'center'
                                }}>Voltar</Text>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.select()} style={{
                                height: 30,
                                paddingHorizontal: 15,
                                justifyContent: 'center',
                                paddingVertical: 5,
                                marginLeft: 15,
                                marginTop: 15,
                                marginRight: 29,
                                marginBottom: 25,
                                borderRadius: 30,
                                borderWidth: 0.8,
                                borderColor: '#ffec00',
                                backgroundColor: '#ffec00'
                            }}>
                                {this.state.refresh ? <ActivityIndicator size='small' color={'#001e46'} /> :
                                    <Text style={{
                                        color: '#001e46',
                                        fontSize: 10,
                                        textAlign: 'center'
                                    }}>Confirmar</Text>}
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View />
                </ScrollView>

                <View style={{ flexDirection: 'row', height: Platform.OS === 'android' ? 55 : 75, backgroundColor: '#ffec00', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('MenuDestino1') }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rdestino.png')}
                            style={{
                                height: 50,
                                width: 50,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        if (this.state.idUsuario == '0') {
                            Alert.alert(
                                'Confirmação',
                                'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                                [
                                    { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                                    { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                                ]
                            );
                        } else {
                            this.props.navigation.navigate('Inicio')
                        }
                    }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rreservas.png')}
                            style={{
                                height: 50,
                                width: 50,
                                marginBottom: 6,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rpontos.png')}
                            style={{
                                height: 50,
                                width: 50,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        if (this.state.idUsuario == '0') {
                            Alert.alert(
                                'Confirmação',
                                'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                                [
                                    { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                                    { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                                ]
                            );
                        } else {
                            this.props.navigation.navigate('MinhaConta')
                        }
                    }} style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                        <Image
                            source={require('../img/rconta.png')}
                            style={{
                                height: 50,
                                width: 50,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                            }} />
                    </TouchableOpacity>

                </View>

            </View>
        );
    }
}


export default reservaSelect1;

