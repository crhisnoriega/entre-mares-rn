import React, { Component } from "react";
import {
    ScrollView,
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
    StatusBar,
    Modal,
    Platform,
    ToastAndroid,
    NetInfo,
    ActivityIndicator,
    DatePickerAndroid,
    Linking,
    RefreshControl
} from "react-native";
import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/EvilIcons';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';

class reservaSelect extends React.Component {
    state = {
        cidade: '',
        local: '',
        tipo: '',
        ava: '',
        diaFim: '',
        dia: '',
        mesFim: '',
        mes: '',
        valor: '',
        adultos: '',
        qtdAdulto: 0,
        qtdCrianca: 0,
        criancas: ''
    }

    componentDidMount() {
        var RNFS = require('react-native-fs');
        var path = RNFS.DocumentDirectoryPath + '/reservaCria.json';

        RNFS.readFile(path, 'utf8')
            .then((contents) => {
                console.warn(contents);
                if (!(contents == '[{}]')) {
                    this.setState({
                        tipo: JSON.parse(contents)[0].tipo,
                        local: JSON.parse(contents)[0].local,
                    })
                } else {
                    this.setState({ tipo: '' })
                }
            })
            .catch((err) => {
                this.setState({ tipo: '' })
                console.log(err.message, err.code);
            });
    }

    select = () => {
        const RNFS = require('react-native-fs');
        const pro = RNFS.DocumentDirectoryPath + '/reservaCria.json';

        let dados = [];

        dados.push({
            "tipo": this.state.tipo,
            "local": this.state.local,
            "adultos": String(this.state.qtdAdulto),
            "criancas": String(this.state.qtdCrianca),
            "dia": this.state.dia + " DE " + this.state.mes,
            "diaFim": this.state.diaFim + " DE " + this.state.mesFim,
        })

        RNFS.writeFile(pro, JSON.stringify(dados), 'utf8')
            .then((success) => {
                console.log('FILE WRITTEN!');
            })
            .catch((err) => {
                console.log(err.message);
            });


        this.props.navigation.navigate('MenuConfirma')
    }

    render() {

        return (
            <View style={{ backgroundColor: '#001e46', height: '100%' }}>

                <StatusBar
                    barStyle="light-content"
                    backgroundColor='#001e46'
                />

                <TouchableOpacity onPress={() => {
                    try {
                        this.props.screenProps.myDrawerNavigation.toggleDrawer();
                    } catch (e) {
                        this.props.navigation.toggleDrawer();
                    }
                }} style={{ marginLeft: 20, marginTop: 40 }}>
                    <Iconr name="menu" color='#ffec00' style={{ padding: 5 }} size={40} />
                </TouchableOpacity>

                <ScrollView style={{ backgroundColor: '#001e46', height: '100%' }}>
                    <View style={{
                        flexDirection: 'row',
                        borderBottomWidth: 0.5,
                        paddingVertical: 10,
                        borderBottomColor: 'white',
                        marginTop: 20,
                        marginHorizontal: 29,
                    }}>
                        <Text
                            style={{
                                backgroundColor: 'transparent',
                                fontSize: 18,
                                color: 'white',
                                fontWeight: 'bold',
                            }}>SELECIONE UMA DATA</Text>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        marginHorizontal: 29,
                        paddingVertical: 15,
                        alignItems: 'center'
                    }}>

                        <IconF name="calendar" color='#ffec00' size={25} />

                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity
                                onPress={async () => {
                                    try {
                                        const { action, year, month, day } = await DatePickerAndroid.open({
                                            date: new Date(),
                                        });
                                        if (action !== DatePickerAndroid.dismissedAction) {
                                            let mes = (parseInt(month) + 1)
                                            mes = String(mes).length == 1 ? ("0" + mes) : mes

                                            mes = mes == '01' ? 'JANEIRO' :
                                                mes == '02' ? 'FEVEREIRO' :
                                                    mes == '03' ? 'MARÇO' :
                                                        mes == '04' ? 'ABRIL' :
                                                            mes == '05' ? 'MAIO' :
                                                                mes == '06' ? 'JUNHO' :
                                                                    mes == '07' ? 'JULHO' :
                                                                        mes == '08' ? 'AGOSTO' :
                                                                            mes == '09' ? 'SETEMBRO' :
                                                                                mes == '10' ? 'OUTUBRO' :
                                                                                    mes == '11' ? 'NOVEMBRO' :
                                                                                        mes == '12' ? 'DEZEMBRO' : NULL

                                            let dia = String(day).length == 1 ? ("0" + day) : day
                                            this.setState({ dia: dia, mes: mes })
                                        }
                                    } catch ({ code, message }) {
                                        console.warn('Cannot open date picker', message);
                                    }
                                }}>
                                <Text style={{ color: 'white', marginHorizontal: 10, fontSize: 12 }}>
                                    {this.state.dia == '' ? 'INICIO' : this.state.dia + ' DE ' + this.state.mes}
                                </Text>
                            </TouchableOpacity>

                            <Text style={{ color: 'white', marginHorizontal: 10 }}> {"-"} </Text>

                            <TouchableOpacity
                                onPress={async () => {
                                    try {
                                        const { action, year, month, day } = await DatePickerAndroid.open({
                                            date: new Date(),
                                        });
                                        if (action !== DatePickerAndroid.dismissedAction) {
                                            let mes = (parseInt(month) + 1)
                                            mes = String(mes).length == 1 ? ("0" + mes) : mes

                                            mes = mes == '01' ? 'JANEIRO' :
                                                mes == '02' ? 'FEVEREIRO' :
                                                    mes == '03' ? 'MARÇO' :
                                                        mes == '04' ? 'ABRIL' :
                                                            mes == '05' ? 'MAIO' :
                                                                mes == '06' ? 'JUNHO' :
                                                                    mes == '07' ? 'JULHO' :
                                                                        mes == '08' ? 'AGOSTO' :
                                                                            mes == '09' ? 'SETEMBRO' :
                                                                                mes == '10' ? 'OUTUBRO' :
                                                                                    mes == '11' ? 'NOVEMBRO' :
                                                                                        mes == '12' ? 'DEZEMBRO' : NULL

                                            let dia = String(day).length == 1 ? ("0" + day) : day
                                            this.setState({ diaFim: dia, mesFim: mes })
                                        }
                                    } catch ({ code, message }) {
                                        console.warn('Cannot open date picker', message);
                                    }
                                }}>
                                <Text style={{ color: 'white', marginHorizontal: 10, fontSize: 12 }}>
                                    {this.state.diaFim == '' ? 'FIM' : this.state.diaFim + ' DE ' + this.state.mesFim}
                                </Text>
                            </TouchableOpacity>
                        </View>

                        <Icon name="downcircleo" color='#ffec00' size={10} />

                    </View>

                    <View style={{
                        borderBottomWidth: 0.5,
                        marginHorizontal: 29,
                        borderBottomColor: 'white'
                    }} />

                    <View style={{
                        flexDirection: 'row',
                        marginHorizontal: 29,
                        paddingVertical: 10,
                        alignItems: 'center'
                    }}>
                        <Text style={{
                            color: 'white',
                            fontSize: 8,
                            marginRight: 10
                        }}>ADULTOS</Text>

                        <TouchableOpacity
                            style={{ padding: 5 }}
                            onPress={() => { this.setState({ qtdAdulto: (this.state.qtdAdulto - 1) }) }}>
                            <Text style={{
                                color: 'white',
                                fontSize: 14,
                                marginRight: 10
                            }}>{""}</Text>
                        </TouchableOpacity>

                        <Text style={{
                            fontSize: 20,
                            color: '#ffec00',
                            marginRight: 10
                        }}>{this.state.qtdAdulto}</Text>

                        <TouchableOpacity
                            style={{ padding: 5 }}
                            onPress={() => { this.setState({ qtdAdulto: (this.state.qtdAdulto + 1) }) }}>
                            <Text style={{
                                color: 'white',
                                fontSize: 14,
                                marginRight: 40
                            }}>+</Text>
                        </TouchableOpacity>


                        <Text style={{
                            color: 'white',
                            fontSize: 8,
                            marginRight: 10
                        }}>CRIANÇAS ATÉ 4 ANOS</Text>

                        <TouchableOpacity
                            style={{ padding: 5 }}
                            onPress={() => { this.setState({ qtdCrianca: (this.state.qtdCrianca - 1) }) }}>
                            <Text style={{
                                color: 'white',
                                fontSize: 16,
                                marginRight: 10
                            }}>{"-"}</Text>
                        </TouchableOpacity>

                        <Text style={{
                            fontSize: 20,
                            color: '#ffec00',
                            marginRight: 10
                        }}>{this.state.qtdCrianca}</Text>

                        <TouchableOpacity
                            style={{ padding: 5 }}
                            onPress={() => { this.setState({ qtdCrianca: (this.state.qtdCrianca + 1) }) }}>
                            <Text style={{ color: 'white', fontSize: 16 }}>+</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{
                        borderBottomWidth: 0.5,
                        marginHorizontal: 29,
                        borderBottomColor: 'white'
                    }} />

                    <View
                        style={{
                            height: 250,
                            marginHorizontal: 29,
                            borderWidth: 0.7,
                            borderColor: '#ffec00',
                            borderRadius: 20,
                            marginTop: 20,
                            backgroundColor: '#001e46',
                            elevation: 4,
                        }}>
                        <Image
                            style={{
                                resizeMode: 'cover',
                                width: '100%',
                                height: '65%',
                                borderTopLeftRadius: 20,
                                borderTopRightRadius: 20
                            }}
                            source={require('../img/praia1.jpg')} />

                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            position: 'absolute',
                            width: '100%'
                        }}>
                            <View />
                            <View style={{
                                height: '220%',
                                width: 35,
                                backgroundColor: '#ffec00',
                                marginTop: 36,
                                justifyContent: 'center'
                            }}>
                                <Text style={{
                                    color: '#001e46',
                                    fontSize: 12,
                                    fontWeight: 'bold',
                                    transform: [
                                        { rotate: '270deg' },
                                        { translateX: -((40 / 2) - (60 / 2)) },
                                        { translateY: ((2 / 2) - (85 / 2)) }
                                    ],
                                    width: 120
                                }}>MAIS RESERVADA</Text>
                            </View>
                        </View>

                        <View style={{
                            height: 15,
                            width: 130,
                            backgroundColor: '#ffec00',
                            marginLeft: 15,
                            marginTop: -8
                        }} />

                        <Text style={{
                            color: '#ffec00',
                            fontSize: 14,
                            fontWeight: 'bold',
                            marginLeft: 15,
                            marginTop: 10
                        }}>{this.state.cidade.toUpperCase()}</Text>

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{
                                color: 'white',
                                fontSize: 10,
                                marginLeft: 15,
                                marginTop: 2
                            }}>{this.state.local.toUpperCase()}</Text>
                        </View>

                        <View style={{ flexDirection: 'row', height: 50 }}>
                            <Text style={{
                                color: 'white',
                                fontSize: 10,
                                marginLeft: 15,
                                marginTop: 2
                            }}>R$</Text>
                            <Text style={{
                                color: '#ffec00',
                                fontSize: 25,
                                fontWeight: 'bold',
                                marginLeft: 2,
                                marginTop: -4
                            }}>35</Text>
                            <View>
                                <Text style={{
                                    color: '#ffec00',
                                    fontSize: 12,
                                    fontWeight: 'bold',
                                    marginLeft: 2,
                                    marginTop: -2
                                }}>,00</Text>
                                <Text style={{
                                    color: 'white',
                                    fontSize: 8,
                                    marginLeft: 5
                                }}>NOITE</Text>
                            </View>
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View />
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity onPress={() => { this.props.navigation.navigate('acomodacoes') }} style={{
                                height: 30,
                                paddingHorizontal: 30,
                                justifyContent: 'center',
                                paddingVertical: 5,
                                marginLeft: 29,
                                marginTop: 15,
                                marginBottom: 25,
                                borderRadius: 30,
                                borderWidth: 0.8,
                                borderColor: '#ffec00',
                                backgroundColor: '#001e46'
                            }}>
                                <Text style={{
                                    color: '#ffec00',
                                    fontSize: 10,
                                    textAlign: 'center'
                                }}>Voltar</Text>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.select()} style={{
                                height: 30,
                                paddingHorizontal: 15,
                                justifyContent: 'center',
                                paddingVertical: 5,
                                marginLeft: 15,
                                marginTop: 15,
                                marginRight: 29,
                                marginBottom: 25,
                                borderRadius: 30,
                                borderWidth: 0.8,
                                borderColor: '#ffec00',
                                backgroundColor: '#ffec00'
                            }}>
                                <Text style={{
                                    color: '#001e46',
                                    fontSize: 10,
                                    textAlign: 'center'
                                }}>Confirmar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View />
                </ScrollView>
            </View>
        );
    }
}


export default reservaSelect;