import Login1 from './login/Login1';
import Inicio from './inicio/Inicio';
import MenuP from './inicio/MenuP';
import Politica from './inicio/Politica';
import Concluido from './inicio/Concluido';
import MinhasReservas from './inicio/MinhasReservas';
import MinhasReservasR from './inicio/MinhasReservasR';
import MinhasSolicitacao from './inicio/MinhasSolicitacao';
import MenuDestino1 from './inicio/MenuDestino1';
import MenuDestinoResult from './inicio/MenuDestinoResult';
import MenuDestinoAcomoda from './inicio/MenuDestinoAcomoda';
import MenuConfirma from './inicio/MenuConfirma';
import Avaliacao from './inicio/Avaliacao';
import reservaSelect1 from './profissionais/reservaSelect1'
import acomodacoes from './profissionais/acomodacoes'
import acomodacoesReserva from './profissionais/acomodacoesReserva'
import TrocaSenha from './inicio/TrocaSenha';
import MinhaConta from './inicio/MinhaConta';
import MinhaCarterinha from './inicio/MinhaCarterinha'
import MessageAguardar from './inicio/MessageAguardar'
import covid from './bloqueio/covid'
import Icon from 'react-native-vector-icons/AntDesign';
import React, { Component } from 'react';
import {
  ScrollView,
  View,
  Alert,
  Dimensions,
  TouchableOpacity,
  Linking
} from 'react-native';
import { TabBarBottom, createStackNavigator, navigation, createDrawerNavigator, DrawerItems } from 'react-navigation';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
const width = Dimensions.get("screen").width;
const widthD = (width * 80) / 100;



class Hidden extends React.Component {
  render() {
    return null;
  }
}

const CustomDrawerContentComponent = (props) => (
  <ScrollView style={{ marginTop: 23 }}>
    <View>
      <TouchableOpacity onPress={() => { }}
        style={{ marginLeft: 20, marginTop: 17 }}>
        <Iconr name="menu" color='#ffec00' style={{ padding: 5 }} size={40} />
      </TouchableOpacity>
    </View>

    <DrawerItems
      labelStyle={{
        fontSize: 18,
        color: '#001e46',
        marginLeft: 0,
        fontWeight: 'bold',
      }}
      
      onItemPress={(route) => {
        if (route.route.routeName == routes.MinhaConta) {
          var RNFS = require('react-native-fs');
          var path = RNFS.DocumentDirectoryPath + '/usuario.json';

          RNFS.readFile(path, 'utf8')
            .then((contents) => {
              if (!(contents == '[{}]')) {
                props.onItemPress(route);
                return;
              } else {                
                alert('É preciso fazer login para prosseguir ')
                return;
              }
            })
            .catch((err) => {              
              alert('É preciso fazer login para prosseguir ')
              return;
            });
        }

        if (route.route.routeName == routes.Inicio) {
          var RNFS = require('react-native-fs');
          var path = RNFS.DocumentDirectoryPath + '/usuario.json';

          RNFS.readFile(path, 'utf8')
            .then((contents) => {
              if (!(contents == '[{}]')) {
                props.onItemPress(route);
                return;
              } else {
                alert('É preciso fazer login para prosseguir ')
                return;
              }
            })
            .catch((err) => {
              alert('É preciso fazer login para prosseguir ')
              return;
            });
        }
        if ((route.route.routeName != routes.Inicio) & (route.route.routeName != routes.MinhaConta) ) {
          alert('Não é')
          props.onItemPress(route);
        }
        }
      }

      itemStyle={{
        marginHorizontal: 35,
        borderBottomWidth: props == null ? 0 : 0,
        borderBottomColor: '#001e46'
      }} {...props} />
  </ScrollView>
);

let Bloqueio = false

export const atBloqueio = sit => {
  Bloqueio = sit
}

const AppDrawer = createDrawerNavigator({

  Inicio: {
    screen: Inicio,
    navigationOptions: {
      drawerLabel: Bloqueio ? 'FAZER UMA RESERVA' : (() => null)
    }
  },

  MenuDestino1: {
    screen: MenuDestino1,
    navigationOptions: {
      drawerLabel: Bloqueio ? 'ESCOLHA SEU DESTINO' : (() => null)
    }
  },

  MinhaConta: {
    screen: MinhaConta,
    navigationOptions: {
      drawerLabel: 'MINHA CONTA'
    }
  },

  MinhasReservas: {
    screen: MinhasReservas,
    navigationOptions: {
      drawerLabel: 'MINHAS RESERVAS'
    }
  },

  MinhasSolicitacao: {
    screen: MinhasSolicitacao,
    navigationOptions: {
      drawerLabel: () => null,
    }
  },

  MinhaCarterinha: {
    screen: MinhaCarterinha,
    navigationOptions: {
      drawerLabel: 'MINHAS CARTEIRINHAS'
    }
  },

  MenuP1: {
    screen: Politica,
    navigationOptions: {
      drawerLabel: 'POLITICA PRIVACIDADE'
    }
  },

  MenuP: {
    screen: MenuP,
    navigationOptions: {
      drawerLabel: () => null,
    }
  },

  MessageAguardar: {
    screen: MessageAguardar,
    navigationOptions: {
      drawerLabel: () => null,
    }
  },

  covid: {
    screen: covid,
    navigationOptions: {
      drawerLabel: () => null,
    }
  },

  reservaSelect1: {
    screen: reservaSelect1,
    navigationOptions: {
      drawerLabel: () => null,
    }
  },

  acomodacoes: {
    screen: acomodacoes,
    navigationOptions: {
      drawerLabel: () => null,
    }
  },

  acomodacoesReserva: {
    screen: acomodacoesReserva,
    navigationOptions: {
      drawerLabel: () => null,
    }
  },

  MinhasReservasR: {
    screen: MinhasReservasR,
    navigationOptions: {
      drawerLabel: () => null,
    }
  },

  Login: {
    screen: Login1,
    navigationOptions: {
      drawerLabel: () => null,
    }
  },

  Concluido: {
    screen: Concluido,
    navigationOptions: {
      drawerLabel: () => null,
    }
  },

  MenuDestinoAcomoda: {
    screen: MenuDestinoAcomoda,
    navigationOptions: {
      drawerLabel: () => null,
    }
  },

  MenuConfirma: {
    screen: MenuConfirma,
    navigationOptions: {
      drawerLabel: () => null,
    }
  },

  MenuDestinoResult: {
    screen: MenuDestinoResult,
    navigationOptions: {
      drawerLabel: () => null,
    }
  },

  In: {
    screen: ({ navigation }) => <App navigationOptions={drawerLabel = () => null} screenProps={{
      myDrawerNavigation: navigation
    }} />,
    navigationOptions: {
      drawerLabel: () => null,
    }
  },

}, {
    initialRouteName: 'In',
    drawerLockMode: 'unlocked',
    drawerPosition: 'left',
    contentComponent: CustomDrawerContentComponent,
    drawerBackgroundColor: 'rgba(255,255,255,0.9)',
    drawerWidth: widthD
  });


const Inici = createStackNavigator({
  Ini: { screen: AppDrawer }
},
  {
    initialRouteName: 'Ini',
    headerMode: 'none',
  });

const App = createStackNavigator({
  Login: {
    screen: Login1,
    navigationOptions: {
      drawerLockMode: 'locked-closed',
    }
  },

  TrocaSenha: {
    screen: TrocaSenha,
    navigationOptions: {
      header: null
    }
  },

  MessageAguardar: {
    screen: MessageAguardar,
    navigationOptions: {
      header: null
    }
  },

  MinhasSolicitacao: {
    screen: MinhasSolicitacao,
    navigationOptions: {
      header: null
    }
  },

  MinhaConta: {
    screen: MinhaConta,
    navigationOptions: {
      header: null
    }
  },

  MinhaCarterinha: {
    screen: MinhaCarterinha,
    navigationOptions: {
      header: null
    }
  },

  reservaSelect1: {
    screen: reservaSelect1,
    navigationOptions: {
      header: null
    }
  },

  acomodacoes: {
    screen: acomodacoes,
    navigationOptions: {
      header: null
    }
  },

  acomodacoesReserva: {
    screen: acomodacoesReserva,
    navigationOptions: {
      header: null
    }
  },

  MinhasReservas: {
    screen: MinhasReservas,
    navigationOptions: {
      header: null
    }
  },

  MinhasReservasR: {
    screen: MinhasReservasR,
    navigationOptions: {
      header: null
    }
  },

  Concluido: {
    screen: Concluido,
    navigationOptions: {
      header: null
    }
  },

  MenuP: {
    screen: MenuP,
    navigationOptions: {
      header: null
    }
  },

  MenuDestino1: {
    screen: MenuDestino1,
    navigationOptions: {
      header: null
    }
  },

  MenuDestinoResult: {
    screen: MenuDestinoResult,
    navigationOptions: {
      header: null
    }
  },

  MenuDestinoAcomoda: {
    screen: MenuDestinoAcomoda,
    navigationOptions: {
      header: null
    }
  },

  MenuConfirma: {
    screen: MenuConfirma,
    navigationOptions: {
      header: null
    }
  },

  Inicio: {
    screen: Inicio,
    navigationOptions: {
      header: null
    }
  },

  covid: {
    screen: covid,
    navigationOptions: {
      header: null
    }
  },

  Avaliacao: {
    screen: Avaliacao,
    navigationOptions: {
      title: 'Dr Trabalho',
      headerTitleStyle: {
        textAlign: 'center',
        alignSelf: 'center',
        flex: 1
      },
      headerLeft: null,
      headerStyle: {
        backgroundColor: '#282827',
      },
      headerTintColor: 'white',
    }
  },

},
  {
    initialRouteName: 'Login',
    headerMode: 'none',
  });

export default { Inici };